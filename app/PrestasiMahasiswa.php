<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PrestasiMahasiswa extends Model
{
    protected $fillable = [
        'id',
        'npm',
        'unit_id',
        'nama_kegiatan',
        'nama_kegiatan_en',
        'kategori_kegiatan',
        'jenis_kegiatan',
        'tgl_pelaksana',
        'tempat_penyelenggara',
        'nama_penyelenggara',
        'tingkat',
        'jenis_prestasi',
        'capaian_prestasi',
        'kategori_peserta',
        'ket_kategori_peserta',

    ];

    public function mahasiswa()
    {
        return $this->belongsTo('App\Mahasiswa','npm');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit','unit_id');
    }

    public function barchartPrestasi($tahun){
    	$category = [];

    	$series[0]['name'] = 'Akademik';
    	$series[1]['name'] = 'Non Akademik';

        $data_mhs = $this::join('units','units.id','prestasi_mahasiswas.unit_id')->groupBy('unit_id')->get();
        foreach($data_mhs as $r){

            $category[] = $r->unit->nama_unit;

            $series[0]['data'][] = Self::where('jenis_prestasi', '=', 'Akademik')->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)->where('unit_id', $r->unit_id)->groupBy('jenis_prestasi')->groupBy('unit_id')
            ->count();
    		$series[1]['data'][] = Self::where('jenis_prestasi', '=', 'Non Akademik')->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)->where('unit_id',$r->unit_id)->groupBy('jenis_prestasi')->groupBy('unit_id')
            ->count();
        }

    	return ['category' => $category, 'series' => $series];
    }

    public function barchartPrestasi_fakultas($tahun){
    	$category = [];

    	$series[0]['name'] = 'Akademik';
    	$series[1]['name'] = 'Non Akademik';

        $data_mhs = DB::table('units AS p_unit')
        ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
        ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
        ->select('p_unit.nama_unit as nama_fakultas','p_unit.id as unit_id')
        ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
        ->groupBy('p_unit.id')->get();

        foreach($data_mhs as $r){

            $category[] = $r->nama_fakultas;
            $series[0]['data'][] = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->select('p_unit.nama_unit as nama_fakultas','p_unit.id as unit_id')
            ->where('jenis_prestasi', '=', 'Akademik')
            ->where('p_unit.id', $r->unit_id)
            ->groupBy('p_unit.id')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();

    		$series[1]['data'][] = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->select('p_unit.nama_unit as nama_fakultas','p_unit.id as unit_id')
            ->where('jenis_prestasi', '=', 'Non Akademik')
            ->where('p_unit.id', $r->unit_id)
            ->groupBy('p_unit.id')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }


    	return ['category' => $category, 'series' => $series];
    }

    public function tingkatKegiatanChart($tahun){
    	$category = [];

    	$series[0]['name'] = 'Lokal';
    	$series[1]['name'] = 'Nasional';
    	$series[2]['name'] = 'Internasional';

        $data_mhs = $this::join('units','units.id','prestasi_mahasiswas.unit_id')->groupBy('unit_id')->get();
        foreach($data_mhs as $r){

            $category[] = $r->unit->nama_unit;
            $series[0]['data'][] = Self::where('tingkat', '=', 'Lokal')->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)->where('unit_id', $r->unit_id)->groupBy('tingkat')->groupBy('unit_id')
            ->count();
    		$series[1]['data'][] = Self::where('tingkat', '=', 'Nasional')->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)->where('unit_id',$r->unit_id)->groupBy('tingkat')->groupBy('unit_id')
            ->count();
    		$series[2]['data'][] = Self::where('tingkat', '=', 'Internasional')->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)->where('unit_id',$r->unit_id)->groupBy('tingkat')->groupBy('unit_id')
            ->count();

        }

    	return ['category' => $category, 'series' => $series];
    }

    public function capaianPrestasiChart($tahun){
    	$category = [];
        // $i=0;
    	$series[]['name'] = '';
        $data_mhs = $this::join('units','units.id','prestasi_mahasiswas.unit_id')->groupBy('capaian_prestasi')->get();

        // dd($data_mhs);
        foreach($data_mhs as $r){

            $category[] = $r->capaian_prestasi;
            $series[0]['data'][] = Self::where('capaian_prestasi', $r->capaian_prestasi)->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)->count();
    		// $series[$i]['name'] = $r->capaian_prestasi;

            // $i++;
        }

    	return ['category' => $category, 'series' => $series];
    }

    public function jenisKegiatanChart(){
    	$category = [];
        // $i=0;
    	$series[]['name'] = '';
        $data_mhs = $this::join('units','units.id','prestasi_mahasiswas.unit_id')->groupBy('jenis_kegiatan')->get();

        // dd($data_mhs);
        foreach($data_mhs as $r){

            $category[] = $r->jenis_kegiatan;
            $series[0]['data'][] = Self::where('jenis_kegiatan', $r->jenis_kegiatan)->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)->count();
    		// $series[$i]['name'] = $r->capaian_prestasi;

            // $i++;
        }

    	return ['category' => $category, 'series' => $series];
    }

    public function dashboard_internasional($tahun){
        if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2){
            $data = $this::where('tingkat','Internasional')
            ->where('jenis_prestasi','Akademik')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 3){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Internasional')
            ->where('jenis_prestasi','Akademik')
            ->where('p_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 4){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Internasional')
            ->where('jenis_prestasi','Akademik')
            ->where('c_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 5){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Internasional')
            ->where('jenis_prestasi','Akademik')
            ->where('prestasi_mahasiswas.npm', \Auth::user()->email)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        return $data;
    }

    public function dashboard_nasional($tahun){

        if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2){
            $data = $this::where('tingkat','Nasional')
            ->where('jenis_prestasi','Akademik')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 3){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Nasional')
            ->where('jenis_prestasi','Akademik')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->where('p_unit.id', \Auth::user()->unit_id)->count();
        }

        if(\Auth::user()->permissions_id == 4){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Nasional')
            ->where('jenis_prestasi','Akademik')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->where('c_unit.id', \Auth::user()->unit_id)->count();
        }

        if(\Auth::user()->permissions_id == 5){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Nasional')
            ->where('jenis_prestasi','Akademik')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->where('prestasi_mahasiswas.npm', \Auth::user()->email)->count();
        }

        return $data;
    }

    public function dashboard_lokal($tahun){

        if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2){
            $data = $this::where('tingkat','Lokal')
            ->where('jenis_prestasi','Akademik')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 3){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Lokal')
            ->where('jenis_prestasi','Akademik')
            ->where('p_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 4){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Lokal')
            ->where('jenis_prestasi','Akademik')
            ->where('c_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 5){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Lokal')
            ->where('jenis_prestasi','Akademik')
            ->where('prestasi_mahasiswas.npm', \Auth::user()->email)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }
        return $data;
    }

    public function dashboard_internasional_non($tahun){
        if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2){
            $data = $this::where('tingkat','Internasional')
            ->where('jenis_prestasi','Non Akademik')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 3){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Internasional')
            ->where('jenis_prestasi','Non Akademik')
            ->where('p_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 4){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Internasional')
            ->where('jenis_prestasi','Non Akademik')
            ->where('c_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 5){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Internasional')
            ->where('jenis_prestasi','Non Akademik')
            ->where('prestasi_mahasiswas.npm', \Auth::user()->email)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        return $data;
    }

    public function dashboard_nasional_non($tahun){

        if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2){
            $data = $this::where('tingkat','Nasional')
            ->where('jenis_prestasi','Non Akademik')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 3){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Nasional')
            ->where('jenis_prestasi','Non Akademik')
            ->where('p_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 4){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Nasional')
            ->where('jenis_prestasi','Non Akademik')
            ->where('c_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 5){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Nasional')
            ->where('jenis_prestasi','Non Akademik')
            ->where('prestasi_mahasiswas.npm', \Auth::user()->email)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        return $data;
    }

    public function dashboard_lokal_non($tahun){

        if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2){
            $data = $this::where('tingkat','Lokal')
            ->where('jenis_prestasi','Non Akademik')
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 3){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Lokal')
            ->where('jenis_prestasi','Non Akademik')
            ->where('p_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 4){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Lokal')
            ->where('jenis_prestasi','Non Akademik')
            ->where('c_unit.id', \Auth::user()->unit_id)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }

        if(\Auth::user()->permissions_id == 5){
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('tingkat','Lokal')
            ->where('jenis_prestasi','Non Akademik')
            ->where('prestasi_mahasiswas.npm', \Auth::user()->email)
            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
            ->count();
        }
        return $data;
    }


}
