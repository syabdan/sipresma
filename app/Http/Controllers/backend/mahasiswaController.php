<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Permission;
use App\User;
use App\Mahasiswa;
use App\PeriodeWisuda;
use Auth;
use File;
use Illuminate\Config;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Hash;

class mahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Grab all the data
        $title                      = "Mahasiswa";
        // Show the page
        return view('backend.mahasiswa.index', compact('title'));
    }

    public function data()
    {
        if(request()->ajax()) {
            if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2){

                return datatables()->of($data = Mahasiswa::select('*')->with('unit'))
                ->addIndexColumn()
                ->make(true);

            }else if(\Auth::user()->permissions_id == 4){

                return datatables()->of($data = Mahasiswa::select('*')->where('unit_id', \Auth::user()->unit_id)->with('unit'))
                ->addIndexColumn()
                ->make(true);

            }

        }
    }

    public function index_identitas()
    {
        $title                      = "Data Identitas Mahasiswa";
        return view('backend.mahasiswa.identitas.index', compact('title'));
    }

    public function data_identitas()
    {
        if(request()->ajax()) {
            // if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2){
            //     return datatables()->of($data = Mahasiswa::select('*')->orderBy()->with('unit'))
            //     ->addColumn('actions',function($data) {
            //         return '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-edit"></i></a>';
            //     })
            //     ->addIndexColumn()
            //     ->rawColumns(['actions'])
            //     ->make(true);

            // }else
            if(\Auth::user()->permissions_id == 4){

                $d  = Mahasiswa::select(['mahasiswas.id as id','mahasiswas.npm', 'mahasiswas.nama','mahasiswas.tahun_angkatan','mahasiswas.tahun_lulus','mahasiswas.unit_id','mahasiswas.tempat_lahir','mahasiswas.tanggal_lahir','mahasiswas.nomor_ijazah','mahasiswas.nomor_ijazah_pin','mahasiswas.judul_skripsi','mahasiswas.judul_skripsi_en','mahasiswas.gelar','units.nama_unit','units.akreditasi','units.no_sertifikat_akre','prestasi_mahasiswas.npm as npmprestasi', 'mahasiswas.tanggal_lulus_sidang'
                ])
                ->join('prestasi_mahasiswas', 'prestasi_mahasiswas.npm', 'mahasiswas.npm')
                ->join('units', 'units.id', 'mahasiswas.unit_id')
                ->groupBy('npmprestasi')
                ->where('mahasiswas.unit_id', \Auth::user()->unit_id);
                $data = $d->get();

                return DataTables::of($data)
                ->addColumn('actions',function($data) {
                    return '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-edit"></i></a>';
                })
                ->addColumn('judul_skripsi',function($data) {
                    return "<div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>$data->judul_skripsi</div>";
                })
                ->addColumn('judul_skripsi_en',function($data) {
                    return "<div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>$data->judul_skripsi_en</div>";
                })
                ->addIndexColumn()
                ->rawColumns(['actions','judul_skripsi','judul_skripsi_en'])
                ->make(true);

            }

        }
    }

    public function store_identitas(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'judul_skripsi' => 'required',
            'judul_skripsi_en' => 'required',
            'nomor_ijazah' => 'required',
            'nomor_ijazah_pin' => 'required',
            'tanggal_lulus_sidang' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->withErrors($validator);
        }else{
            $data                       = Mahasiswa::find($id);
        
            if(is_null($data->no_skpi) || $data->status_validasi != 1){
                $create_nomor            = $this->generate_nomor($request->unit_id);
                $data->no_skpi           = $create_nomor;

                $cek_periode        = PeriodeWisuda::where('status', 1)->orderBy('id','desc')->first();
                $update_noUrut      = PeriodeWisuda::find($cek_periode->id)->update(['no_terakhir' => $cek_periode->no_terakhir + 1]);

            }

            $data->judul_skripsi        = $request->judul_skripsi;
            $data->judul_skripsi_en     = $request->judul_skripsi_en;
            $data->nomor_ijazah         = $request->nomor_ijazah;
            $data->nomor_ijazah_pin     = $request->nomor_ijazah_pin;
            $data->tanggal_lulus_sidang = $request->tanggal_lulus_sidang;
            $data->status_validasi      = 1;
            

            $data->update() ? 1 : 0;
        }   
    }

    function generate_nomor($unit_id){
        $cek_periode    = PeriodeWisuda::where('status', 1)->orderBy('id','desc')->first();
        $no_urut        = $cek_periode->no_terakhir + 1;
        if(\Str::length($no_urut) == 1){
            $no_urut    = '000'.$no_urut;
        }else if(\Str::length($no_urut) == 2){
            $no_urut    = '00'.$no_urut;
        }else if(\Str::length($no_urut) == 3){
            $no_urut    = '0'.$no_urut;
        }else{
            $no_urut    = $no_urut;
        }
        $bulan          = $cek_periode->bulan;        
        $tahun          = $cek_periode->tahun;        

        $no_skpi        = "SKPI/$no_urut/$unit_id/UIR-$bulan/$tahun";

        return $no_skpi;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        // return app(skpiController::class)->show($id);
        $data = Mahasiswa::select('mahasiswas.id', 'mahasiswas.npm', 'mahasiswas.nama','mahasiswas.tahun_angkatan','mahasiswas.tahun_lulus','mahasiswas.unit_id','mahasiswas.tempat_lahir','mahasiswas.tanggal_lahir','mahasiswas.gelar','units.nama_unit','units.akreditasi','units.no_sertifikat_akre'
        ,'capaian_pembelajarans.isi_cp','capaian_pembelajarans.isi_cp_en'
        ,'prestasi_mahasiswas.nama_kegiatan','prestasi_mahasiswas.nama_kegiatan_en'
        )

        ->leftjoin('prestasi_mahasiswas', 'prestasi_mahasiswas.npm', 'mahasiswas.npm')
        ->leftjoin('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->where('mahasiswas.id',$id)
        ->orderBy('mahasiswas.id', 'DESC')->first();

        // Ambil API Alumni untuk data skripsi
        $api_skripsi    = api_skripsi_by_npm($data['npm']);
        $data_skripsi   = collect(json_decode($api_skripsi))->first();
        $data2          = json_encode($data_skripsi ?? array(0 => 'kosong'));

        if($data_skripsi === NULL){
            return response()->json(['errors' => ['status_lulus' => ['Mahasiswa Belum Lulus']]], 422);
        }else{
            return json_encode(array_merge(json_decode($data, true),json_decode($data2 , true)));
        }

        // return response()->json($data);
    }


    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {


    }


    public function edit($user)
    {

    }

    public function update(Request $request, $id)
    {


    }

    public function destroy($id)
    {

    }
}
