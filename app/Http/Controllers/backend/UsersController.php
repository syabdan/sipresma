<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Permission;
use App\User;
use App\Menu;
use App\Unit;
use Auth;
use File;
use Illuminate\Config;
use Illuminate\Http\Request;
use App\Http\Requests\userRequest;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use DB;
use Hash;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Grab all the data
        $data           = User::all();
        $permissions    = Permission::all();
        $units          = Unit::all();
        $title          = "User";
        // Show the page
        return view('backend.user.index', compact('data','title','permissions','units'));
    }

    public function data()
    {             
        $data = User::select(['id','name', 'email','permissions_id'])
                ->where('permissions_id', '<>', 5)
                ->orderBy('id', 'DESC')->get();
        return DataTables::of($data)
            ->editColumn('level',function(User $data) {
                return $data->permissionsGroup->permission_name;
            })
 
            ->addColumn('actions',function($data) {
                $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Edit"><i class="zmdi zmdi-edit"></i></a>';
                if($data->id != Auth::user()->id){
                    $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Delete"><i class="zmdi zmdi-delete"></i></a>';

                }
              
                return $actions;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','level'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $data = User::FindOrFail($id);    
        return json_encode($data);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(userRequest $request)
    {

        $data = new User();

        if ($file = $request->file('pic_file')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            // if(!file_exists( public_path() . 'backend/assets/images/users/'))
            // {
            //     File::makeDirectory( public_path() . 'backend/assets/images/users/');
            // }
            $destinationPath = 'backend/assets/images/users/';

            $file->move($destinationPath, $gambarName);
            $data->pic = $gambarName;
        }
        $data->name                 = $request->name;
        $data->email                = $request->email;
        $data->hp                   = $request->hp;
        $data->permissions_id       = $request->level;
        $data->unit_id              = $request->unit_id;
        $data->status               = 1;
        $data->created_by           = Auth::user()->id;
        $data->password             = bcrypt($request->password);

        return $data->save() ? 1 : 0;

        
    }


    public function edit($user)
    {
        
    }

    public function update(userRequest $request, $id)
    {

        $data = User::find($id);
        if ($file = $request->file('pic_file')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];
    
            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
    
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension(); 
            // if(!file_exists( public_path() . 'backend/assets/images/users/'))
            // {
            //     File::makeDirectory( public_path() . 'backend/assets/images/users/');
            // }
            $destinationPath = 'backend/assets/images/users/';

            $file->move($destinationPath, $gambarName);
            $data->pic = $gambarName;
        }

        if($request->name){
            $data->name                 = $request->name;
        }

        if($request->email){
            $data->email                = $request->email;
        }
        
        if($request->hp){
            $data->hp                   = $request->hp;
        }

        if($request->level){
            $data->permissions_id       = $request->level;
        }

        if($request->unit_id){
            $data->unit_id              = $request->unit_id;
        }

        if($request->password){
            $data->password             = bcrypt($request->password);
        }
        $data->updated_by               = Auth::user()->id;
        

        return $data->update() ? 1 : 0;

    }

    public function destroy($id)
    {
        $data = User::find($id);
        return $data->delete() ? 1 : 0;
    }
}
