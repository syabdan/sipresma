<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Permission;
use App\User;
use App\PrestasiMahasiswa;
use App\Mahasiswa;
use App\Unit;
use App\CapaianPembelajaran;
use Auth;
use File;
use Illuminate\Config;
use App\Http\Requests\prestasiRequest;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Hash;
use Carbon\Carbon;

class prestasiController extends Controller
{
    public function index()
    {
		\Artisan::call('update:total_prestasi');
        // Grab all the data
        $data                       = User::all();
        $prestasi                   = PrestasiMahasiswa::all();
        $units                      = Unit::all();
        $title                      = "Prestasi Mahasiswa";
        // Show the page
        return view('backend.prestasi.index', compact('data','title','prestasi','units'));
    }

    public function validasi_index()
    {
        // Grab all the data
        $data                       = User::all();
        $prestasi                   = PrestasiMahasiswa::all();
        $units                      = Unit::all();
        $title                      = "Validasi Prestasi Mahasiswa";
        // Show the page
        return view('backend.prestasi.validasi', compact('data','title','prestasi','units'));
    }

    public function monitoring_index()
    {
        // Grab all the data
        $title                      = "Monitoring Prestasi Mahasiswa";
        // Show the page
        return view('backend.prestasi.monitoring', compact('title'));
    }
    
    public function rekapitulasi_prodi($tahun=NULL)
    {
        // Grab all the data
        $title                      = "Rekapitulasi Prestasi Mahasiswa";
        $tahun_kegiatan             = PrestasiMahasiswa::selectRaw('year(tgl_pelaksanaan) tahun_kegiatan')->groupBy('tahun_kegiatan')->pluck('tahun_kegiatan','tahun_kegiatan');

        // Show the page
        return view('backend.prestasi.rekapitulasi_prodi', compact('title','tahun_kegiatan','tahun'));
    }

    public function akademik()
    {
        // Grab all the data
        $data                       = User::all();
        $prestasi                   = PrestasiMahasiswa::all();
        $units                      = Unit::all();
        $title                      = "Prestasi Akademik Mahasiswa";
        // Show the page
        return view('backend.prestasi.akademik', compact('data','title','prestasi','units'));
    }

    public function nonakademik()
    {
        // Grab all the data
        $data                       = User::all();
        $prestasi                   = PrestasiMahasiswa::all();
        $units                      = Unit::all();
        $title                      = "Prestasi Non Akademik Mahasiswa";
        // Show the page
        return view('backend.prestasi.nonakademik', compact('data','title','prestasi','units'));
    }

    public function data()
    {
        if(\Auth::user()->permissions_id == 5 ){ // Mahasiswa
            $data = PrestasiMahasiswa::select(['prestasi_mahasiswas.*'])
                    ->where('npm', Auth::user()->email)
                    ->orderBy('id', 'DESC')->get();
        }else if(\Auth::user()->permissions_id == 4 ){ // Prodi
            $data = PrestasiMahasiswa::select(['prestasi_mahasiswas.*','mahasiswas.npm as npm_mhs','nama','tempat_lahir','tanggal_lahir','email','total_prestasi'])
                    ->join('units','units.id','prestasi_mahasiswas.unit_id')
                    ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('units.id', Auth::user()->unit_id)
                    // ->whereNull('status_verifikasi_prodi')
                    ->groupBy('npm_mhs')
                    ->orderBy('mahasiswas.total_prestasi','desc')->get();
        }else if(\Auth::user()->permissions_id == 3 ){ // Fakultas
            $data = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('p_unit.id', \Auth::user()->unit_id)
                    ->select(['prestasi_mahasiswas.*','nama','tempat_lahir','tanggal_lahir','email','total_prestasi','c_unit.nama_unit as prodi', 'p_unit.nama_unit as fakultas'])
                    ->groupBy('mahasiswas.npm')
                    ->orderBy('mahasiswas.total_prestasi','desc')->get();
        }else{
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->select(['prestasi_mahasiswas.*','nama','c_unit.nama_unit as prodi', 'p_unit.nama_unit as fakultas','tempat_lahir','tanggal_lahir','email','total_prestasi'])
            ->groupBy('mahasiswas.npm')
            ->orderBy('mahasiswas.total_prestasi','desc')
            ->get();
        }
        return DataTables::of($data)
            ->addColumn('actions',function($data) {
                if(\Auth::user()->permissions_id != 3){
                    $actions = '<a onclick="detailForm('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Detail"><i class="zmdi zmdi-search"></i></a>';
                    $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Delete"><i class="zmdi zmdi-delete"></i></a>';
                    // if($data->status_verifikasi_prodi == NULL && \Auth::user()->permissions_id == 4){
                    //     $actions .= '<a onclick="transFormAll('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Translate"><i class="zmdi zmdi-spellcheck"></i></a>';
                    //     $actions .= '<a onclick="approve_oneForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Approve"><i class="zmdi zmdi-assignment"></i></a>';
                    // }
                    if(\Auth::user()->permissions_id == 5){
                        $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-edit"></i></a>';
                        $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Delete"><i class="zmdi zmdi-delete"></i></a>';
                    }
                }else{
                    $actions = '<a onclick="detailForm('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Detail"><i class="zmdi zmdi-search"></i></a>';
                }
                return $actions;
            })

            ->addColumn('sertifikat',function($data) {
            $file = '<a href='. url('prestasi/ambilFile/'.$data->id) .' target="_blank"><i class="zmdi zmdi-download"></i> Lihat</a>';
            return $file;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','sertifikat'])
            ->make(true);
    }

    public function data_validasi()
    {

            $data = PrestasiMahasiswa::select(['prestasi_mahasiswas.*','mahasiswas.npm as npm_mhs','nama','tempat_lahir','tanggal_lahir','email'])
                    ->join('units','units.id','prestasi_mahasiswas.unit_id')
                    ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('units.id', Auth::user()->unit_id)
                    ->whereNull('status_verifikasi_prodi')
                    ->groupBy('npm_mhs')
                    ->orderBy('prestasi_mahasiswas.updated_at', 'DESC')->get();

        return DataTables::of($data)
            ->addColumn('actions',function($data) {
                    $actions = '<a onclick="detailForm('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Detail"><i class="zmdi zmdi-search"></i></a>';
                    $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Delete"><i class="zmdi zmdi-delete"></i></a>';
                    // $actions .= '<a onclick="transFormAll('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Translate"><i class="zmdi zmdi-spellcheck"></i></a>';
                    // $actions .= '<a onclick="approveForm('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Approve"><i class="zmdi zmdi-assignment"></i></a>';

                return $actions;
            })

            ->addColumn('sertifikat',function($data) {
            $file = '<a href='. url('prestasi/ambilFile/'.$data->id) .' target="_blank"><i class="zmdi zmdi-download"></i> Lihat</a>';
            return $file;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','sertifikat'])
            ->make(true);
    }

    public function data_monitoring()
    {

            $data = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->rightjoin('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('mahasiswas.unit_id', \Auth::user()->unit_id)
                    ->select(['mahasiswas.npm as npm','kategori_kegiatan','jenis_prestasi','nama','c_unit.id as idp','mahasiswas.total_prestasi'])
                    ->orderBy('mahasiswas.total_prestasi','desc')
                    ->groupBy('npm')
                    ->get();

            // $data = PrestasiMahasiswa::select(['prestasi_mahasiswas.npm as npm','kategori_kegiatan','jenis_prestasi','nama'])
            //         ->join('units','units.id','prestasi_mahasiswas.unit_id')
            //         ->rightjoin('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            //         ->where('mahasiswas.unit_id', Auth::user()->unit_id)
            //         ->groupBy('npm')->get();

            $p_akademik = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('c_unit.id', \Auth::user()->unit_id)
                    ->where('kategori_kegiatan', "Prestasi")->where('jenis_prestasi', "Akademik")
                    ->select(['prestasi_mahasiswas.npm as npm'])->get();
            $p_nonakademik = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('c_unit.id', \Auth::user()->unit_id)
                    ->where('kategori_kegiatan', "Prestasi")->where('jenis_prestasi', "Non Akademik")
                    ->select(['prestasi_mahasiswas.npm as npm'])->get();
            $a_akademik = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('c_unit.id', \Auth::user()->unit_id)
                    ->where('kategori_kegiatan', "Aktivitas")->where('jenis_prestasi', "Akademik")
                    ->select(['prestasi_mahasiswas.npm as npm'])->get();
            $a_nonakademik = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('c_unit.id', \Auth::user()->unit_id)
                    ->where('kategori_kegiatan', "Aktivitas")->where('jenis_prestasi', "Non Akademik")
                    ->select(['prestasi_mahasiswas.npm as npm'])->get();
    //   dd($a_nonakademik);
        return DataTables::of($data)
            ->addColumn('prestasi_akademik',function($data) use($p_akademik) {
                $i=0;
                foreach($p_akademik as $r){
                    if($r->npm == $data->npm){
                       $i++;
                    }
                }
                return $i;
            })
            ->addColumn('prestasi_nonakademik',function($data) use($p_nonakademik) {
                $i=0;
                foreach($p_nonakademik as $r){
                    if($r->npm == $data->npm){
                       $i++;
                    }
                }
                return $i;
            })
            ->addColumn('aktifitas_akademik',function($data) use($a_akademik) {
                $i=0;
                foreach($a_akademik as $r){
                    if($r->npm == $data->npm){
                       $i++;
                    }
                }
                return $i;
            })
            ->addColumn('aktifitas_nonakademik',function($data) use($a_nonakademik) {
                $i=0;
                foreach($a_nonakademik as $r){
                    if($r->npm == $data->npm){
                       $i++;
                    }
                }
                return $i;
            })


            ->addIndexColumn()
            ->rawColumns(['prestasi_akademik','prestasi_nonakademik','aktifitas_akademik','aktifitas_nonakademik'])
            ->make(true);
    }
   
    public function data_rekapitulasi($tahun=NULL)
    {
        is_null($tahun) ? $tahun = Carbon::now()->year : $tahun = $tahun;

            $d       = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->leftjoin('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->rightjoin('mahasiswas', 'prestasi_mahasiswas.npm', 'mahasiswas.npm')
                    ->where('c_unit.id', '!=', '111');
                    if(\Auth::user()->permissions_id == 3){
                        $d->where('p_unit.id', \Auth::user()->unit_id);
                    }

                    $d->orderBy('mahasiswas.total_prestasi', 'DESC')
                    ->select(['c_unit.jenis_pendidikan','kategori_kegiatan','jenis_prestasi','c_unit.nama_unit','c_unit.id as idp'])
                    ->groupBy('c_unit.nama_unit');

                    $data = $d->get();


            $p_akademik = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                    ->where('kategori_kegiatan', "Prestasi")->where('jenis_prestasi', "Akademik")
                    ->select(['prestasi_mahasiswas.unit_id as unit_id'])->get();
            $p_nonakademik = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                    ->where('kategori_kegiatan', "Prestasi")->where('jenis_prestasi', "Non Akademik")
                    ->select(['prestasi_mahasiswas.unit_id as unit_id'])->get();
            $a_akademik = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                    ->where('kategori_kegiatan', "Aktivitas")->where('jenis_prestasi', "Akademik")
                    ->select(['prestasi_mahasiswas.unit_id as unit_id'])->get();
            $a_nonakademik = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                    ->where('kategori_kegiatan', "Aktivitas")->where('jenis_prestasi', "Non Akademik")
                    ->select(['prestasi_mahasiswas.unit_id as unit_id'])->get();
    //   dd($a_nonakademik);
        return DataTables::of($data)
            ->addColumn('prestasi_akademik',function($data) use($p_akademik) {
                $i=0;
                foreach($p_akademik as $r){
                    if($r->unit_id == $data->idp){
                       $i++;
                    }
                }
                return $i;
            })
            ->addColumn('prestasi_nonakademik',function($data) use($p_nonakademik) {
                $i=0;
                foreach($p_nonakademik as $r){
                    if($r->unit_id == $data->idp){
                       $i++;
                    }
                }
                return $i;
            })
            ->addColumn('aktifitas_akademik',function($data) use($a_akademik) {
                $i=0;
                foreach($a_akademik as $r){
                    if($r->unit_id == $data->idp){
                       $i++;
                    }
                }
                return $i;
            })
            ->addColumn('aktifitas_nonakademik',function($data) use($a_nonakademik) {
                $i=0;
                foreach($a_nonakademik as $r){
                    if($r->unit_id == $data->idp){
                       $i++;
                    }
                }
                return $i;
            })


            ->addIndexColumn()
            ->rawColumns(['prestasi_akademik','prestasi_nonakademik','aktifitas_akademik','aktifitas_nonakademik'])
            ->make(true);
    }

    public function data_jenisprestasi(Request $request)
    {
        if(\Auth::user()->permissions_id == 5 ){ // Mahasiswa
            $data = PrestasiMahasiswa::select(['prestasi_mahasiswas.*'])
                    ->where('npm', Auth::user()->email)
                    ->where('jenis_prestasi', $request->jenis_prestasi)
                    ->orderBy('id', 'DESC')->get();
        }else if(\Auth::user()->permissions_id == 4 ){ // Prodi
            $data = PrestasiMahasiswa::select(['prestasi_mahasiswas.*','nama'])
                    ->join('units','units.id','prestasi_mahasiswas.unit_id')
                    ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('units.id', Auth::user()->unit_id)
                    ->where('jenis_prestasi', $request->jenis_prestasi)
                    ->orderBy('prestasi_mahasiswas.id', 'DESC')->get();
        }else if(\Auth::user()->permissions_id == 3 ){ // Fakultas
            $data = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                    ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                    ->where('p_unit.id', \Auth::user()->unit_id)
                    ->where('jenis_prestasi', $request->jenis_prestasi)
                    ->select(['prestasi_mahasiswas.*','nama','c_unit.nama_unit as prodi', 'p_unit.nama_unit as fakultas'])
                    ->orderBy('prestasi_mahasiswas.npm', 'DESC')->get();

        }else{
            $data = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
            ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
            ->where('jenis_prestasi', $request->jenis_prestasi)
            ->select(['prestasi_mahasiswas.*','nama','c_unit.nama_unit as prodi', 'p_unit.nama_unit as fakultas'])
            // ->groupBy('mahasiswas.npm')
            ->get();

        }
        return DataTables::of($data)
            ->addColumn('actions',function($data) {
                if(\Auth::user()->permissions_id != 3){
                    // $actions = '<a onclick="detailForm('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Detail"><i class="zmdi zmdi-edit"></i></a>';
                    $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-edit"></i></a>';
                    $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Delete"><i class="zmdi zmdi-delete"></i></a>';
                    // if($data->status_verifikasi_prodi == NULL && \Auth::user()->permissions_id == 4){
                    //     $actions .= '<a onclick="transFormAll('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Translate"><i class="zmdi zmdi-spellcheck"></i></a>';
                    //     $actions .= '<a onclick="approveForm('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Approve"><i class="zmdi zmdi-assignment"></i></a>';
                    // }
                    if(\Auth::user()->permissions_id == 5){
                        $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-edit"></i></a>';
                        $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Delete"><i class="zmdi zmdi-delete"></i></a>';
                    }
                }else{
                    $actions = '-';
                }
                return $actions;
            })

            ->addColumn('sertifikat',function($data) {
            $file = '<a href='. url('prestasi/ambilFile/'.$data->id) .' target="_blank"><i class="zmdi zmdi-download"></i> Lihat</a>';
            return $file;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','sertifikat'])
            ->make(true);
    }

    public function data_detail(Request $request)
    {
        $data = PrestasiMahasiswa::select(['prestasi_mahasiswas.*'])
                    ->where('npm', $request->npm)
                    ->orderBy('id', 'DESC')->get();
        return DataTables::of($data)
            ->addColumn('actions',function($data) {
                if(Auth::user()->permissions_id == 4 && $data->status_verifikasi_prodi == NULL){
                    $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-edit"></i></a>';
                    $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-delete"></i></a>';
                    $actions .= '<a onclick="approve_oneForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Approve"><i class="zmdi zmdi-assignment"></i></a>';
                }else{
                    $actions = '-';
                }

                return $actions;
            })

            ->addColumn('sertifikat',function($data) {
            $file = '<a href='. url('prestasi/ambilFile/'.$data->id) .' target="_blank"><i class="zmdi zmdi-download"></i> Lihat</a>';
            return $file;
            })

            ->addColumn('status_verifikasi_prodi',function($data) {
                if($data->status_verifikasi_prodi == 1){
                    $data = '<button  class="btn btn-round btn-success disabled btn-xs"> Approved</button>';
                }else{
                    $data = '<button  class="btn btn-round btn-warning disabled btn-xs"><i class="zmdi zmdi-loading"></i> Pending</button>';
                }
                return $data;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','sertifikat','status_verifikasi_prodi'])
            ->make(true);
    }

    public function ambilFile($id)
    {
        $data = PrestasiMahasiswa::where('id',$id)->first();
        return response()->file(public_path('backend/assets/images/sertifikats/'.$data->sertifikat));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $data = PrestasiMahasiswa::FindOrFail($id);
        return json_encode($data);
    }

    public function showTranslate($npm)
    {
        $data = PrestasiMahasiswa::select('prestasi_mahasiswas.*','judul_skripsi','judul_skripsi_en')
        ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
        ->where('prestasi_mahasiswas.npm', $npm)
        ->orderBy('id', 'DESC')->get();

        // dd($data);
        return json_encode($data);
    }


    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(prestasiRequest $request)
    {

        $data = new PrestasiMahasiswa();
        if(\Auth::user()->permissions_id == 5){
            $validator = Validator::make($request->all(),[
                'sertifikat' => 'required|mimes:pdf|max:2000'
            ]);
        }
        if ($file = $request->file('sertifikat')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];

            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);

            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
            // if(!file_exists( public_path() . 'backend/assets/images/users/'))
            // {
            //     File::makeDirectory( public_path() . 'backend/assets/images/users/');
            // }
            $destinationPath = public_path('backend/assets/images/sertifikats/');

            $file->move($destinationPath, $gambarName);
            $data->sertifikat = $gambarName;
        }


        $data->npm                          = Auth::user()->email;
        $data->unit_id                      = Auth::user()->unit_id;
        $data->nama_kegiatan                = $request->nama_kegiatan;
        $data->nama_kegiatan_en             = $request->nama_kegiatan_en;
        $data->kategori_kegiatan            = $request->kategori_kegiatan;
        $data->jenis_kegiatan               = $request->jenis_kegiatan;
        $data->tgl_pelaksanaan              = $request->tgl_pelaksanaan;
        $data->tempat_penyelenggara         = $request->tempat_penyelenggara;
        $data->nama_penyelenggara           = $request->nama_penyelenggara;
        $data->tingkat                      = $request->tingkat;
        $data->jenis_prestasi               = $request->jenis_prestasi;
        $data->capaian_prestasi             = $request->capaian_prestasi;
        $data->kategori_peserta             = $request->kategori_peserta;
        $data->ket_kategori_peserta         = $request->ket_kategori_peserta;

        $mhs = Mahasiswa::where('npm', \Auth::user()->email)->first();

        if($mhs){
            $data->save() ? 1 : 0;
            $data2 = DB::table('mahasiswas')->where('npm', \Auth::user()->email)->update(['total_prestasi' => $mhs->total_prestasi+1]);
            return $data2;
        }else{
            return response()->view('errors.custom', [], 500);
        }

    }

    public function simpanTranslate(prestasiRequest $request, $npm)
    {
        $hitung = count($request->id_prestasi);
        // dd($hitung);
        for($i=0; $i<$hitung; $i++)
        {
            $data = PrestasiMahasiswa::find($request->id_prestasi[$i]);
            $data->nama_kegiatan                = $request->nama_kegiatan_trans[$i];
            $data->nama_kegiatan_en             = $request->nama_kegiatan_en_trans[$i];
            $data->update();
        }

        $data_mahasiswa = array(
            'judul_skripsi'          => $request->judul_skripsi,
            'judul_skripsi_en'       => $request->judul_skripsi_en
        );
        DB::table('mahasiswas')->where('npm', $request->npm)->update($data_mahasiswa);

        return $data;
    }


    public function edit($user)
    {

    }

    public function update(prestasiRequest $request, $id)
    {
        $data = PrestasiMahasiswa::find($id);
        if($request->form_trans == '1'){
                $data->nama_kegiatan_en             = $request->nama_kegiatan_en;
                return $data->update() ? 1 : 0;

        }else{
            if ($file = $request->file('sertifikat')) {
                $fullName=$file->getClientOriginalName();
                $name=explode('.',$fullName)[0];

                $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                $replaceImage=strtolower($originalImage);

                $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
                // if(!file_exists( public_path() . 'backend/assets/images/users/'))
                // {
                //     File::makeDirectory( public_path() . 'backend/assets/images/users/');
                // }
                $destinationPath = public_path('backend/assets/images/sertifikats/');

                $file->move($destinationPath, $gambarName);
                $data->sertifikat = $gambarName;
            }
            // $data->npm                          = Auth::user()->email;
            $data->nama_kegiatan                = $request->nama_kegiatan;
            $data->kategori_kegiatan            = $request->kategori_kegiatan;
            $data->jenis_kegiatan               = $request->jenis_kegiatan;
            $data->tgl_pelaksanaan              = $request->tgl_pelaksanaan;
            $data->tempat_penyelenggara         = $request->tempat_penyelenggara;
            $data->nama_penyelenggara           = $request->nama_penyelenggara;
            $data->tingkat                      = $request->tingkat;
            $data->jenis_prestasi               = $request->jenis_prestasi;
            $data->capaian_prestasi             = $request->capaian_prestasi;
            $data->kategori_peserta             = $request->kategori_peserta;
            $data->ket_kategori_peserta         = $request->ket_kategori_peserta;
        }

        return $data->update() ? 1 : 0;

    }

    // public function trans(Request $request)
    // {
    //     $data = PrestasiMahasiswa::find($request->idTrans);

    //     $data->nama_kegiatan_en             = $request->nama_kegiatan_en;
    //     $data->deskripsi_prestasi_en        = $request->deskripsi_prestasi_en;

    //     return $data->update() ? 1 : 0;

    // }

    public function destroy($id)
    {
        $data = PrestasiMahasiswa::find($id);
        File::delete(public_path('backend/assets/images/sertifikats/'.$data->sertifikat));

        $data->delete() ? 1 : 0;

        $mhs = Mahasiswa::where('npm', \Auth::user()->email)->first();
        $data2 = DB::table('mahasiswas')->where('npm', \Auth::user()->email)->update(['total_prestasi' => $mhs->total_prestasi-1]);

        return $data2;
    }

    public function approve($npm)
    {
        $data = PrestasiMahasiswa::where('npm',$npm)->select('id')->get();

       foreach($data as $d)
       {
           $data = PrestasiMahasiswa::find($d->id);
           $data->status_verifikasi_prodi       = 1;
           $data->update();
       }

       return $data;
    }

    public function approve_one($id)
    {
        $cek_cp = CapaianPembelajaran::join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','capaian_pembelajarans.unit_id')
                                        ->where('prestasi_mahasiswas.id', $id)
                                        ->count();

        if($cek_cp > 0){
            $data = PrestasiMahasiswa::find($id);
            $data->status_verifikasi_prodi       = 1;
            $data->update();

           return 'sukses';
        }else{
            return 'cp_kosong';
        }

    }
}
