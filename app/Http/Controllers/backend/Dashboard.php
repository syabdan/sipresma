<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use App\PrestasiMahasiswa;
use DB;
use App\User;
use App\Unit;
use App\Mahasiswa;
use App\Charts\UserChart;
use Carbon\Carbon;
// use App\Charts\PrestasiBarChart;


class Dashboard extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tahun=NULL)
    {
        is_null($tahun) ? $tahun = Carbon::now()->year : $tahun = $tahun;

        //Tingkat Jenis Prestasi Akademik
        if(\Auth::user()->permissions_id == 5){
            $lokal_akademik = PrestasiMahasiswa::select('id')
                            ->where('tingkat', 'Lokal')
                            ->where('jenis_prestasi', 'Akademik')
                            ->where('npm', \Auth::user()->email)
                            ->whereYear('created_at', $tahun)
                            ->count();
            $nasional_akademik = PrestasiMahasiswa::select('id')
                            ->where('tingkat', 'Nasional')
                            ->where('jenis_prestasi', 'Akademik')
                            ->where('npm', \Auth::user()->email)
                            ->whereYear('created_at', $tahun)
                            ->count();
            $internasional_akademik = PrestasiMahasiswa::select('id')
                            ->where('tingkat', 'Internasional')
                            ->where('jenis_prestasi', 'Akademik')
                            ->where('npm', \Auth::user()->email)
                            ->whereYear('created_at', $tahun)
                            ->count();

        }else if(\Auth::user()->permissions_id == 4){
            $lokal_akademik = PrestasiMahasiswa::select('id')
                            ->where('tingkat', 'Lokal')
                            ->where('jenis_prestasi', 'Akademik')
                            ->where('unit_id', \Auth::user()->unit_id)
                            ->whereYear('created_at', $tahun)
                            ->count();
            $nasional_akademik = PrestasiMahasiswa::select('id')
                            ->where('tingkat', 'Nasional')
                            ->where('jenis_prestasi', 'Akademik')
                            ->where('unit_id', \Auth::user()->unit_id)
                            ->whereYear('created_at', $tahun)
                            ->count();
            $internasional_akademik = PrestasiMahasiswa::select('id')
                            ->where('tingkat', 'Internasional')
                            ->where('jenis_prestasi', 'Akademik')
                            ->where('unit_id', \Auth::user()->unit_id)
                            ->whereYear('created_at', $tahun)
                            ->count();

        }else if(\Auth::user()->permissions_id == 3){
            $lokal_akademik = DB::table('units AS p_unit')
                            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                            ->where('p_unit.id', \Auth::user()->unit_id)
                            ->where('tingkat', 'Lokal')
                            ->where('jenis_prestasi', 'Akademik')
                            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                            ->count();

            $nasional_akademik = DB::table('units AS p_unit')
                            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                            ->where('p_unit.id', \Auth::user()->unit_id)
                            ->where('tingkat', 'Nasional')
                            ->where('jenis_prestasi', 'Akademik')
                            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                            ->count();

            $internasional_akademik = DB::table('units AS p_unit')
                            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                            ->where('p_unit.id', \Auth::user()->unit_id)
                            ->where('tingkat', 'Internasional')
                            ->where('jenis_prestasi', 'Akademik')
                            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                            ->count();

        }else{
            $lokal_akademik = PrestasiMahasiswa::select('id')
                            ->where('tingkat', 'Lokal')
                            ->where('jenis_prestasi', 'Akademik')
                            ->whereYear('created_at', $tahun)
                            ->count();

            $nasional_akademik = PrestasiMahasiswa::select('id')
                            ->where('tingkat', 'Nasional')
                            ->where('jenis_prestasi', 'Akademik')
                            ->whereYear('created_at', $tahun)
                            ->count();

            $internasional_akademik = PrestasiMahasiswa::select('id')
                            ->where('tingkat', 'Internasional')
                            ->where('jenis_prestasi', 'Akademik')
                            ->whereYear('created_at', $tahun)
                            ->count();
        }

        //#Tingkat Jenis Prestasi Akademik

        //Tingkat Jenis Prestasi Non Akademik
        if(\Auth::user()->permissions_id == 5){
            $lokal_nonakademik = PrestasiMahasiswa::select('id')
                                ->where('tingkat', 'Lokal')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->where('npm', \Auth::user()->email)
                                ->whereYear('created_at', $tahun)
                                ->count();

            $nasional_nonakademik = PrestasiMahasiswa::select('id')
                                ->where('tingkat', 'Nasional')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->where('npm', \Auth::user()->email)
                                ->whereYear('created_at', $tahun)
                                ->count();

            $internasional_nonakademik = PrestasiMahasiswa::select('id')
                                ->where('tingkat', 'Internasional')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->where('npm', \Auth::user()->email)
                                ->whereYear('created_at', $tahun)
                                ->count();

        } else if(\Auth::user()->permissions_id == 4){
            $lokal_nonakademik = PrestasiMahasiswa::select('id')
                                ->where('tingkat', 'Lokal')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->where('unit_id', \Auth::user()->unit_id)
                                ->whereYear('created_at', $tahun)
                                ->count();
            $nasional_nonakademik = PrestasiMahasiswa::select('id')
                                ->where('tingkat', 'Nasional')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->where('unit_id', \Auth::user()->unit_id)
                                ->whereYear('created_at', $tahun)
                                ->count();
            $internasional_nonakademik = PrestasiMahasiswa::select('id')
                                ->where('tingkat', 'Internasional')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->where('unit_id', \Auth::user()->unit_id)
                                ->whereYear('created_at', $tahun)
                                ->count();
        }else if(\Auth::user()->permissions_id == 3){
            $lokal_nonakademik = DB::table('units AS p_unit')
                                ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                                ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                                ->where('p_unit.id', \Auth::user()->unit_id)
                                ->where('tingkat', 'Lokal')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                                ->count();
            $nasional_nonakademik = DB::table('units AS p_unit')
                                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                                    ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                                    ->where('p_unit.id', \Auth::user()->unit_id)
                                    ->where('tingkat', 'Nasional')
                                    ->where('jenis_prestasi', 'Non Akademik')
                                    ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                                    ->count();
            $internasional_nonakademik = DB::table('units AS p_unit')
                                ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                                ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                                ->where('p_unit.id', \Auth::user()->unit_id)
                                ->where('tingkat', 'Internasional')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                                ->count();
        }
        else{
            $lokal_nonakademik = PrestasiMahasiswa::select('id')
                                ->where('tingkat', 'Lokal')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->whereYear('created_at', $tahun)
                                ->count();
            $nasional_nonakademik = PrestasiMahasiswa::select('id')
                                ->where('tingkat', 'Nasional')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->whereYear('created_at', $tahun)
                                ->count();
            $internasional_nonakademik = PrestasiMahasiswa::select('id')
                                ->where('tingkat', 'Internasional')
                                ->where('jenis_prestasi', 'Non Akademik')
                                ->whereYear('created_at', $tahun)
                                ->count();

        }
        //#Tingkat Jenis Prestasi  Non Akademik

        // Jenis Prestasi Akademik
        if(\Auth::user()->permissions_id == 5){
            $akademik = PrestasiMahasiswa::select('id')
                        ->where('jenis_prestasi', 'Akademik')
                        ->where('npm', \Auth::user()->email)
                        ->whereYear('created_at', $tahun)
                        ->count();

            $nonakademik = PrestasiMahasiswa::select('id')
                        ->where('jenis_prestasi', 'Non Akademik')
                        ->where('npm', \Auth::user()->email)
                        ->whereYear('created_at', $tahun)
                        ->count();

        }else if(\Auth::user()->permissions_id == 4){
            $akademik = PrestasiMahasiswa::select('id')
                        ->where('jenis_prestasi', 'Akademik')
                        ->where('unit_id', \Auth::user()->unit_id)
                        ->whereYear('created_at', $tahun)
                        ->count();

            $nonakademik = PrestasiMahasiswa::select('id')
                        ->where('jenis_prestasi', 'Non Akademik')
                        ->where('unit_id', \Auth::user()->unit_id)
                        ->whereYear('created_at', $tahun)
                        ->count();

        }else if(\Auth::user()->permissions_id == 3){
            $akademik = DB::table('units AS p_unit')
                        ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                        ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                        ->where('p_unit.id', \Auth::user()->unit_id)
                        ->where('jenis_prestasi', 'Akademik')
                        ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                        ->count();

            $nonakademik = DB::table('units AS p_unit')
                            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                            ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
                            ->where('p_unit.id', \Auth::user()->unit_id)
                            ->where('jenis_prestasi', 'Non Akademik')
                            ->whereYear('prestasi_mahasiswas.tgl_pelaksanaan', $tahun)
                            ->count();

        }
        else{
            $akademik = PrestasiMahasiswa::select('id')
                        ->where('jenis_prestasi', 'Akademik')
                        ->whereYear('tgl_pelaksanaan', $tahun)
                        ->count();
            $nonakademik = PrestasiMahasiswa::select('id')
                        ->where('jenis_prestasi', 'Non Akademik')
                        ->whereYear('tgl_pelaksanaan', $tahun)
                        ->count();
        }
        //# Jenis Prestasi Akademik

        // // Highchart Mahasiswa
        // $record['chart'] = Mahasiswa::select(\DB::raw("COUNT(*) as count"))
        //             ->whereYear('created_at', date('Y'))
        //             ->groupBy(\DB::raw("Month(created_at)"))
        //             ->pluck('count');
        // // End Highchart

        // // Chart Register User
        // $users = User::select(\DB::raw("COUNT(*) as count"))
        //             ->whereYear('created_at',date('Y'))
        //             ->groupBy(\DB::raw("Month(created_at)"))
        //             ->pluck('count');
        // $usersChart = new UserChart;
        // $usersChart->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
        // $usersChart->dataset('New User Register Chart', 'line', $users)->options([
        //     'fill' => 'true',
        //     'borderColor' => '#51C1C0'
        // ]);
        // // End Chart Regiter User

        // // Chart Prestasi berdasarkan Kategori Kegiatan
        $data_prestasi              = new PrestasiMahasiswa;
        $prestasi_bar               = $data_prestasi->barchartPrestasi($tahun);
        // // #Chart Prestasi berdasarkan Kategori Kegiatan

        // // Chart Prestasi berdasarkan Kategori Kegiatan
        $prestasi_bar_fakultas      = $data_prestasi->barchartPrestasi_fakultas($tahun);
        // // #Chart Prestasi berdasarkan Kategori Kegiatan

        // // Chart Prestasi berdasarkan Kategori Kegiatan
        // $data_mahasiswa          = new Mahasiswa;
        // $mahasiswa_bar           = $data_mahasiswa->chartMahasiswa();
        // // #Chart Prestasi berdasarkan Kategori Kegiatan

        // // Chart Prestasi berdasarkan Tingkat Kegiatan
        // $tingkat_chart           = $data_prestasi->tingkatKegiatanChart();
        // // #Chart Prestasi berdasarkan Tingkat Kegiatan

        // // Chart Prestasi berdasarkan Capaian Prestasi
        // $capaianPrestasi_chart  = $data_prestasi->capaianPrestasiChart();
        // // #Chart Prestasi berdasarkan Capaian Prestasi

        // // Chart Prestasi berdasarkanj enisKegiatanChart
        // $jenisKegiatan_chart     = $data_prestasi->jenisKegiatanChart();
        // // #Chart Prestasi berdasarkan jenisKegiatanChart

        //Total Dashboard Akademik
        $prestasi_internasional     = $data_prestasi->dashboard_internasional($tahun);
        $prestasi_nasional          = $data_prestasi->dashboard_nasional($tahun);
        $prestasi_lokal             = $data_prestasi->dashboard_lokal($tahun);

        //Total Dashboard Non Akademik
        $prestasi_internasional_non = $data_prestasi->dashboard_internasional_non($tahun);
        $prestasi_nasional_non      = $data_prestasi->dashboard_nasional_non($tahun);
        $prestasi_lokal_non         = $data_prestasi->dashboard_lokal_non($tahun);

        $tahun_kegiatan             = PrestasiMahasiswa::selectRaw('year(tgl_pelaksanaan) tahun_kegiatan')->groupBy('tahun_kegiatan')->pluck('tahun_kegiatan','tahun_kegiatan');
        // dd($tahun_kegiatan);
        //#Total Dashboard
        return view('backend/dashboard/dashboard', [
            // 'jenisKegiatan_chart'        => $jenisKegiatan_chart,
            // 'capaianPrestasi_chart'      => $capaianPrestasi_chart,
            // 'tingkat_chart'              => $tingkat_chart,
            'prestasi_bar'                  => $prestasi_bar,
            'prestasi_bar_fakultas'         => $prestasi_bar_fakultas,
            // 'mahasiswa_bar'              => $mahasiswa_bar,
            // 'usersChart'                 => $usersChart,
            'nonakademik'                   => $nonakademik,
            'akademik'                      => $akademik,
            'internasional_nonakademik'     => $internasional_nonakademik,
            'nasional_nonakademik'          => $nasional_nonakademik,
            'lokal_nonakademik'             => $lokal_nonakademik,
            'internasional_akademik'        => $internasional_akademik,
            'nasional_akademik'             => $nasional_akademik,
            'lokal_akademik'                => $lokal_akademik,

            'prestasi_internasional'        => $prestasi_internasional,
            'prestasi_nasional'             => $prestasi_nasional,
            'prestasi_lokal'                => $prestasi_lokal,
            'prestasi_internasional_non'    => $prestasi_internasional_non,
            'prestasi_nasional_non'         => $prestasi_nasional_non,
            'prestasi_lokal_non'            => $prestasi_lokal_non,
            'tahun_kegiatan'                => $tahun_kegiatan,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
