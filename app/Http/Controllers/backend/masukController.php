<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Permission;
use App\User;
use App\Unit;
use App\Mahasiswa;
use Auth;
use File;
use Illuminate\Config;
use App\Http\Requests\prestasiRequest;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use DB;
use Hash;

class masukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.login.index');
    }

    public function masuk(Request $request)
	{
            $usr        = new User;
            $username   = $request->input('email');
			$pwd        = $request->input('password');
            $mantra     = $usr->mantra($username,$pwd);

            if($mantra == 'kosong'){
                $mantra   = $usr->mantras2($username,$pwd);
            }
            $user       = User::where('email',$username)->first();


            //return $mantra;
			if ($mantra != 'kosong' && !$user) {
				//$pwd = str_random(15);
				$data = array(
					'email'                 => $mantra[0]['npm'],
					'name'                  => $mantra[0]['nama'],
                    'password'              => bcrypt($pwd),
                    'status'                => 1,
                    'permissions_id'        => 5,
                    'unit_id'               => $mantra[0]['kode_prodi'],
				);
				$data_mahasiswa = array(
					'npm'               => $mantra[0]['npm'],
                    'nama'              => $mantra[0]['nama'],
                    'tahun_angkatan'    => $mantra[0]['tahun_angkatan'],
                    'tgl_lulus'         => $mantra[0]['tahun_lulus'],
                    'unit_id'           => $mantra[0]['kode_prodi'],
                    'jenis_kelamin'     => $mantra[0]['jenis_kelamin'],
                    'tempat_lahir'      => $mantra[0]['tempat_lahir'],
                    'tanggal_lahir'     => $mantra[0]['tgl_lahir'],
                    'email'             => $mantra[0]['email'],
                    'status_mahasiswa'  => $mantra[0]['status_mahasiswa'],
                    'nomor_ijazah'      => $mantra[0]['nomor_ijazah'],
                    'judul_skripsi'     => $mantra[0]['judul_skripsi'],
                    'gelar'             => $mantra[0]['gelar']
				);

                //array_push($userdata, $mantra->toArray());
				$mahasiswa = Mahasiswa::updateOrCreate(['npm'=> $mantra[0]['npm']], $data_mahasiswa);
				if ($mahasiswa) {
					$user = User::updateOrCreate(['email'=> $mantra[0]['npm']],$data);
					//$grupakses = $user->aksesgrup_id;
					$userId = $user->id;
					if ($user) {
						$userdata = array(
							'email'     => $username,
							'password'  => $pwd,
						);
						if (\Auth::attempt($userdata)) {
								$usr = User::find($userId);
                                $usr->save();
							    $respon = array('status' => true, 'pesan' => ['msg' => 'Berhasil login']);

                                return json_encode($respon);

						} else {
                            $respon = array("status" => false, "pesan" => ['msg' => 'Gagal Login, Username atau Password salah!!']);
                            return redirect('/')->withErrors($respon);

						}
					}
				}
			} else {
                $userdata = array(
                    'email'     => $username,
                    'password'  => $pwd,
                );
                if (\Auth::attempt($userdata)) {
                    $respon = array('status' => true, 'pesan' => ['msg' => 'Berhasil login']);
                    return json_encode($respon);

            }
                $respon = array("status" => false, "pesan" => ['msg' => 'Gagal Login, Username atau Password salah!!']);

                return redirect('/')->withErrors($respon);

			}

		return response()->json($respon);


	}

}
