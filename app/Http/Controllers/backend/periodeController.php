<?php

namespace App\Http\Controllers\backend;


use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Permission;
use App\PeriodeWisuda;
use Auth;
use File;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;

class periodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Grab all the data
        $data           = PeriodeWisuda::all();
        $title          = "Periode Wisuda";
        // Show the page
        return view('backend.periode.index', compact('data','title'));
    }

    public function data()
    {             
        $data = PeriodeWisuda::orderBy('status', 'DESC')->get();
        return DataTables::of($data)
            ->editColumn('status',function($data) {
                if($data->status == 1){
                    $status = '<span class="btn btn-success btn-round waves-effect btn-sm">Aktif</span>';
                }else{
                    $status = '<span class="btn btn-danger btn-round waves-effect btn-sm">Tidak Aktif</span>';
                }
                return $status;
            })
 
            ->addColumn('actions',function($data) {
                $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Edit"><i class="zmdi zmdi-edit"></i></a>';
                $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Delete"><i class="zmdi zmdi-delete"></i></a>';
              
                return $actions;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','status'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $data = PeriodeWisuda::FindOrFail($id);    
        return json_encode($data);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $data               = new PeriodeWisuda;
        $data->bulan        = $request->bulan;
        $data->tahun        = $request->tahun;
        $data->no_terakhir  = 0;
        $data->status       = 1;
        $data->save() ? 1 : 0;
        
        $non_aktif = PeriodeWisuda::whereNotIn('id', [$data->id])->update(['status' => 0]);
        return $non_aktif;

        
    }


    public function edit($user)
    {
        
    }

    public function update(Request $request, $id)
    {

        $data = PeriodeWisuda::find($id);
        $data->bulan        = $request->bulan;
        $data->tahun        = $request->tahun;
        return $data->update() ? 1 : 0;

    }

    public function destroy($id)
    {
        $data = PeriodeWisuda::find($id);
        return $data->delete() ? 1 : 0;
    }
}
