<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Permission;
use App\User;
use App\CapaianPembelajaran;
use App\Unit;
use Auth;
use File;
use Illuminate\Config;
use Illuminate\Http\Request;
use App\Http\Requests\cpmRequest;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use DB;
use Hash;

class capaianPembelajaranController extends Controller
{
    public function index()
    {
        // Grab all the data
        $data                       = User::all();
        $CapaianPembelajarans       = CapaianPembelajaran::all();
        $units                      = Unit::all();
        $title                      = "Capaian Pembelajaran";
        $prodi                      = Unit::select('id','nama_unit', 'jenis_pendidikan')->whereNotNull('parent_unit_id')->get();
        // Show the page
        return view('backend.capaian_pembelajaran.index', compact('data','title','CapaianPembelajarans','units','prodi'));
    }

    public function data()
    {
       
        DB::statement(DB::raw('set @rownum=0'));       
        if(Auth::user()->permissions_id == 4 ){ // Prodi
            $data = CapaianPembelajaran::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'capaian_pembelajarans.id','jenis_cp', 'unit_id','isi_cp', 'isi_cp_en'])
                    ->join('units','units.id','capaian_pembelajarans.unit_id')
                    ->where('units.id', Auth::user()->unit_id)
                    ->orderBy('capaian_pembelajarans.id', 'DESC')->get();
        }else if(Auth::user()->permissions_id == 3 ){ // Fakultas
            $data = DB::table('units AS p_unit')
                    ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
                    ->join('CapaianPembelajaran','CapaianPembelajaran.unit_id','c_unit.id')
                    ->where('p_unit.id', Auth::user()->unit_id)
                    ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'capaian_pembelajarans.id','jenis_cp', 'unit_id','isi_cp', 'isi_cp_en'])
                    ->orderBy('capaian_pembelajarans.id', 'DESC')->get();
        }else{
            $data = CapaianPembelajaran::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','jenis_cp', 'unit_id','isi_cp', 'isi_cp_en'])->orderBy('id', 'DESC')->get();
        }

        return DataTables::of($data)
            ->addColumn('unit',function(CapaianPembelajaran $data) {
                if($data->unit->jenis_pendidikan){
                    $unit = $data->unit->nama_unit." | ".$data->unit->jenis_pendidikan;
                }else{
                    $unit = $data->unit->nama_unit;

                }
                return $unit;
            })
            ->addColumn('isi_cp',function(CapaianPembelajaran $data) {
                return "<div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>$data->isi_cp</div>";
            })
            ->addColumn('isi_cp_en',function(CapaianPembelajaran $data) {
                return "<div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>$data->isi_cp_en</div>";
            })
 
            ->addColumn('actions',function($data) {
                $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Detail"><i class="zmdi zmdi-edit"></i></a>';
                $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Delete"><i class="zmdi zmdi-delete"></i></a>';

                return $actions;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','unit','isi_cp','isi_cp_en'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $data = CapaianPembelajaran::FindOrFail($id);    
        return json_encode($data);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(cpmRequest $request)
    {

        $data = new CapaianPembelajaran();

        $data->jenis_cp             = $request->jenis_cp;
        $data->isi_cp               = $request->isi_cp;
        $data->isi_cp_en            = $request->isi_cp_en;
        $data->unit_id              = Auth::user()->unit_id;
        if(\Auth::user()->permissions_id == 1){
            $data->unit_id              = $request->unit_id;
        }

        return $data->save() ? 1 : 0;

        
    }


    public function edit($user)
    {
        
    }

    public function update(cpmRequest $request, $id)
    {

        $data = CapaianPembelajaran::find($id);
        $data->jenis_cp             = $request->jenis_cp;
        $data->isi_cp               = $request->isi_cp;
        $data->isi_cp_en            = $request->isi_cp_en;
        $data->unit_id              = Auth::user()->unit_id;
        

        return $data->update() ? 1 : 0;

    }

    public function destroy($id)
    {
        $data = CapaianPembelajaran::find($id);
        return $data->delete() ? 1 : 0;
    }
}
