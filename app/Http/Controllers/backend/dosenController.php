<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Permission;
use App\User;
use App\Dosen;
use App\Unit;
use Auth;
use File;
use Illuminate\Config;
use App\Http\Requests\dosenRequest;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use DB;
use Hash;

class dosenController extends Controller
{
    public function index()
    {
        // Grab all the data
        $title                      = "Dosen";
        $dosens                     = Dosen::all();
        // Show the page
        return view('backend.dosen.index', compact('data','title','dosens'));
    }

    public function data()
    {
        if(request()->ajax()) {
            return datatables()->of(Dosen::select('*'))
            ->addIndexColumn()
            ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $data = Dosen::FindOrFail($id);    
        return json_encode($data);
    }

    
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(dosenRequest $request)
    {

     
    }


    public function edit($user)
    {
        
    }

    public function update(prestasiRequest $request, $id)
    {

        $data = Dosen::find($id);
    
        
        $data->nidn         = $request->nidn;
        $data->nama_dosen   = $request->nama_dosen;

        return $data->update() ? 1 : 0;

    }

    public function destroy($id)
    {
        $data = Dosen::find($id);

        return $data->delete() ? 1 : 0;
    }
}
