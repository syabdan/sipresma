<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Permission;
use App\User;
use App\PrestasiMahasiswa;
use App\CapaianPembelajaran;
use App\Unit;
use App\Mahasiswa;
use App\Dosen;
use Auth;
use File;
use Illuminate\Config;
use App\Http\Requests\prestasiRequest;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use DB;
use Hash;
use PDF;
use Illuminate\Support\Str;

class skpiController extends Controller
{
    public function index()
    {
        // Grab all the data
        $data                       = User::all();
        $prestasi                   = PrestasiMahasiswa::all();
        $units                      = Unit::all();
        $dosens                     = Dosen::all();
        $title                      = "Surat Keterangan Pendamping Ijazah";
        // Show the page
        return view('backend.skpi.index', compact('data','title','prestasi','units','dosens'));
    }

    public function non()
    {
        // Grab all the data
        $data                       = User::all();
        $prestasi                   = PrestasiMahasiswa::all();
        $units                      = Unit::all();
        $dosens                     = Dosen::all();
        $title                      = "Surat Keterangan Pendamping Ijazah";
        // Show the page
        return view('backend.skpi.non', compact('data','title','prestasi','units','dosens'));
    }

    public function data()
    {
        $d = DB::table('units AS p_unit')
        ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
        ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
        ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
        ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->where('prestasi_mahasiswas.status_verifikasi_prodi', 1)
        ->where('prestasi_mahasiswas.status_verifikasi_uir', 1)
        ->select(['mahasiswas.id as id','mahasiswas.npm', 'mahasiswas.nama','mahasiswas.tahun_angkatan','mahasiswas.tahun_lulus','mahasiswas.unit_id','mahasiswas.tempat_lahir','mahasiswas.tanggal_lahir','mahasiswas.nomor_ijazah','mahasiswas.judul_skripsi','mahasiswas.judul_skripsi_en','mahasiswas.gelar','c_unit.nama_unit as prodi','p_unit.nama_unit as fakultas','c_unit.akreditasi', 'c_unit.jenis_pendidikan','c_unit.no_sertifikat_akre'
        ,'capaian_pembelajarans.isi_cp','capaian_pembelajarans.isi_cp_en'
        ,'prestasi_mahasiswas.nama_kegiatan','prestasi_mahasiswas.nama_kegiatan_en','prestasi_mahasiswas.npm as npmprestasi', 'mahasiswas.status_validasi', 'mahasiswas.nomor_ijazah_pin', 'mahasiswas.no_skpi'
        ])
        ->groupBy('npmprestasi');
        if(\Auth::user()->permissions_id == 4){
            $d->where('mahasiswas.unit_id', \Auth::user()->unit_id);
        }
        $data = $d->get();
        return DataTables::of($data)

            ->addColumn('actions',function($data) {
            if(Auth::user()->permissions_id == 2 || Auth::user()->permissions_id == 1){ // UIR
                $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-edit"></i></a>';
            }else{
                if($data->status_validasi == 1){
                    $link = 'href="skpi/cetak/'.$data->npm.'"';
                    $warna = "btn-success";
                }else{
                    $link = '';
                    $warna = "btn-default";
                }

                // $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-delete"></i></a>';

                $actions = '<a '.$link.' target="_blank" class="btn btn-icon btn-neutral btn-icon-mini '.$warna.'" ><i class="zmdi zmdi-print" title="Cetak"></i></a>';
                $actions .= '<a onclick="printForm('.$data->npm.')" class="btn btn-icon btn-neutral btn-icon-mini btn-warning"><i class="zmdi zmdi-edit" title="Edit Penandatangan"></i></a>';
                if(\Auth::user()->permissions_id == 4){
                    $actions .= '<a onclick="validasiForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini btn-success"><i class="zmdi zmdi-lock" title="Validasi Identitas"></i></a>';
                }
            }
                return $actions;
            })
            ->addColumn('ttl',function($data) {
                return $data->tempat_lahir.', '.$data->tanggal_lahir;
            })
            ->addColumn('isi_cp',function($data) {
                return strip_tags($data->isi_cp);
            })
            ->addIndexColumn()
            ->rawColumns(['actions','ttl'])
            ->make(true);
    }

    public function data_skpinon()
    {

        $d = DB::table('units AS p_unit')
        ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
        ->join('prestasi_mahasiswas','prestasi_mahasiswas.unit_id','c_unit.id')
        ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
        ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->where('prestasi_mahasiswas.status_verifikasi_prodi', 1)
        ->whereNull('prestasi_mahasiswas.nama_kegiatan_en')
        ->whereNull('prestasi_mahasiswas.status_verifikasi_uir')
        ->select(['mahasiswas.id as id','mahasiswas.npm', 'mahasiswas.nama','mahasiswas.tahun_angkatan','mahasiswas.tahun_lulus','mahasiswas.unit_id','mahasiswas.tempat_lahir','mahasiswas.tanggal_lahir','mahasiswas.nomor_ijazah','mahasiswas.judul_skripsi','mahasiswas.judul_skripsi_en','mahasiswas.gelar','c_unit.nama_unit as prodi','p_unit.nama_unit as fakultas','c_unit.akreditasi', 'c_unit.jenis_pendidikan','c_unit.no_sertifikat_akre'
        ,'capaian_pembelajarans.isi_cp','capaian_pembelajarans.isi_cp_en'
        ,'prestasi_mahasiswas.nama_kegiatan','prestasi_mahasiswas.nama_kegiatan_en','prestasi_mahasiswas.npm as npmprestasi', 'mahasiswas.status_validasi', 'mahasiswas.nomor_ijazah_pin', 'mahasiswas.no_skpi'
        ])
        ->groupBy('npmprestasi');
        if(\Auth::user()->permissions_id == 4){
            $d->where('mahasiswas.unit_id', \Auth::user()->unit_id);
        }
        $data = $d->get();

        return DataTables::of($data)

            ->addColumn('actions',function($data) {
                $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Edit"><i class="zmdi zmdi-edit"></i></a>';
                // $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-delete"></i></a>';
                // $actions .= '<a onclick="printForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-print"></i></a>';
                // $actions .= '<a href="skpi/cetak/'.$data->npm.'" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-print" target="_blank"></i></a>';

                return $actions;
            })
            ->addColumn('ttl',function($data) {
                return $data->tempat_lahir.', '.$data->tanggal_lahir;
            })
            ->addColumn('isi_cp',function($data) {
                return strip_tags($data->isi_cp);
            })
            ->addIndexColumn()
            ->rawColumns(['actions','ttl'])
            ->make(true);
    }

    public function cetak_skpi($npm){

        $mhs = Mahasiswa::select('mahasiswas.id as id',
        'npm', 
        'nama',
        'tahun_angkatan',
        'tahun_lulus',
        'mahasiswas.unit_id as unit_id',
        'tempat_lahir','tanggal_lahir',
        'nomor_ijazah_pin','judul_skripsi',
        'judul_skripsi_en',
        'units.gelar as gelar',
        'units.gelar_en as gelar_en',
        'units.singkatan_gelar as singkatan_gelar',
        'units.singkatan_gelar_en as singkatan_gelar_en',
        'nama_unit','nama_unit_en','akreditasi','no_sertifikat_akre',
        'tanggal_lulus_sidang',
        'tahun_angkatan',
        'no_skpi')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->where('npm', $npm)
        ->orderBy('mahasiswas.id', 'DESC')->first();
        
        $cp_umum = Mahasiswa::select(
        'isi_cp',
        'isi_cp_en')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->where('npm', $npm)
        ->where('jenis_cp', 'Keterampilan Umum')
        ->orderBy('mahasiswas.id', 'DESC')->first();
        
        $cp_khusus = Mahasiswa::select(
        'isi_cp',
        'isi_cp_en')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->where('npm', $npm)
        ->where('jenis_cp',  'Keterampilan Khusus')
        ->orderBy('mahasiswas.id', 'DESC')->first();
        
        $cp_pengetahuan = Mahasiswa::select(
        'isi_cp',
        'isi_cp_en')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->where('npm', $npm)
        ->where('jenis_cp',  'Penguasaan Pengetahuan')
        ->orderBy('mahasiswas.id', 'DESC')->first();
        
        $cp_sikapkhusus = Mahasiswa::select(
        'isi_cp',
        'isi_cp_en')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->where('npm', $npm)
        ->where('jenis_cp',  'Sikap Khusus')
        ->orderBy('mahasiswas.id', 'DESC')->first();

        $data_prestasi = Mahasiswa::
         leftjoin('prestasi_mahasiswas', 'prestasi_mahasiswas.npm', 'mahasiswas.npm')
        ->where('mahasiswas.npm', $npm)
        ->where('prestasi_mahasiswas.status_verifikasi_prodi', 1)
        ->where('prestasi_mahasiswas.status_verifikasi_uir', 1)
        ->whereNotNull('prestasi_mahasiswas.nama_kegiatan_en')
        ->orderBy('mahasiswas.id', 'DESC')->get();

        $pimpinan = DB::table('units AS p_unit')
        ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
        ->join('mahasiswas','mahasiswas.unit_id','c_unit.id')
        ->join('dosens', 'dosens.id', 'p_unit.dosen_id')
        ->where('npm', $npm)
        ->select('p_unit.*','gelar_depan','gelar_belakang','nama_dosen','nip')
        ->first();

        $namapdf = "laporan_skpi_".Str::slug($mhs->nama.'-');
        $path = storage_path('app/');
        $file   =   $path.$namapdf.'.pdf';

		if(File::exists($file)){
			File::delete($file);
		}

        $namaFile = $namapdf.'.pdf';

		$upd2               =   Mahasiswa::where('id',$mhs->id)->first();
		$upd2->file_skpi    =   $namaFile;
		$upd2->update();


		$linkFile=url('skpi/unduh/'.$mhs->npm.'/'.$mhs->id.'/'. Str::slug($mhs->nama.'-').'/'.$namaFile);
        $qrcode = \QrCode::size(500)
        ->format('svg')
        ->generate($linkFile, public_path('images/qrcode.svg'));

     

        $pdf = PDF::loadView('backend.skpi.report', compact('mhs','data_prestasi','pimpinan','qrcode','cp_umum','cp_khusus','cp_pengetahuan','cp_sikapkhusus'));

        $output = $pdf->output();
        \Storage::disk('local')->put($namaFile, $output); // Simpan file

        return $pdf->stream("Laporan_SKPI_$mhs->nama.pdf");
    }

    public function cetak_skpi_ex($npm, $dosen_id){

        $mhs = Mahasiswa::select('mahasiswas.id as id',
        'npm', 
        'nama',
        'tahun_angkatan',
        'tahun_lulus',
        'mahasiswas.unit_id as unit_id',
        'tempat_lahir','tanggal_lahir',
        'nomor_ijazah_pin','judul_skripsi',
        'judul_skripsi_en',
        'units.gelar as gelar',
        'units.gelar_en as gelar_en',
        'units.singkatan_gelar as singkatan_gelar',
        'units.singkatan_gelar_en as singkatan_gelar_en',
        'nama_unit','nama_unit_en','akreditasi','no_sertifikat_akre',
        'tanggal_lulus_sidang',
        'tahun_angkatan',
        'no_skpi')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->where('npm', $npm)
        ->orderBy('mahasiswas.id', 'DESC')->first();
        
        $cp_umum = Mahasiswa::select(
            'isi_cp',
            'isi_cp_en')
            ->join('units', 'units.id', 'mahasiswas.unit_id')
            ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
            ->where('npm', $npm)
            ->where('jenis_cp', 'Keterampilan Umum')
            ->orderBy('mahasiswas.id', 'DESC')->first();
            
        $cp_khusus = Mahasiswa::select(
            'isi_cp',
            'isi_cp_en')
            ->join('units', 'units.id', 'mahasiswas.unit_id')
            ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
            ->where('npm', $npm)
            ->where('jenis_cp',  'Keterampilan Khusus')
            ->orderBy('mahasiswas.id', 'DESC')->first();
        
        $cp_pengetahuan = Mahasiswa::select(
        'isi_cp',
        'isi_cp_en')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->where('npm', $npm)
        ->where('jenis_cp',  'Penguasaan Pengetahuan')
        ->orderBy('mahasiswas.id', 'DESC')->first();
        
        $cp_sikapkhusus = Mahasiswa::select(
        'isi_cp',
        'isi_cp_en')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->join('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->where('npm', $npm)
        ->where('jenis_cp',  'Sikap Khusus')
        ->orderBy('mahasiswas.id', 'DESC')->first();

        $data_prestasi = Mahasiswa::
         leftjoin('prestasi_mahasiswas', 'prestasi_mahasiswas.npm', 'mahasiswas.npm')
        ->where('mahasiswas.npm', $npm)
        ->where('prestasi_mahasiswas.status_verifikasi_prodi', 1)
        ->where('prestasi_mahasiswas.status_verifikasi_uir', 1)
        ->whereNotNull('prestasi_mahasiswas.nama_kegiatan_en')
        ->orderBy('mahasiswas.id', 'DESC')->get();

        $pimpinan = Dosen::join('units', 'units.id', 'dosens.unit_id')
        ->where('dosens.id', $dosen_id)
        ->first();

        $namapdf = "laporan_skpi_".Str::slug($mhs->nama.'-');
        $path = storage_path('app/');
        $file   =   $path.$namapdf.'.pdf';

		if(File::exists($file)){
			File::delete($file);
		}

        $namaFile = $namapdf.'.pdf';

		$upd2               =   Mahasiswa::where('id',$mhs->id)->first();
		$upd2->file_skpi    =   $namaFile;
		$upd2->update();


		$linkFile=url('skpi/unduh/'.$mhs->npm.'/'.$mhs->id.'/'. Str::slug($mhs->nama.'-').'/'.$namaFile);
        $qrcode = \QrCode::size(500)
        ->format('svg')
        ->generate($linkFile, public_path('images/qrcode.svg'));

  

        $pdf = PDF::loadView('backend.skpi.report', compact('mhs','data_prestasi','pimpinan','qrcode','cp_umum','cp_khusus','cp_pengetahuan','cp_sikapkhusus'));

        $output = $pdf->output();
        \Storage::disk('local')->put($namaFile, $output); // Simpan file

        return $pdf->stream("Laporan_SKPI_$mhs->nama.pdf");
    }


    public function unduh($npm,$id,$nama=NULL,$file=NULL){
        $file = Mahasiswa::where([['id',$id],['npm',$npm]])->first();

		if(empty($file)){
            return 'Maaf, Data Tidak Valid !';
        }else{
            return response()->file(storage_path('app/'.$file->file_skpi));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $data = Mahasiswa::select('mahasiswas.id', 'mahasiswas.npm', 'mahasiswas.nama','mahasiswas.tahun_angkatan','mahasiswas.tahun_lulus','mahasiswas.unit_id','mahasiswas.tempat_lahir','mahasiswas.tanggal_lahir','mahasiswas.gelar','units.nama_unit','units.akreditasi','units.no_sertifikat_akre'
        ,'capaian_pembelajarans.isi_cp','capaian_pembelajarans.isi_cp_en'
        ,'prestasi_mahasiswas.nama_kegiatan','prestasi_mahasiswas.nama_kegiatan_en'
        )

        ->leftjoin('prestasi_mahasiswas', 'prestasi_mahasiswas.npm', 'mahasiswas.npm')
        ->leftjoin('capaian_pembelajarans', 'capaian_pembelajarans.unit_id', 'mahasiswas.unit_id')
        ->join('units', 'units.id', 'mahasiswas.unit_id')
        ->where('mahasiswas.id',$id)
        ->orderBy('mahasiswas.id', 'DESC')->first();

        // Ambil API Alumni untuk data skripsi
        $api_skripsi    = api_skripsi_by_npm($data['npm']);
        $data_skripsi   = collect(json_decode($api_skripsi))->first();
        $data2          = json_encode($data_skripsi ?? array(0 => 'kosong'));

        return json_encode(array_merge(json_decode($data, true),json_decode($data2 , true)));
    }


    public function showPrestasi($npm)
    {

        $data = PrestasiMahasiswa::select('prestasi_mahasiswas.*')
        ->where('npm', $npm)
        ->where('status_verifikasi_prodi', 1)->get();

        return json_encode($data);

    }

    // public function validasi(Request $request){

    //     $data_pres_mahasiswa = array(
    //         'nama_kegiatan_en'    => $request->nama_kegiatan_en
    //     );
    //     DB::table('prestasi_mahasiswas')->where('npm', $request->npm)->update($data_pres_mahasiswa);


    //     $data_mahasiswa = array(
    //         'npm'               => $request->npm,
    //         'nama'              => $request->nama,
    //         'judul_skripsi'     => $request->judul_skripsi,
    //         'judul_skripsi_en'  => $request->judul_skripsi_en
    //     );
	//     $mahasiswa = Mahasiswa::where('npm',$request->npm)->update($data_mahasiswa);

    //     // return redirect('skpi');
    // }

    public function update( Request $request, $id)
    {

        $data = Mahasiswa::find($id);
        $data->npm                  = $request->npm;
        $data->nama                 = $request->nama;
        $data->judul_skripsi        = $request->judul_skripsi;
        $data->judul_skripsi_en     = $request->judul_skripsi_en;
        $data->nomor_ijazah         = $request->nomor_ijazah;
        $data->tanggal_lulus_sidang = $request->tanggal_lulus;
        $data->lama_studi           = siswa_waktu($request->tanggal_lulus, $request->angkatan);

        $data->update() ? 1 : 0;

        $data_pres_mahasiswa = array(
            // 'nama_kegiatan_en'          => $request->nama_kegiatan_en,
            'status_verifikasi_uir'     => 1
        );
        DB::table('prestasi_mahasiswas')->where('npm', $request->npm)->update($data_pres_mahasiswa);

        $hitung = count($request->id_prestasi);
        // dd($hitung);
        for($i=0; $i<$hitung; $i++)
        {
            $data = PrestasiMahasiswa::find($request->id_prestasi[$i]);
            $data->nama_kegiatan                = $request->nama_kegiatan_trans[$i];
            $data->nama_kegiatan_en             = $request->nama_kegiatan_en_trans[$i];
            $data->update();
        }

    }

}
