<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Permission;
use App\User;
use App\Dosen;
use App\Unit;
use Auth;
use File;
use Illuminate\Config;
use App\Http\Requests\unitRequest;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use DB;
use Hash;

class UnitController extends Controller
{
    public function index()
    {
        // Grab all the data
        $data                       = Unit::get();
        // $dosens                     = Dosen::get();
            $d = DB::table('units AS p_unit')
            ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_unit_id')
            ->join('dosens', 'dosens.unit_id', 'c_unit.id');
            if(\Auth::user()->permissions_id == 3){
                $d->where('p_unit.id', \Auth::user()->unit_id);
            }
            if(\Auth::user()->permissions_id == 4){
                $d->where('c_unit.id', \Auth::user()->unit_id);
            }
            $d->select('dosens.id','gelar_depan','gelar_belakang','nama_dosen');

            $dosens = $d->get();
        $title                      = "Unit";
        $p_unit                     = Unit::where('id', \Auth::user()->unit_id)->whereNull('parent_unit_id')->first();
        if($p_unit){
            $nama_unit = 'Prodi / Fakultas';
        }else{
            $nama_unit = 'Prodi';
        }
        // Show the page
        return view('backend.unit.index', compact('data','title','dosens','nama_unit','p_unit'));
    }

    public function data()
    {

        $data = Unit::select(['units.*', 'dosens.nama_dosen','dosens.nidn','dosens.gelar_depan','dosens.gelar_belakang'])
                ->leftjoin('dosens','dosens.id','units.dosen_id')
                // ->leftjoin('dosens','dosens.nidn','units.nidn_pimpinan')
                ->where('units.id', \Auth::user()->unit_id)
                ->orderBy('units.id', 'DESC')->get();

        if(\Auth::user()->permissions_id == 4 ){ // Prodi
            $data = Unit::select(['units.*', 'dosens.nama_dosen','dosens.nidn','dosens.gelar_depan','dosens.gelar_belakang'])
                    ->leftjoin('dosens','dosens.id','units.dosen_id')
                    ->where('units.id', \Auth::user()->unit_id)
                    ->orderBy('units.id', 'DESC')->get();
        }else if(\Auth::user()->permissions_id == 3 ){ // Fakultas
        $data_prodi =  Unit::select(['units.*', 'dosens.nama_dosen','dosens.nidn','dosens.gelar_depan','dosens.gelar_belakang'])
                ->leftjoin('dosens','dosens.id','units.dosen_id')
                ->where('units.parent_unit_id', \Auth::user()->unit_id)
                ->orderBy('units.id', 'DESC')->get();

        $data_fakultas = Unit::select(['units.*', 'dosens.nama_dosen','dosens.nidn','dosens.gelar_depan','dosens.gelar_belakang'])
                ->leftjoin('dosens','dosens.id','units.dosen_id')
                ->where('units.id', \Auth::user()->unit_id)
                ->orderBy('units.id', 'DESC')->get();

        $merged = $data_prodi->merge($data_fakultas);

        $data = $merged->all();

        }else{
            $data = Unit::select(['units.*', 'dosens.nama_dosen','dosens.nidn','dosens.gelar_depan','dosens.gelar_belakang'])
            ->leftjoin('dosens','dosens.id','units.dosen_id')
            ->orderBy('units.id', 'DESC')->get();
        }

        return DataTables::of($data)

            ->addColumn('actions',function($data) {
                $actions = '<a onclick="editForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini" title="Edit"><i class="zmdi zmdi-edit"></i></a>';
                // $actions .= '<a onclick="deleteForm('.$data->id.')" class="btn btn-icon btn-neutral btn-icon-mini"><i class="zmdi zmdi-delete"></i></a>';

                return $actions;
            })

            ->addColumn('dokumen_unit',function($data) {
                $file = '<a href='. url('unit/ambilFile_unit/'.$data->id) .' target="_blank"><i class="zmdi zmdi-download"></i> Lihat</a>';
                if($data->dokumen_unit){
                    return $file;

                }else{
                    return '-';
                }
            })

            ->addColumn('dokumen_akre',function($data) {
                $file = '<a href='. url('unit/ambilFile_akre/'.$data->id) .' target="_blank"><i class="zmdi zmdi-download"></i> Lihat</a>';
                if($data->dokumen_akre){
                    return $file;

                }else{
                    return '-';
                }
            })

            ->addColumn('pimpinan',function($data) {
                $pimpinan = $data->gelar_depan." ".$data->nama_dosen." ".$data->gelar_belakang;
                if($pimpinan){
                    return $pimpinan;

                }else{
                    return '-';
                }
            })

            ->addIndexColumn()
            ->rawColumns(['actions','dokumen_unit','dokumen_akre', 'pimpinan'])
            ->make(true);
    }

    public function ambilFile_unit($id)
    {
        $data = Unit::where('id',$id)->first();
        return response()->file(public_path('backend/assets/images/unit/'.$data->dokumen_unit));
    }

    public function ambilFile_akre($id)
    {
        $data = Unit::where('id',$id)->first();
        return response()->file(public_path('backend/assets/images/akreditasi/'.$data->dokumen_akre));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {
        $data = Unit::FindOrFail($id);
        return json_encode($data);
    }


    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(unitRequest $request)
    {

        $data = new Unit();

        if ($file = $request->file('dokumen_akre')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];

            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);

            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();

            $destinationPath = public_path('backend/assets/images/akreditasi/');

            $file->move($destinationPath, $gambarName);
            $data->dokumen_akre = $gambarName;
        }

        if ($file = $request->file('dokumen_unit')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];

            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);

            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();

            $destinationPath = public_path('backend/assets/images/unit/');

            $file->move($destinationPath, $gambarName);
            $data->dokumen_akre = $gambarName;
        }

        $data->nama_unit                    = $request->nama_unit;
        $data->nama_unit_en                 = $request->nama_unit_en;
        $data->no_sk_unit                   = $request->no_sk_unit;
        $data->tgl_sk_unit                  = $request->tgl_sk_unit;
        $data->gelar                        = $request->gelar;
        $data->singkatan_gelar              = $request->singkatan_gelar;
        $data->gelar_en                     = $request->gelar_en;
        $data->singkatan_gelar_en           = $request->singkatan_gelar_en;
        $data->akreditasi                   = $request->akreditasi;
        $data->no_sertifikat_akre           = $request->no_sertifikat_akre;
        $data->dosen_id                     = $request->nidn_pimpinan;

        return $data->save() ? 1 : 0;
    }


    public function edit($user)
    {

    }

    public function update(unitRequest $request, $id)
    {

        $data = Unit::find($id);
        if ($file = $request->file('dokumen_akre')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];

            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);

            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();

            $destinationPath = public_path('backend/assets/images/akreditasi/');

            $file->move($destinationPath, $gambarName);
            $data->dokumen_akre = $gambarName;
        }

        if ($file = $request->file('dokumen_unit')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];

            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);

            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();

            $destinationPath = public_path('backend/assets/images/unit/');

            $file->move($destinationPath, $gambarName);
            $data->dokumen_akre = $gambarName;
        }

        $data->nama_unit                    = $request->nama_unit;
        $data->nama_unit_en                 = $request->nama_unit_en;
        $data->no_sk_unit                   = $request->no_sk_unit;
        $data->tgl_sk_unit                  = $request->tgl_sk_unit;
        $data->gelar                        = $request->gelar;
        $data->singkatan_gelar              = $request->singkatan_gelar;
        $data->gelar_en                     = $request->gelar_en;
        $data->singkatan_gelar_en           = $request->singkatan_gelar_en;
        $data->akreditasi                   = $request->akreditasi;
        $data->no_sertifikat_akre           = $request->no_sertifikat_akre;
        $data->dosen_id                     = $request->nidn_pimpinan;

        return $data->update() ? 1 : 0;

    }

    public function destroy($id)
    {
        $data = Unit::find($id);
        File::delete(public_path('backend/assets/images/unit/'.$data->dokumen_unit));
        File::delete(public_path('backend/assets/images/akreditasi/'.$data->dokumen_akre));

        return $data->delete() ? 1 : 0;
    }
}
