<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Menu;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $menu =  Menu::orderBy('posisi','asc')->whereNull('parent_menu_id')->orderBy('posisi', 'asc')->get();
        View::share('menu', $menu);

    }

    public function ValidasiData($input,$rule)
    {
        $messages = [
                      'required'=> 'Data :attribute Tidak Boleh Kosong',
                      'string'  => 'Data :attribute Harus Dalam Bentuk Text',
                      'file'    => 'Data :attribute Harus Dalam Bentuk File',
                      'mimes'   => 'Supported file format for :attribute are :mimes',
                      'max'     => 'The :attribute must have a maximum length of :max',
                    ];
        return Validator::make($input,$rule,$messages);
    }

}
