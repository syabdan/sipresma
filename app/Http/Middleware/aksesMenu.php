<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use App\Menu;
use App\Permission;

class aksesMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user			= \Auth::user();
        $current	=	explode(".", Route::currentRouteName());
        
        $submenu	=	Menu::where('link', $current[0])->orderBy('id','asc')->first();
        // dd($submenu);

        if($submenu !== NULL){
            // foreach($submenu as $smn){
                if($submenu->tampil){
                    $data_sections_arr = explode(",", \Auth::user()->permissionsGroup->data_menus);

                    if(in_array($submenu->id,$data_sections_arr)){
                    // if($aksessub->first()){
                        return $next($request);
                    }else{
                        return redirect('/dashboard');
                        // return response()->json(array('status' => false, 'pesan' => ['msg' => 'Tidak Ada akses']));
                    }
                }else{
                    return $next($request);
                }
            // }
        }else{
            return redirect('/');
            // return response()->json(array('status' => false, 'pesan' => ['msg' => 'Tidak Ada akses']));
        }
    }
}
