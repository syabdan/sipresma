<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class unitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_unit' => 'required',
            'nama_unit_en' => 'required',
            'gelar' => 'required',
            'gelar_en' => 'required',
            'singkatan_gelar' => 'required',
            'singkatan_gelar_en' => 'required',
            'nidn_pimpinan' => 'required',
        ];
    }
}
