<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class prestasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_kegiatan' => 'required',
            'kategori_kegiatan' => 'required',
            'jenis_kegiatan' => 'required',
            'tgl_pelaksanaan' => 'required',
            'tempat_penyelenggara' => 'required',
            'nama_penyelenggara' => 'required',
            'tingkat' => 'required',
            'jenis_prestasi' => 'required',
            'capaian_prestasi' => 'required',
            'kategori_peserta' => 'required',
        ];
    }
}
