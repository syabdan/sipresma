<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodeWisuda extends Model
{
    protected $fillable = [
        'bulan', 
        'tahun', 
        'no_terakhir',
        'status',
    ];
}
