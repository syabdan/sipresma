<?php
function tanggal_indonesia($tgl, $tampil_hari=true){
	$nama_hari=array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
	$nama_bulan=array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun=substr($tgl,0,4);
	$bulan=$nama_bulan[(int)substr($tgl,5,2)];
	$tanggal=substr($tgl,8,2);

	$text="";

	if($tampil_hari){
		$urutan_hari=date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));

		$hari=$nama_hari[$urutan_hari];
		$text .= $hari.", ";
	}

	$text .=$tanggal." ".$bulan." ".$tahun;

	return $text;
}

function tanggal_english($tgl, $tampil_hari=true){
	$nama_hari=array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
	$nama_bulan=array(1=>"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

	$tahun=substr($tgl,0,4);
	$bulan=$nama_bulan[(int)substr($tgl,5,2)];
	$tanggal=substr($tgl,8,2);

	$text="";

	if($tampil_hari){
		$urutan_hari=date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));

		$hari=$nama_hari[$urutan_hari];
		$text .= $hari.", ";
	}

	$text .=$tanggal." ".$bulan." ".$tahun;

	return $text;
}

// function getBulan($tgl){
// 	$nama_bulan=array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

// 	$bulan=$nama_bulan[(int)substr($tgl,5,2)];

// 	return $bulan;
// }

function tanggal_indonesia2($tgl, $tampil_hari=true){
	$nama_hari=array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
	$nama_bulan=array(1=>"Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des");

	$tahun=substr($tgl,0,4);
	$bulan=$nama_bulan[(int)substr($tgl,5,2)];
	$tanggal=substr($tgl,8,2);

	$text="";

	if($tampil_hari){
		$urutan_hari=date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));

		$hari=$nama_hari[$urutan_hari];
		$text .= $hari.", ";
	}

	$text .=$tanggal." ".$bulan." ".$tahun;

	return $text;
}

function getBulanRomawi ($bln){
	switch($bln){
		case 1:
			return "I";
		break;
		case 2:
			return "II";
		break;
		case 3:
			return "III";
		break;
		case 4:
			return "IV";
		break;
		case 5:
			return "V";
		break;
		case 6:
			return "VI";
		break;
		case 7:
			return "VII";
		break;
		case 8:
			return "VIII";
		break;
		case 9:
			return "IX";
		break;
		case 10:
			return "X";
		break;
		case 11:
			return "XI";
		break;
		case 12:
			return "XII";
		break;
	}
}

function hariInd($tgl){
	$timestamp = strtotime($tgl);
	$day = date('w', $timestamp);

	switch($day){
		case 1:
			$hari='Senin';
		break;
		case 2:
			$hari='Selasa';
		break;
		case 3:
			$hari='Rabu';
		break;
		case 4:
			$hari='Kamis';
		break;
		case 5:
			$hari='Jumat';
		break;
		case 6:
			$hari='Sabtu';
		break;
		case 7:
			$hari='Minggu';
		break;
	}
	return $hari;
}

function terbilang ($nilai){
	$nilai = abs($nilai);
	$huruf = array("", " satu", " dua", " tiga", " empat", " lima", " enam", " tujuh", " delapan", " sembilan", " sepuluh", " sebelas");
	$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = terbilang($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = terbilang($nilai/10)." puluh". terbilang($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . terbilang($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = terbilang($nilai/100) . " ratus" . terbilang($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . terbilang($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = terbilang($nilai/1000) . " ribu" . terbilang($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = terbilang($nilai/1000000) . " juta" . terbilang($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = terbilang($nilai/1000000000) . " milyar" . terbilang(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = terbilang($nilai/1000000000000) . " trilyun" . terbilang(fmod($nilai,1000000000000));
		}
		return trim($temp);
}

function siswa_waktu ($tgl_lulus, $tahun_angkatan, $status_eng=NULL){
	$diff  = date_diff( date_create($tgl_lulus), date_create($tahun_angkatan."-07-01") );

	$data =  $diff->format('%Y tahun %M bulan');
	if($status_eng == 1){
	if($diff->format('%Y') == '01' && $diff->format('%M') == '01'){
		$data =  $diff->format('%Y year %M month');
	}else if($diff->format('%Y') == '01' || $diff->format('%Y') == '0'){
		$data =  $diff->format('%Y year %M months');

	}else if($diff->format('%M') == '01' ||$diff->format('%M') == '0'){
		$data =  $diff->format('%Y years %M month');

	}else if($diff->format('%Y') == '0' && $diff->format('%M') == '0'){
		$data =  $diff->format('%Y year %M month');
	}else{
		$data =  $diff->format('%Y years %M months');
	}
	}

	return $data;
}

function prestasiEng($prestasi){
	switch($prestasi){
		case 'Juara I':
			return 'Gold Medal';
		break;
		case 'Juara II':
			return 'Silver Medal';
		break;
		case 'Juara III':
			return 'Bronze Medal';
		break;
		case 'Harapan I':
			return 'First Consolation Prize Winner';
		break;
		case 'Harapan II':
			return 'Second Consolation Prize Winner';
		break;
		case 'Harapan III':
			return 'Third Consolation Prize Winner';
		break;
		case 'Peserta':
			return 'Participant';
		break;
		case 'Penerima Hibah':
			return 'Beneficiaries';
		break;
		case 'Peserta Terbaik':
			return 'The best participant';
		break;
		case 'Pemakalah Terbaik':
			return 'Best Presenter';
		break;
		case 'Makalah/Paper Terbaik':
			return 'Best Paper';
		break;
		case 'Pemakalah':
			return 'Presenter';
		break;
	}
	// return $capaian;
}
?>
