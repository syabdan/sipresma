<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CapaianPembelajaran extends Model
{
    protected $fillable = [
        'jenis_cp', 
        'isi_cp', 
        'isi_cp_en',
        'unit_id',
    ];

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }
}
