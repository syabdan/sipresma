<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{

    protected $fillable = [
        'id',
        'nama_unit',
        'nama_unit_en',
        'parent_unit_id',
        'no_sk_unit',
        'tgl_sk_unit',
        'jenis_pendidikan',
        'dokumen_unit',
        'gelar',
        'gelar_en',
        'singkatan_gelar',
        'singkatan_gelar_en',
        'akreditasi',
        'no_sertifikat_akre',
        'dokumen_akre',
        'nidn_pimpinan',
        'dosen_id'

    ];

    public function capaian_pembelajaran()
    {
        return $this->belongsTo('App\CapaianPembelajaran', 'id');
    }

    public function dosen()
    {
        return $this->belongsTo('App\Dosen', 'dosen_id');
    }

    public function child()
    {
        return $this->hasMany('App\Unit', 'parent_unit_id')->with('parent');
    }

    public function parent()
    {
        return $this->belongsTo('App\Unit', 'parent_unit_id');
    }
    // static function addIdToQuery($query, $org)
    // {
    //     $query = $query->orWhere('id', 3);
    //     foreach ($org->subUnit as $org)
    //     {
    //         $query = Unit::addIdToQuery($query, $org);
    //     }
    //     return $query;
    // }

    // public function ambil_unit_with_child()
    // {
    //     $query = $this->subUnit();
    //     $query = Unit::addIdToQuery($query, $this);
    //     return $query;
    // }

}
