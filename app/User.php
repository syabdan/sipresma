<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'email',
        'hp',
        'telp',
        'email_verified_at',
        'password',
        'status',
        'permissions_id',
        'unit_id',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // relation with Permissions
    public function permissionsGroup()
    {
        return $this->belongsTo('App\Permission', 'permissions_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }

    public function mantra($npm,$pass)
    {
        $masuk_log = api_login($npm,$pass);

        if($masuk_log[0]->valid == '1'){
            $data = api_mahasiswa_by_npm($npm);
            $sumber = $data;
            $list = json_decode($sumber, true);

            return $list;
        }else{
            return 'kosong';
        }
    }

    public function mantras2($npm,$pass)
    {
        $masuk_log = api_login($npm,$pass);

        if($masuk_log[0]->valid == '1'){
            $data = api_mahasiswa_by_npm_s2($npm);
            $sumber = $data;
            $list = json_decode($sumber, true);

            return $list;
        }else{
            return 'kosong';
        }
    }
}
