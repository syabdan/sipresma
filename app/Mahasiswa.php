<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
	protected $guarded = ['npm'];

    protected $fillable = ['id','npm','nama','tahun_angkatan','unit_id','jenis_kelamin','tempat_lahir','tanggal_lahir','email','status_mahasiswa','nomor_ijazah','judul_skripsi','judul_skripsi_en','gelar','gelar_en','singkatan_gelar','singkatan_gelar_en','tanggal_lulus_sidang','lama_studi','total_prestasi','nomor_ijazah_pin','status_validasi'];

    public function unit()
    {
        return $this->belongsTo('App\Unit','unit_id');
    }

    public function chartMahasiswa(){

    	$category = [];

    	$series[0]['name'] = 'Lulus';
    	$series[1]['name'] = 'Aktif';

        $data_mhs = $this::join('units','units.id','mahasiswas.unit_id')->groupBy('unit_id')->get();
        foreach($data_mhs as $r){

            $category[] = $r->unit->nama_unit;
            $series[0]['data'][] = Self::where('status_mahasiswa', '=', 'L')->where('unit_id', $r->unit_id)->groupBy('status_mahasiswa')->groupBy('unit_id')
            ->count();
    		$series[1]['data'][] = Self::where('status_mahasiswa', '=', 'A')->where('unit_id',$r->unit_id)->groupBy('status_mahasiswa')->groupBy('unit_id')
            ->count();
        }

    	return ['category' => $category, 'series' => $series];
    }
}
