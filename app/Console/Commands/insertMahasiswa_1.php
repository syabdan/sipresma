<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mahasiswa;
use DB;

class insertMahasiswa_1 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:mahasiswa_1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert data mahasiswa from API Sikad to database sipresma';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            $this->info('Proses Insert Data Mahasiswa ...');
            $start = microtime(true);

            $alldata = array();
            $data1 = api_mahasiswa_limit(0,8000);

            // decode json to array
            $array[] = json_decode($data1, true);

            // encode array to json
            $result = json_encode($array);
            $sumber = $result;
            $list = json_decode($sumber, true);
            // dd(count($list[0]));

                for($a=0; $a < count($list[0]); $a++){

                    Mahasiswa::updateOrCreate(['npm'=> $list[0][$a]['npm']],
                        [
                        'npm'               => $list[0][$a]['npm'],
                        'nama'              => $list[0][$a]['nama'],
                        'tahun_angkatan'    => $list[0][$a]['tahun_angkatan'],
                        'tahun_lulus'       => $list[0][$a]['tahun_lulus'],
                        'unit_id'           => $list[0][$a]['kode_prodi'],
                        'jenis_kelamin'     => $list[0][$a]['jenis_kelamin'],
                        'tempat_lahir'      => $list[0][$a]['tempat_lahir'],
                        'tanggal_lahir'     => $list[0][$a]['tgl_lahir'],
                        // 'email'             => $list[0][$a]['email'],
                        'status_mahasiswa'  => $list[0][$a]['status_mahasiswa'],
                        // 'nomor_ijazah'      => $list[0][$a]['nomor_ijazah'],
                        // 'judul_skripsi'     => $list[0][$a]['judul_skripsi'],
                        'gelar'             => $list[0][$a]['gelar']
                    ]);
            }


        $time_elapsed_secs = microtime(true) - $start;
        $this->info('Insert Data Mahasiswa Selesai '.$time_elapsed_secs);

    }
}
