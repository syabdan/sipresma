<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PrestasiMahasiswa;
use App\Mahasiswa;
use Auth;

class updateTotalPrestasi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:total_prestasi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Total Prestasi Mahasiswa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses Update Data Akreditasi ...');
        $start = microtime(true);
            if(\Auth::user()->permissions_id == 5 ){ // Mahasiswa
                $data_prestasi = PrestasiMahasiswa::select(['prestasi_mahasiswas.*'])
                        ->where('npm', Auth::user()->email)
                        ->orderBy('id', 'DESC')->get();
            }else if(\Auth::user()->permissions_id == 4 ){ // Prodi
                $data_prestasi = PrestasiMahasiswa::select(['prestasi_mahasiswas.*'])
                        ->join('units','units.id','prestasi_mahasiswas.unit_id')
                        ->where('units.id', Auth::user()->unit_id)
                        ->groupBy('prestasi_mahasiswas.npm')
                        ->orderBy('prestasi_mahasiswas.updated_at', 'DESC')->get();
            }

            if(\Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5 ){

                foreach($data_prestasi as $data){
                    if(\Auth::user()->permissions_id == 5 ){ // Mahasiswa
                        $count_prestasi = PrestasiMahasiswa::select(['prestasi_mahasiswas.*'])
                                ->where('npm', Auth::user()->email)
                                ->count();
                    }else if(\Auth::user()->permissions_id == 4 ){ // Prodi
                        $count_prestasi = PrestasiMahasiswa::select(['prestasi_mahasiswas.*','mahasiswas.npm as npm_mhs','nama','tempat_lahir','tanggal_lahir','email','total_prestasi'])
                                ->join('units','units.id','prestasi_mahasiswas.unit_id')
                                ->join('mahasiswas','mahasiswas.npm','prestasi_mahasiswas.npm')
                                ->where('units.id', Auth::user()->unit_id)
                                ->where('prestasi_mahasiswas.npm', $data['npm'])
                                ->count();
                    }

                    Mahasiswa::updateOrCreate(['npm' => $data['npm']],[
                        'total_prestasi'              => $count_prestasi
                    ]);
                }
            }

        $time_elapsed_secs = microtime(true) - $start;
        $this->info('Update Data Akreditasi Selesai '.$time_elapsed_secs);
    }
}
