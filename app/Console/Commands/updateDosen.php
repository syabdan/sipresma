<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Dosen;

class updateDosen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:dosen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Dosen to DB Sipresma';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses Update Data Akreditasi ...');
        $start = microtime(true);

        $data = api_dosen();
        $sumber = $data;
        $list = json_decode($sumber, true);

		for($a=0; $a < count($list); $a++){

			Dosen::updateOrCreate(['nama_dosen' => $list[$a]['nama'], 'nidn' => $list[$a]['nidn']],[
                'nama_dosen'        => $list[$a]['nama'],
                'gelar_depan'       => $list[$a]['gelar_depan'],
                'gelar_belakang'    => $list[$a]['gelar_belakang'],
                'unit_id'           => $list[$a]['kode_prodi'],
                'prodi'             => $list[$a]['nama_prodi'],
                'fakultas'          => $list[$a]['nama_fakultas'],
                'nip'               => $list[$a]['nip'],
                'nidn'              => $list[$a]['nidn'],
            ]);
        }
        
        $time_elapsed_secs = microtime(true) - $start;
        $this->info('Update Data Akreditasi Selesai '.$time_elapsed_secs);
    }
}
