<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mahasiswa;
use DB;

class insertMahasiswa_6 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:mahasiswa_6';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert data mahasiswa from API Sikad to database sipresma';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            $this->info('Proses Insert Data Mahasiswa ...');
            $start = microtime(true);

            $alldata = array();
            $data6 = api_mahasiswa_limit(25001,30000);

            // decode json to array
            $array[] = json_decode($data6, true);

            // encode array to json
            $result = json_encode($array);
            $sumber = $result;
            $list = json_decode($sumber, true);
            // dd(count($list[0]));

                for($a=0; $a < count($list[0]); $a++){

                    Mahasiswa::updateOrCreate(['npm'=> $list[0][$a]['npm']],
                        [
                        'npm'               => $list[0][$a]['npm'],
                        'nama'              => $list[0][$a]['nama'],
                        'tahun_angkatan'    => $list[0][$a]['tahun_angkatan'],
                        'tahun_lulus'       => $list[0][$a]['tahun_lulus'],
                        'unit_id'           => $list[0][$a]['kode_prodi'],
                        'jenis_kelamin'     => $list[0][$a]['jenis_kelamin'],
                        'tempat_lahir'      => $list[0][$a]['tempat_lahir'],
                        'tanggal_lahir'     => $list[0][$a]['tgl_lahir'],
                        // 'email'             => $list[0][$a]['email'],
                        'status_mahasiswa'  => $list[0][$a]['status_mahasiswa'],
                        // 'nomor_ijazah'      => $list[0][$a]['nomor_ijazah'],
                        // 'judul_skripsi'     => $list[0][$a]['judul_skripsi'],
                        'gelar'             => $list[0][$a]['gelar']
                    ]);
            }
            // Ambil data kedua
            // $data2 = api_mahasiswa_limit(15001,30000);
            // $sumber2 = $data2;
            // $list2 = json_decode($sumber2, true);
            // if (isset($list2)) {
            // for($a=0; $a < count($list2); $a++){
            //     Mahasiswa::updateOrCreate(['npm'=> $list2[$a]['npm']],
            //     [
            //         'npm'               => $list2[$a]['npm'],
            //         'nama'              => $list2[$a]['nama'],
            //         'tahun_angkatan'    => $list2[$a]['tahun_angkatan'],
            //         'tahun_lulus'       => $list2[$a]['tahun_lulus'],
            //         'unit_id'           => $list2[$a]['kode_prodi'],
            //         'jenis_kelamin'     => $list2[$a]['jenis_kelamin'],
            //         'tempat_lahir'      => $list2[$a]['tempat_lahir'],
            //         'tanggal_lahir'     => $list2[$a]['tgl_lahir'],
            //         'email'             => $list2[$a]['email'],
            //         'status_mahasiswa'  => $list2[$a]['status_mahasiswa'],
            //         'nomor_ijazah'      => $list2[$a]['nomor_ijazah'],
            //         'judul_skripsi'     => $list2[$a]['judul_skripsi'],
            //         'gelar'             => $list2[$a]['gelar']
            //     ]);
            // }
            // }

        $time_elapsed_secs = microtime(true) - $start;
        $this->info('Insert Data Mahasiswa Selesai '.$time_elapsed_secs);

    }
}
