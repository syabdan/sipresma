<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Unit;
use DB;

class updateAkreditasi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:akreditasi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update data akreditasi ke table unit from API Sadardiri to database Sipresma';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses Update Data Akreditasi ...');
        $start = microtime(true);
            $data = api_akreditasi();
            $sumber = $data;
            $list = json_decode($sumber, true);

            foreach($list['data'] as $data){
                Unit::updateOrCreate(['id' => $data['unit_id']],[
                    'akreditasi'              => $data['akreditasi'],
                    'no_sertifikat_akre'      => $data['sk_akreditasi']
                ]);
            }

        $time_elapsed_secs = microtime(true) - $start;
        $this->info('Update Data Akreditasi Selesai '.$time_elapsed_secs);
    }
}
