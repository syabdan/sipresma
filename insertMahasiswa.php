<?php
        $data = api_mahasiswa_limit(0,15000);
        $sumber = $data;
        $list = json_decode($sumber, true);
        
        if (isset($list)) {
		for($a=0; $a < count($list); $a++){
                Mahasiswa::updateOrCreate(['npm'=> $list[$a]['npm']],
                    [
                    'npm'               => $list[$a]['npm'],
                    'nama'              => $list[$a]['nama'],
                    'tahun_angkatan'    => $list[$a]['tahun_angkatan'],
                    'tahun_lulus'       => $list[$a]['tahun_lulus'],
                    'unit_id'           => $list[$a]['kode_prodi'],
                    'jenis_kelamin'     => $list[$a]['jenis_kelamin'],
                    'tempat_lahir'      => $list[$a]['tempat_lahir'],
                    'tanggal_lahir'     => $list[$a]['tgl_lahir'],
                    'email'             => $list[$a]['email'],
                    'status_mahasiswa'  => $list[$a]['status_mahasiswa'],
                    'nomor_ijazah'      => $list[$a]['nomor_ijazah'],
                    'judul_skripsi'     => $list[$a]['judul_skripsi'],
                    'gelar'             => $list[$a]['gelar']
                ]);
            }
        }
                
            $data2 = api_mahasiswa_limit(15001,30000);
            $sumber2 = $data2;
            $list2 = json_decode($sumber2, true);
            
            if (isset($list2)) {
            for($a=0; $a < count($list2); $a++){
                Mahasiswa::updateOrCreate(['npm'=> $list2[$a]['npm']],
                [
                    'npm'               => $list2[$a]['npm'],
                    'nama'              => $list2[$a]['nama'],
                    'tahun_angkatan'    => $list2[$a]['tahun_angkatan'],
                    'tahun_lulus'       => $list2[$a]['tahun_lulus'],
                    'unit_id'           => $list2[$a]['kode_prodi'],
                    'jenis_kelamin'     => $list2[$a]['jenis_kelamin'],
                    'tempat_lahir'      => $list2[$a]['tempat_lahir'],
                    'tanggal_lahir'     => $list2[$a]['tgl_lahir'],
                    'email'             => $list2[$a]['email'],
                    'status_mahasiswa'  => $list2[$a]['status_mahasiswa'],
                    'nomor_ijazah'      => $list2[$a]['nomor_ijazah'],
                    'judul_skripsi'     => $list2[$a]['judul_skripsi'],
                    'gelar'             => $list2[$a]['gelar']
                ]);
            }
        }
?>