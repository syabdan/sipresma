<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('npm');
            $table->string('nama');
            $table->string('tahun_angkatan');
            $table->string('tahun_lulus')->nullable();
            $table->date('tanggal_lulus_sidang')->nullable();
            $table->string('lama_studi')->nullable();
            $table->string('unit_id');
            $table->string('jenis_kelamin');
            $table->string('tempat_lahir')->nullable();
            $table->string('tanggal_lahir');
            $table->string('email')->nullable();
            $table->string('status_mahasiswa');
            $table->string('nomor_ijazah')->nullable();
            $table->string('judul_skripsi')->nullable();
            $table->string('judul_skripsi_en')->nullable();
            $table->string('gelar')->nullable();
            $table->integer('total_prestasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswas');
    }
}
