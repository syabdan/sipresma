<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrestasiMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestasi_mahasiswas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('npm');
            $table->string('nama_kegiatan');
            $table->string('nama_kegiatan_en')->nullable();
            $table->string('kategori_kegiatan');
            $table->string('jenis_kegiatan');
            $table->string('tgl_pelaksanaan');
            $table->string('tempat_penyelenggara');
            $table->string('nama_penyelenggara');
            $table->string('tingkat');
            $table->string('jenis_prestasi');
            $table->string('capaian_prestasi');
            $table->string('deskripsi_prestasi');
            $table->string('deskripsi_prestasi_en')->nullable();
            $table->string('kategori_peserta');
            $table->string('ket_kategori_peserta')->nullable()->comment('jumlah mahasiswa yang terlibat');
            $table->string('sertifikat');
            $table->integer('status_verifikasi_prodi')->nullable();
            $table->integer('status_verifikasi_uir')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestasi_mahasiswas');
    }
}
