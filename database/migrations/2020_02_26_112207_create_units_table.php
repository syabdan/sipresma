<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_unit');
            $table->string('nama_unit_en')->nullable();
            $table->bigInteger('parent_unit_id')->nullable();
            $table->string('no_sk_unit')->nullable();
            $table->string('tgl_sk_unit')->nullable();
            $table->string('jenis_pendidikan')->nullable();
            $table->string('dokumen_unit')->nullable();
            $table->string('gelar')->nullable();
            $table->string('gelar_en')->nullable();
            $table->string('singkatan_gelar')->nullable();
            $table->string('singkatan_gelar_en')->nullable();
            $table->string('akreditasi')->nullable();
            $table->string('no_sertifikat_akre')->nullable();
            $table->string('dokumen_akre')->nullable();
            $table->bigInteger('nidn_pimpinan')->nullable();
            $table->bigInteger('dosen_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
