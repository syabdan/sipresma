<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Permissions = new Permission();
        $Permissions->permission_name = "Superadmin";
        $Permissions->view_status = false;
        $Permissions->add_status = true;
        $Permissions->edit_status = true;
        $Permissions->delete_status = true;
       
        $Permissions->data_menus = "1,2,3,4,5";
        $Permissions->status = true;
        $Permissions->created_by = 1;
        $Permissions->save();

        $Permissions = new Permission();
        $Permissions->permission_name = "Admin";
        $Permissions->view_status = false;
        $Permissions->add_status = true;
        $Permissions->edit_status = true;
     
        $Permissions->data_menus = "1,2,3,4";
        $Permissions->status = true;
        $Permissions->created_by = 1;
        $Permissions->save();


        $Permissions = new Permission();
        $Permissions->permission_name = "Limited User";
        $Permissions->view_status = true;
        $Permissions->add_status = true;
        $Permissions->edit_status = true;
        $Permissions->delete_status = false;
    
        $Permissions->data_menus = "1,2,3";
        $Permissions->status = true;
        $Permissions->created_by = 1;
        $Permissions->save();
    }
}
