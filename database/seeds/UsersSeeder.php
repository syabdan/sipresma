<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newuser = new User();
        $newuser->name = "admin";
        $newuser->email = "syabdan@riau.go.id";
        $newuser->password = bcrypt("secret");
        $newuser->permissions_id = "1";
        $newuser->status = 1;
        $newuser->created_by = 1;
        $newuser->save();
    }
}
