<?php

use Illuminate\Database\Seeder;
use App\Dosen;
class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = api_dosen();
        $sumber = $data;
        $list = json_decode($sumber, true);

		for($a=0; $a < count($list); $a++){

			Dosen::updateOrCreate(['nama_dosen' => $list[$a]['nama']],[
                'nama_dosen'        => $list[$a]['nama'],
                'gelar_depan'       => $list[$a]['gelar_depan'],
                'gelar_belakang'    => $list[$a]['gelar_belakang'],
                'unit_id'           => $list[$a]['kode_prodi'],
                'prodi'             => $list[$a]['nama_prodi'],
                'fakultas'          => $list[$a]['nama_fakultas'],
                'nip'               => $list[$a]['nip'],
            ]);
		}
    }
}
