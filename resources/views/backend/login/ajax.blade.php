<script>
    $(document).ready(function(){
    $('#masuk').on('submit', function(event){
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
            url:"{{ route('masuk') }}",
            method:"POST",
            enctype: "multipart/form-data",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){
                        sebelumKirim();
                        $(".pesan").hide(500);

                },
                success: function(data){
                    window.location.href = 'dashboard';
                },
                error: function(){
                    $(".pesan").show(500);
                    $(".pesan").html('<div class="alert alert-danger" role="alert"><i class="fa fa-ban"></i> Username atau Password salah !</div>');
                    $("button").prop("disabled", false);
                    $("input").prop('disabled', false);

                }
            })
        });


function sebelumKirim(){
		$("button").prop("disabled", true);
		$("input").prop('disabled', true);
		$(".loading").show();
		$(".fa-floppy-o").hide();
}

function adanLoadingFadeIn() {
      loadingBlock();
    }
function adanLoadingFadeOut() {
      $.unblockUI();
    }



function loadingBlock() {
	$.blockUI({
		css: {
			border: 'none',
			padding: '10px',
			width: '150px',
			top:"40%",left:"45%",
			textAlign:"center",
			backgroundColor: '#000',
			'-webkit-border-radius': '10px',
			'-moz-border-radius': '10px',
			opacity: .5,
			color: '#fff'
		},
		message: '<h4>Loading... <i class="fa fa-refresh fa-spin"></i></h4>',
		onOverlayClick: $.unblockUI
	});
}



    });

    </script>
