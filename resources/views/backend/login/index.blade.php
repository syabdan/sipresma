@extends('layouts.login_backend')
@section('content')
<div class="page-header">
    <div class="page-header-image" style="background-image:url({{ asset('backend/assets/images/ologin.jpg')}})"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                {{-- <form method="POST" action="{{ route('login') }}">  --}}
                <form method="POST" id="masuk">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  value="POST"/>
                        <div class="header">
                            <div class="logo-container">
                                <img src="{{ asset('backend/assets/images/uir.png')}}" alt="">
                            </div>
                            {{-- <h5>{{ __('Login') }}</h5> --}}
                        </div>
                        <div class="content">
                        <center><span class='pesan'></span></center>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                <span class="help-block">{{ $error }}</span>
                                @endforeach
                            </div>
                        @endif
                        <div class="input-group input-lg">
                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder="Username" autofocus>


                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>

                        <div class="input-group input-lg">
                             <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>

                    </div>
                    <div class="footer text-center">
                         <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                        <button type="submit" class="btn btn-primary btn-round btn-lg btn-block btnLogin" id="goLogin">
                            {{ __('Login') }}
                        </button>
<!--
                        @if (Route::has('password.request'))
                             <h5><a class="link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a></h5>
                        @endif -->
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <nav>
                <ul>
                    <li><a href="https://uir.ac.id/" target="_blank">Website UIR</a></li>
                    <li><a href="http://sikad.uir.ac.id/" target="_blank">Sikad UIR</a></li>
                    <li><a href="https://uir.ac.id/kontak">Contact UIR</a></li>
                </ul>
            </nav>
            <div class="copyright">
                &copy;
                <script>
                    document.write(new Date().getFullYear())
                </script>,
                <span>Devoleped by <a href="https://www.instagram.com/syabdandalimunthe/" target="_blank">UIR</a></span>
            </div>
        </div>
    </footer>
</div>
@endsection
@push('scriptbottom')
@include('backend.login.ajax')
@endpush
