@extends('layouts.backend.app')
@section('title', 'Dashboard')
@section('header','Dashboard')
@section('subheader','Selamat datang di Sistem Informasi Prestasi Mahasiswa')
@section('bread') <a href="">Dashboard</a> @endsection

@push('style')

@endpush
@push('scripthead')
<link href="{{ asset('backend/assets/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="row clearfix">

                <div class="card">
                    <br>
                    <div class="col-lg-3 float-right">
                        {!! Form::select('tahun_kegiatan', $tahun_kegiatan, NULL, array('id' => 'tahun_kegiatan', 'class'=>"form-control show-tick" ,"data-live-search" => "true" , 'style'=>'font-weight:bold;', 'placeholder' => 'Pilih Tahun Kegiatan')) !!}
                    </div>
                    <div class="header">
                        <h6><strong>Prestasi Akademik</strong></h6>
                    </div>
                    <div class="col-lg-12">
                        <div class="card">
                            <ul class="row profile_state list-unstyled">
                                <li class="col-lg-4 col-md-4 col-6">
                                    <div class="body">
                                        <i class="zmdi zmdi-star text-warning"></i>
                                        <h4>{{ $prestasi_internasional }}</h4>
                                        <span>Total Prestasi Internasional</span>
                                    </div>
                                </li>
                                <li class="col-lg-4 col-md-4 col-6">
                                    <div class="body">
                                        <i class="zmdi zmdi-star-half col-red"></i>
                                        <h4>{{ $prestasi_nasional }}</h4>
                                        <span>Total Prestasi Nasional</span>
                                    </div>
                                </li>
                                <li class="col-lg-4 col-md-4 col-6">
                                    <div class="body">
                                        <i class="zmdi zmdi-star-outline col-blue"></i>
                                        <h4>{{ $prestasi_lokal }}</h4>
                                        <span>Total Prestasi Lokal</span>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row clearfix">
                <div class="card">
                    <div class="header">
                        <h6><strong>Prestasi Non Akademik</strong></h6>
                    </div>
                    <div class="col-lg-12">
                        <div class="card">
                            <ul class="row profile_state list-unstyled">
                                <li class="col-lg-4 col-md-4 col-6">
                                    <div class="body">
                                        <i class="zmdi zmdi-star text-success"></i>
                                        <h4>{{ $prestasi_internasional_non }}</h4>
                                        <span>Total Prestasi Internasional</span>
                                    </div>
                                </li>
                                <li class="col-lg-4 col-md-4 col-6">
                                    <div class="body">
                                        <i class="zmdi zmdi-star-half text-warning"></i>
                                        <h4>{{ $prestasi_nasional_non }}</h4>
                                        <span>Total Prestasi Nasional</span>
                                    </div>
                                </li>
                                <li class="col-lg-4 col-md-4 col-6">
                                    <div class="body">
                                        <i class="zmdi zmdi-star-outline text-danger"></i>
                                        <h4>{{ $prestasi_lokal_non }}</h4>
                                        <span>Total Prestasi Lokal</span>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                </div>

            </div>
            @if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2)
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Prestasi Mahasiswa</strong></h2>
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div id="prestasi_bar"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Prestasi Mahasiswa</strong></h2>
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div id="prestasi_bar_fakultas"></div>
                    </div>
                </div>
            </div>
            @endif

        </div>
        <div class="row clearfix">
          <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Prestasi Akademik</strong></h2>
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <!-- <div id="capaianPrestasi_chart"></div> -->
                        <div id="tingkat_akademik_chart"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Jenis Prestasi</strong></h2>
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <!-- <div id="tingkat_chart"></div> -->
                        <div id="jenis_prestasi_chart"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Prestasi Non Akademik</strong></h2>
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <!-- <div id="jenisKegiatan_chart" ></div> -->
                        <div id="tingkat_nonakademik_chart" ></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@push('scriptbottom')

    <script type="text/javascript" src="{{ asset('backend/assets/js/loader.js') }}"></script>
    <script src="{{ asset('backend/assets/js/pages/forms/basic-form-elements.js')}}"></script>

    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
            ['Tingkat', 'Jumlah Jenis Prestasi'],
            ['Lokal',   {{ $lokal_akademik }}],
            ['Nasional',  {{ $nasional_akademik }}],
            ['Internasional', {{ $internasional_akademik }}]
            ]);

            var options = {
            // is3D: true,
            pieHole: 0.4,
            colors: [ '#db7093', '#ff7f50', '#dc143c'],
            legend:{position: 'bottom'},
            chartArea: {width: 400, height: 150}

            };

            var chart = new google.visualization.PieChart(document.getElementById('tingkat_akademik_chart'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
            ['Tingkat', 'Jumlah Jenis Prestasi'],
            ['Akademik',      {{$akademik}}],
            ['Non Akademik',  {{$nonakademik}}]
            ]);

            var options = {
            colors: [ '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5'],
            legend:{position: 'bottom'},
            chartArea: {width: 400, height: 150}



            };

            var chart = new google.visualization.PieChart(document.getElementById('jenis_prestasi_chart'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
            ['Tingkat', 'Jumlah Jenis Prestasi'],
            ['Lokal',   {{ $lokal_nonakademik }}],
            ['Nasional',  {{ $nasional_nonakademik }}],
            ['Internasional', {{ $internasional_nonakademik }}]
            ]);

            var options = {
            // is3D: true,
            pieHole: 0.4,
            colors: [ '#db7093', '#ff7f50', '#dc143c'],
            legend:{position: 'bottom'},
            chartArea: {width: 400, height: 150}


            };

            var chart = new google.visualization.PieChart(document.getElementById('tingkat_nonakademik_chart'));
            chart.draw(data, options);
        }
    </script>

<script src="https://code.highcharts.com/highcharts.js"></script>

@if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2)
<script>
// Chart Prestasi
var chart = Highcharts.chart('prestasi_bar', {
  chart: {
    type: 'bar',
    animation: false
  },
  title: {
    text: 'Perbandingan berdasarkan Jenis Prestasi'
  },
  subtitle: {
    text: 'Per Program Studi'
  },
  xAxis: {
    categories: {!! json_encode($prestasi_bar['category']) !!},
    title: {
      text: null
    }
  },
  yAxis: {
    allowDecimals: false,
    title: {
      text: 'Jumlah Mahasiswa'
    }
  },
  colors: [
        '#f08080', '#f28f43'],
  plotOptions: {
    bar: {
      dataLabels: {
        enabled: true
      }
    }
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'top',
    floating: true,
    borderWidth: 1,
    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
    shadow: true
  },
  credits: {
    enabled: false
  },
  series: {!! json_encode($prestasi_bar['series']) !!}
})
$(function() {
  $(".toggle").change(function() {
    var type = this.value;
    if (type !== '0') {
      $(chart.series).each(function() {
        this.update({
          type: type
        }, false);
      });
      chart.redraw();
    }
  });
});
// #Chart Prestasi

// Chart Prestasi Fakultas
var chart = Highcharts.chart('prestasi_bar_fakultas', {
  chart: {
    type: 'column',
    animation: false
  },
  title: {
    text: 'Perbandingan berdasarkan Jenis Prestasi'
  },
  subtitle: {
    text: 'Per Fakultas'
  },
  xAxis: {
    categories: {!! json_encode($prestasi_bar_fakultas['category']) !!},
    title: {
      text: null
    }
  },
  yAxis: {
    allowDecimals: false,
    title: {
      text: 'Jumlah Mahasiswa'
    }
  },
  colors: [  '#20b2aa', '#daa520'],
  plotOptions: {
    bar: {
      dataLabels: {
        enabled: true
      }
    }
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'top',
    floating: true,
    borderWidth: 1,
    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
    shadow: true
  },
  credits: {
    enabled: false
  },
  series: {!! json_encode($prestasi_bar_fakultas['series']) !!}
})
$(function() {
  $(".toggle").change(function() {
    var type = this.value;
    if (type !== '0') {
      $(chart.series).each(function() {
        this.update({
          type: type
        }, false);
      });
      chart.redraw();
    }
  });
});
// #Chart Prestasi Fakultas
</script>
@endif

<script>
    $(document).on("change","#tahun_kegiatan", function(){
        var tahun     = $("#tahun_kegiatan").val();

        location.href = "{!! url('dashboard/"+tahun+"') !!}";
    });
</script>
@endpush
