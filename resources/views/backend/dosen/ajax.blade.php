<script type="text/javascript">

var table;
$(document).ready( function () {
   $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  table = $('#table').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: '{!! route('dosen.data') !!}',
          type: 'POST'
         },
         columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                    { data: 'nidn' },
                    { data: 'nama_dosen'},
                    { data: 'prodi'}
               ],
        order: [[1, 'desc']]
      });
});
    

//OPEN MODAL EDIT
function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal').modal('show');
    $('#modal .modal-title').text('Edit dosen');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });
    $.ajax({
        url: "{{ url('dosen') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $('#nama_dosen').val(data.nama_dosen);
                $('#nidn').val(data.nidn);
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL DELETE
function deleteForm(id){
    swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            }, function () {
                    $.ajax({
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},
                    type: "POST",
                    url: "{{ url('dosen') }}"+'/'+id+"/delete",
                    timeout: 3000,
                    success : function(e) {
                        if (e = 1) {
                            ShowAlertDeletedWithReload();
                        }
                        else{
                            swal("Oops!", "Gagal Menghapus Data.", "warning");
                        }
                    },
                    error : function(e){
                        showErrorMessage();
                    }
                    });
            });
};


// End DELETE Function
    
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#btnSave').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){
            var id=$('#id').val();

            if (id == '') {
                var url  = "{{ route('dosen.store') }}";
            }
            else
            {
                var url  = "{{  url('dosen') }}"+'/'+id;
            }

    
            var form = $('#modal-form')[0];
            var inputData = new FormData(form);
            $.ajax({
                processData: false,
                contentType: false,
                cache: false,
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: inputData,
                timeout: 600000,
                success : function(syabdan) {
       
                        document.getElementById("modal-form").reset();
                        showAjaxLoaderSuccesMessage();
                                
                },
                error : function(syabdan){
                 showErrorMessage();
                }
            });
            return false;
        }
});


</script>