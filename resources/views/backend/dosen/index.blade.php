@extends('layouts.backend.app')
@section('title', 'Dosen')
@section('header','Dosen')
@section('subheader','Dosen')
@section('bread') <a href="">Dosen</a> @endsection

@push('style')
<link href="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
<link href="{{ asset('backend/assets/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('backend/assets/plugins/footable-bootstrap/css/footable.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/assets/plugins/footable-bootstrap/css/footable.standalone.min.css')}}">
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="{{ asset('backend/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<!-- Dropzone Css -->
<link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropzone/dropzone.css')}}">
@endpush
@push('scripthead')
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <!-- <button type="button" class="btn btn-info btn-round waves-effect btn-sm align-right"  onclick="modal_add_show()"><i class="zmdi zmdi-plus "></i> Tambah</button> -->
                    </div>

                    <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-hover m-b-0 c_list" id="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIDN</th>
                                    <th data-breakpoints="xs">Nama Dosen</th>
                                    <th data-breakpoints="xs">Prodi</th>
                                    <!-- <th data-breakpoints="sm xs md">Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
	@include('backend.dosen.form')
@endsection
@push('scriptbottom')
<script src="{{ asset('backend/assets/plugins/momentjs/moment.js')}}"></script> 
<script src="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script> 
<script src="{{ asset('backend/assets/js/pages/forms/basic-form-elements.js')}}"></script> 
<script src="{{ asset('backend/assets/bundles/footable.bundle.js')}}"></script>

<!-- Jquery DataTable Plugin Js --> 
<script src="{{ asset('backend/assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>

<script src="{{ asset('backend/assets/plugins/ckeditor/ckeditor.js')}}"></script> <!-- Ckeditor --> 
<script src="{{ asset('backend/assets/plugins/ckeditor/config.js')}}"></script> <!-- Ckeditor --> 
@include('backend.dosen.ajax')

@endpush