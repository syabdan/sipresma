<script type="text/javascript">
var table;
$(document).ready( function () {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('unit.data') !!}',
          type: 'POST'
         },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            { data: 'nama_unit' },
            { data: 'no_sk_unit'},
            { data: 'tgl_sk_unit'},
            { data: 'jenis_pendidikan'},
            { data: 'dokumen_unit'},
            { data: 'gelar'},
            { data: 'singkatan_gelar'},
            { data: 'akreditasi'},
            { data: 'no_sertifikat_akre'},
            { data: 'dokumen_akre'},
            { data: 'pimpinan'},
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});


//OPEN MODAL EDIT
function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal').modal('show');
    $('#modal .modal-title').text('Edit Unit');

    $("body").on("click",".clearCacheForm",function(){
            $(".refresh").empty();
            $(".refresh").attr('readonly',false);
    });
    $.ajax({
        url: "{{ url('unit') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $('#nama_unit').val(data.nama_unit).attr("readonly",true);
                $('#nama_unit_en').val(data.nama_unit_en);
                $('#no_sk_unit').val(data.no_sk_unit);
                $('#tgl_sk_unit').val(data.tgl_sk_unit);
                $('#gelar').val(data.gelar);
                $('#singkatan_gelar').val(data.singkatan_gelar);
                $('#gelar_en').val(data.gelar_en);
                $('#singkatan_gelar_en').val(data.singkatan_gelar_en);
                $('#akreditasi').val(data.akreditasi);
                $('#no_sertifikat_akre').val(data.no_sertifikat_akre);
                $("#nidn_pimpinan option[value='" + data.dosen_id + "']").prop("selected", true).change();

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL DELETE
function deleteForm(id){
    swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            }, function () {
                    $.ajax({
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},
                    type: "POST",
                    url: "{{ url('unit') }}"+'/'+id+"/delete",
                    timeout: 3000,
                    success : function(e) {
                        if (e = 1) {
                            ShowAlertDeletedWithReload();
                        }
                        else{
                            swal("Oops!", "Gagal Menghapus Data.", "warning");
                        }
                    },
                    error : function(e){
                        showErrorMessage();
                    }
                    });
            });
};


// End DELETE Function

$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#btnSave').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){
            var id=$('#id').val();

            if (id == '') {
                var url  = "{{ route('unit.store') }}";
            }
            else
            {
                var url  = "{{  url('unit') }}"+'/'+id;
            }


            var form = $('#modal-form')[0];
            var inputData = new FormData(form);
            $.ajax({
                processData: false,
                contentType: false,
                cache: false,
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: inputData,
                timeout: 600000,
                success : function(syabdan) {

                        document.getElementById("modal-form").reset();
                        showAjaxLoaderSuccesMessage();

                },
                error : function(syabdan){
                    if(syabdan['responseJSON']['errors']) {
                        if(syabdan['responseJSON']['errors']['nama_unit_en']){
                            $( '#nama_unit_en-Error' ).html(syabdan['responseJSON']['errors']['nama_unit_en'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['gelar']){
                            $( '#gelar-Error' ).html(syabdan['responseJSON']['errors']['gelar'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['gelar_en']){
                            $( '#gelar_en-Error' ).html(syabdan['responseJSON']['errors']['gelar_en'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['singkatan_gelar']){
                            $( '#singkatan_gelar-Error' ).html(syabdan['responseJSON']['errors']['singkatan_gelar'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['singkatan_gelar_en']){
                            $( '#singkatan_gelar_en-Error' ).html(syabdan['responseJSON']['errors']['singkatan_gelar_en'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['nidn_pimpinan']){
                            $( '#nidn_pimpinan-Error' ).html(syabdan['responseJSON']['errors']['nidn_pimpinan'][0]);
                        }

                    }
                    showErrorMessage();
                }
            });
            return false;
        }
});


</script>

<script>
    $(function () {
        $('#tgl_sk_unit').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            time: false
        });
    });

    $(".select2").select2();

</script>
