<div class="modal fade" id="modal" tabindex="" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal" id="modal-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  />
                    <div class="row clearfix" style="display: none;">

                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="hidden" id="id" name="id" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="nama_user">Nama {{ $nama_unit }}</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama_unit" name="nama_unit" class="form-control" placeholder="" required>
                            </div>
                            <span style="color: red;"><i><p id="nama_unit-Error"></p></i></span>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="nama_user">Nama {{ $nama_unit }} (English)</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama_unit_en" name="nama_unit_en" class="form-control" placeholder="" required="true">
                            </div>
                            <span style="color: red;"><i><p id="nama_unit_en-Error"></p></i></span>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="nama_user">No. SK Pendirian {{ $nama_unit }}</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="no_sk_unit" name="no_sk_unit" class="form-control" placeholder="No SK Unit" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Tanggal SK {{ $nama_unit }}</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                        <div class="input-group " >
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="tgl_sk_unit" name="tgl_sk_unit" class="form-control" placeholder="Please choose date">
                        </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Dokumen SK Pendirian {{ $nama_unit }} (*.pdf) </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="file" class="form-control" accept="Application/pdf" name="dokumen_unit">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Gelar</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="gelar" name="gelar" class="form-control" placeholder="eg. Sarjana Teknik" required>
                            </div>
                            <span style="color: red;"><i><p id="gelar-Error"></p></i></span>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Singkatan Gelar</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="singkatan_gelar" name="singkatan_gelar" class="form-control" placeholder="eg. S.T" required="true">
                            </div>
                            <span style="color: red;"><i><p id="singkatan_gelar-Error"></p></i></span>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Gelar (English)</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="gelar_en" name="gelar_en" class="form-control" placeholder="eg. Bachelor of Engineering" required="true">
                            </div>
                            <span style="color: red;"><i><p id="gelar_en-Error"></p></i></span>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Singkatan Gelar (English)</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="singkatan_gelar_en" name="singkatan_gelar_en" class="form-control" placeholder="eg. B.Eng" ="true">
                            </div>
                            <span style="color: red;"><i><p id="singkatan_gelar_en-Error"></p></i></span>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Akreditasi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="akreditasi" max_length="1" name="akreditasi" class="form-control" placeholder="eg. A" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">No. SK Akreditasi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="no_sertifikat_akre" name="no_sertifikat_akre" class="form-control" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    {{--  <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Dokumen Akreditasi (*.pdf) </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="file" class="form-control" accept="Application/pdf" name="dokumen_akre">
                            </div>
                        </div>
                    </div>  --}}
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Pimpinan {{ $nama_unit }} </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <select class="form-control select2" style="width:100%" name="nidn_pimpinan" id="nidn_pimpinan" required="true">
                            <option value="">- Pilih Dosen - </option>
                            @foreach($dosens as $dosen)
                            <option value="{{ $dosen->id }}"
                                    @if(old('id') === '{{ $dosen->id }}' ) selected="selected" @endif >{{ $dosen->gelar_depan }} {{ $dosen->nama_dosen}} {{ $dosen->gelar_belakang }}
                            </option>
                           @endforeach
                                </select>
                            <span style="color: red;"><i><p id="nidn_pimpinan-Error"></p></i></span>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSave" class="btn btn-default btn-round waves-effect"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit</button>
            </div>
        </div>
    </div>
</div>

