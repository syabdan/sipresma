<script type="text/javascript">

var table;
$(document).ready( function () {
   $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  table = $('#table').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: '{!! route('capaian_pembelajaran.data') !!}',
          type: 'POST'
         },

        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'unit' },
            { data: 'jenis_cp'},
            { data: 'isi_cp'},
            { data: 'isi_cp_en'},
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ],
        // order: [[1, 'desc']]
      });
});


// OPEN MODAL ADD
function modal_add_show(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#modal').modal('show');
    $('#modal form')[0].reset();
    $('#modal .modal-title').text('Create Capaian Pembelajaran');
    $('.modal-backdrop').hide();
    $('#btnSave').attr('disabled', false);

}

//OPEN MODAL EDIT
function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal').modal('show');
    $('#modal .modal-title').text('Edit Capaian Pembelajaran');

    $("body").on("click",".clearCacheForm",function(){
            $(".refresh").empty();
            $(".refresh").attr('readonly',false);
    });
    $.ajax({
        url: "{{ url('capaian_pembelajaran') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#jenis_cp option[value='" + data.jenis_cp + "']").prop("selected", true).change();
                $("#unit_id option[value='" + data.unit_id + "']").prop("selected", true).change();

                CKEDITOR.instances['isi_cp'].setData(data.isi_cp);

                CKEDITOR.instances['isi_cp_en'].setData(data.isi_cp_en);


        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL DELETE
function deleteForm(id){
    swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            }, function () {
                    $.ajax({
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},
                    type: "POST",
                    url: "{{ url('capaian_pembelajaran') }}"+'/'+id+"/delete",
                    timeout: 3000,
                    success : function(e) {
                        if (e = 1) {
                            ShowAlertDeletedWithReload();
                        }
                        else{
                            swal("Oops!", "Gagal Menghapus Data.", "warning");
                        }
                    },
                    error : function(e){
                        showErrorMessage();
                    }
                    });
            });
};


// End DELETE Function

$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#btnSave').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){
            var id=$('#id').val();

            if (id == '') {
                var url  = "{{ route('capaian_pembelajaran.store') }}";
            }
            else
            {
                var url  = "{{  url('capaian_pembelajaran') }}"+'/'+id;
            }

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances['isi_cp'].updateElement();
                CKEDITOR.instances['isi_cp_en'].updateElement();
            }
            var form = $('#modal-form')[0];
            var inputData = new FormData(form);
            $.ajax({
                processData: false,
                contentType: false,
                cache: false,
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: inputData,
                timeout: 600000,
                beforeSend: function(){
                    $(".btnSave").prop("disabled", true);
                    $(".btnSave").html('Submitting...');
                    },
                success : function(syabdan) {

                        document.getElementById("modal-form").reset();
					    $(".btnSave").prop("disabled", false);
                        $(".btnSave").html('Submit');
                        showAjaxLoaderSuccesMessage();

                },
                error : function(syabdan){
				$(".btnSave").prop("disabled", false);
                $(".btnSave").html('Submit');

                 showErrorMessage();
                 if(syabdan['responseJSON']['errors']) {
                        if(syabdan['responseJSON']['errors']['jenis_cp']){
                            $( '#jenis_cp-Error' ).html(syabdan['responseJSON']['errors']['jenis_cp'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['isi_cp']){
                            $( '#isi_cp-Error' ).html(syabdan['responseJSON']['errors']['isi_cp'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['isi_cp_en']){
                            $( '#isi_cp_en-Error' ).html(syabdan['responseJSON']['errors']['isi_cp_en'][0]);
                        }

                    }
                }
            });
            return false;
        }
});


</script>

<script>
    CKEDITOR.replace("isi_cp");
    CKEDITOR.replace("isi_cp_en");
</script>
