@extends('layouts.backend.app')
@section('title', 'Capaian Pembelajaran Mahasiswa')
@section('header','Capaian Pembelajaran Mahasiswa')
@section('subheader','Data Capaian Pembelajaran Mahasiswa')
@section('bread') <a href="">Capaian Pembelajaran Mahasiswa</a> @endsection

@push('style')
<link href="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
<link href="{{ asset('backend/assets/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('backend/assets/plugins/footable-bootstrap/css/footable.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/assets/plugins/footable-bootstrap/css/footable.standalone.min.css')}}">
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="{{ asset('backend/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<!-- Dropzone Css -->
<link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropzone/dropzone.css')}}">
@endpush
@push('scripthead')
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <button type="button" class="btn btn-info btn-round waves-effect btn-sm align-right"  onclick="modal_add_show()"><i class="zmdi zmdi-plus "></i> Tambah</button>
                    </div>

                    <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-hover m-b-0 c_list" id="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Prodi</th>
                                    <th data-breakpoints="xs">Jenis Capaian Pembelajaran</th>
                                    <th width="30%">Isi</th>
                                    <th width="30%">Isi English</th>
                                    <th data-breakpoints="sm xs md">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
	@include('backend.capaian_pembelajaran.form')
@endsection
@push('scriptbottom')
<script src="{{ asset('backend/assets/plugins/momentjs/moment.js')}}" type="text/javascript"></script> 
<script src="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}" type="text/javascript"></script> 
<script src="{{ asset('backend/assets/js/pages/forms/basic-form-elements.js')}}" type="text/javascript"></script> 
<script src="{{ asset('backend/assets/bundles/footable.bundle.js')}}" type="text/javascript"></script>

<!-- Jquery DataTable Plugin Js --> 
<script src="{{ asset('backend/assets/bundles/datatablescripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}" type="text/javascript"></script>

<script src="{{ asset('backend/assets/plugins/ckeditor/ckeditor.js')}}" type="text/javascript"></script> <!-- Ckeditor --> 
<script src="{{ asset('backend/assets/plugins/ckeditor/config.js')}}" type="text/javascript"></script> <!-- Ckeditor --> 
@include('backend.capaian_pembelajaran.ajax')

@endpush