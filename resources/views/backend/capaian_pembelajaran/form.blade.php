<div class="modal fade" id="modal" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false" tabindex="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal" id="modal-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  />
                    <div class="row clearfix" style="display: none;">

                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="hidden" id="id" name="id" class="form-control">
                            </div>
                        </div>
                    </div>
                    @if(\Auth::user()->permissions_id == 1)
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="NomorHp">Program Studi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <select class="form-control show-tick" data-live-search="true" name="unit_id" id="unit_id">
                                <option value="">- Pilih Prodi - </option>
                                @foreach($prodi as $p)
                                <option value="{{ $p->id }}"
                                        @if(old('id') === '{{ $p->id }}' ) selected="selected" @endif >{{ $p->nama_unit}} | {{ $p->jenis_pendidikan }}
                                </option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="NomorHp">Jenis Capaian Pembelajaran</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <select class="form-control show-tick" data-live-search="true" name="jenis_cp" id="jenis_cp">
                            <option value="Keterampilan Umum" >Keterampilan Umum</option>
                            <option value="Keterampilan Khusus" >Keterampilan Khusus</option>
                            <option value="Penguasaan Pengetahuan" >Penguasaan Pengetahuan</option>
                            <option value="Sikap Khusus" >Sikap Khusus</option>
                            </select>
                            <span style="color: red;"><i><p id="jenis_cp-Error"></p></i></span>

                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="isi_cp">Isi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group" >
                            <textarea class="form-control" name="isi_cp" id="isi_cp" >
                            </textarea>
                            <span style="color: red;"><i><p id="isi_cp-Error"></p></i></span>

                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="isi_cp_en">Isi ( bahasa Inggris )</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <textarea class="form-control" name="isi_cp_en" id="isi_cp_en">

                            </textarea>
                            <span style="color: red;"><i><p id="isi_cp_en-Error"></p></i></span>

                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSave" class="btn btn-default btn-round waves-effect btnSave"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit</button>
            </div>
        </div>
    </div>
</div>

