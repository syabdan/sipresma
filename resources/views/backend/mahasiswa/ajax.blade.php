<script type="text/javascript">

var table;
$(document).ready( function () {
   $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  table = $('#table').DataTable({
         processing: true,
         serverSide: true,
         ajax: {
          url: '{!! route('mahasiswa.data') !!}',
          type: 'POST'
         },
         columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                    { data: 'npm' },
                    { data: 'nama'},
                    { data: 'unit.nama_unit', name: 'unit.nama_unit'},
                    { data: 'tempat_lahir'},
                    { data: 'tanggal_lahir'},
                    { data: 'email'}
               ],
        order: [[1, 'desc']],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10
                },
            },
        'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, 100, 200, 500, 1000, -1 ],
            [ '10 rows', '25 rows', '50 rows', '100 rows', '200 rows', '500 rows', '1000 rows', 'Show all' ]
        ],

      });
});


//OPEN MODAL EDIT
function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal').modal('show');
    $('#modal .modal-title').text('Edit Mahasiswa');

    $("body").on("click",".clearCacheForm",function(){
            $(".refresh").empty();
            $(".refresh").attr('readonly',false);
    });
    $.ajax({
        url: "{{ url('mahasiswa') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $('#npm').val(data.npm);
                $('#nama').val(data.nama);
                $('#tanggal_lulus_sidang').val(data.tanggal_lulus_sidang);
                $('#lama_studi').val(data.lama_studi);
                $('#nomor_ijazah').val(data.nomor_ijazah);
                $('#judul_skripsi').val(data.judul_skripsi);
                $('#judul_skripsi_en').val(data.judul_skripsi_en);
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL DELETE
function deleteForm(id){

};


// End DELETE Function

$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#btnSave').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){
            var id=$('#id').val();

            if (id == '') {
                var url  = "{{ route('mahasiswa.store') }}";
            }
            else
            {
                var url  = "{{  url('mahasiswa') }}"+'/'+id;
            }


            var form = $('#modal-form')[0];
            var inputData = new FormData(form);
            $.ajax({
                processData: false,
                contentType: false,
                cache: false,
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: inputData,
                timeout: 600000,
                success : function(syabdan) {

                        document.getElementById("modal-form").reset();
                        showAjaxLoaderSuccesMessage();

                },
                error : function(syabdan){
                 showErrorMessage();
                }
            });
            return false;
        }
});


</script>
