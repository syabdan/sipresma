<script type="text/javascript">

var table;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('mahasiswa.data_identitas') !!}',
          type: 'POST'
         },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            { data: 'npm' },
            { data: 'nama'},
            // { data: 'nama_unit'},
            { data: 'gelar'},
            { data: 'nomor_ijazah'},
            { data: 'nomor_ijazah_pin'},
            { data: 'tanggal_lulus_sidang'},
            { data: 'judul_skripsi'},
            { data: 'judul_skripsi_en'},
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});


//OPEN MODAL EDIT
function editForm(id){
    $('input[name=_method]').val('PUT');
    $('#modal .modal-title').text('Detail Identitas Mahasiswa');
    $("#btnSave").html('Update');

    $("body").on("click",".clearCacheForm",function(){
            $(".refresh").empty();
            $(".refresh").attr('readonly',false);
    });

    $.ajax({
        url: "{{ url('mahasiswa') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#modal').modal('show');
                $('#id').val(data.id);
                $('#npm').val(data.npm);
                $('#nama').val(data.nama);
                $('#nama_prodi').val(data.nama_unit);
                $('#unit_id').val(data.unit_id);
                $('#tempat_lahir').val(data.tempat_lahir);
                $('#nomor_ijazah').val(data.nomor_ijazah);
                $('#tanggal_lulus').val(data.tgl_lulus_sidang);
                $('#angkatan').val(data.tahun_angkatan);
                $('#tanggal_lahir').val(data.tanggal_lahir);
                $('#gelar').val(data.gelar);
                $('#judul_skripsi').val(data.judul_skripsi);
                $('#judul_skripsi_en').val(data.judul_skripsi_inggris);
                $('#nomor_ijazah_pin').val(data.nin);

                //Isi Lamat Studi
                $.ajax({
                    url: "{{ url('lama_studi') }}"+'/'+data.tanggal_lulus_sidang+'/'+data.tahun_angkatan,
                    type: "GET",
                    success: function(syabdan) {
                        $('#lama_studi').val(syabdan);
                    }
                });

        },
        error: function(syabdan) {
            $('#modal').modal('hide');
            swal({
                title: "Maaf, "+syabdan['responseJSON']['errors']['status_lulus'][0]+" !",
                text: "Anda tidak dapat melakukan 'Validasi Data' pada Mahasiswa yang belum lulus !",
                type: "error",
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true

            }, function () {
                location.reload();
            });
        }
    });
};

$('#btnSave').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){

            swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, validate it!",
            closeOnConfirm: false
            }, function () {
            var id=$('#id').val();
            // var npm=$('#npm').val();
            // var judul_skripsi=$('#judul_skripsi').val();
            // var judul_skripsi_en=$('#judul_skripsi_en').val();
            // var nomor_ijazah=$('#nomor_ijazah').val();
            // var tanggal_lulus=$('#tanggal_lulus').val();

            var url  = "{{  url('mahasiswa/store_identitas/') }}"+'/'+id;

            var form = $('#modal-form')[0];
            var inputData = new FormData(form);
                $.ajax({
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: inputData,
                    // data: {'id':id, 'judul_skripsi':judul_skripsi, 'judul_skripsi_en':judul_skripsi_en, 'tanggal_lulus':tanggal_lulus, 'nomor_ijazah':nomor_ijazah},
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},

                    timeout: 600000,
                    beforeSend: function(){
                    $(".btnSave").prop("disabled", true);
                    $(".btnSave").html('Submitting...');
                    },
                    success : function(syabdan) {
                            // console.log(syabdan.npm);
                            document.getElementById("modal-form").reset();
					        $(".btnSave").prop("disabled", false);
                            showAjaxLoaderSuccesMessage();

                    },
                    error : function(syabdan){
				    $(".btnSave").prop("disabled", false);
                    $(".btnSave").html('Submit');
                    showErrorMessage();
                    }
                });
            });
            return false;
        }
});

</script>

<script>
$(function () {
        $('#tanggal_lulus').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            time: false
        });
    });

</script>
