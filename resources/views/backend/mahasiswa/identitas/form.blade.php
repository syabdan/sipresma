<div class="modal fade" id="modal" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false" tabindex="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal" id="modal-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  />

                    <div class="row clearfix" style="display: none;">
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="hidden" id="id" name="id" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>NPM</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="npm" name="npm" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Nama Mahasiswa</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama" name="nama" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Program Studi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama_prodi" name="nama_prodi" class="form-control" readonly>
                                <input type="hidden" id="unit_id" name="unit_id" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Tempat Lahir</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Tanggal Lahir</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="tanggal_lahir" name="tanggal_lahir" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Gelar</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="gelar" name="gelar" class="form-control" required readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Nomor Ijazah</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nomor_ijazah" name="nomor_ijazah" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Penomoran Ijazah Nasional</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nomor_ijazah_pin" name="nomor_ijazah_pin" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Tanggal Lulus</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                        <div class="input-group " >
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="tanggal_lulus" name="tanggal_lulus_sidang" class="form-control" placeholder="Please choose date" pickTime="false">
                        </div>
                        </div>
                    </div>
                    <!-- <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Lama Studi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="lama_studi" name="lama_studi" class="form-control" required readonly>
                            </div>
                        </div>
                    </div> -->
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Judul Skripsi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="judul_skripsi" name="judul_skripsi" class="form-control" required >
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Judul Skripsi (English)</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="judul_skripsi_en" name="judul_skripsi_en" class="form-control" required >
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSave" class="btn btn-default btn-round waves-effect btnSave"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit & Validasi</button>
            </div>
        </div>
    </div>
</div>

