<div class="modal fade" id="modal" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false" tabindex="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal" id="modal-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  />
                    <div class="row clearfix" style="display: none;">

                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="hidden" id="id" name="id" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">NPM</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="npm" name="npm" class="form-control" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Nama</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama" name="nama" class="form-control" placeholder="" required>
                            </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Tanggal Lulus Sidang</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="tanggal_lulus_sidang" name="tanggal_lulus_sidang" class="form-control" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Lama Studi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="lama_studi" name="lama_studi" class="form-control" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Nomor Ijazah</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nomor_ijazah" name="nomor_ijazah" class="form-control" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Judul Skripsi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="judul_skripsi" name="judul_skripsi" class="form-control" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Judul Skripsi (English)</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="judul_skripsi_en" name="judul_skripsi_en" class="form-control" placeholder="" required>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSave" class="btn btn-default btn-round waves-effect"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit</button>
            </div>
        </div>
    </div>
</div>

