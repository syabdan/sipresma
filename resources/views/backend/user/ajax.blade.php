<script type="text/javascript">

var table;
$(document).ready( function () {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('user.data') !!}',
          type: 'POST'
         },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'name' },
            { data: 'email'},
            { data: 'level'},
            { data: 'actions', name: 'actions', orderable: false, searchable: false }
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});
    
// OPEN MODAL ADD
function modal_add_show(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#modal').modal('show');
    $('#modal form')[0].reset();
    $('#modal .modal-title').text('Create User');
    $('.modal-backdrop').hide();  
    $('#btnSave').attr('disabled', false);

}

//OPEN MODAL EDIT
function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal').modal('show');
    $('#modal .modal-title').text('Edit User');

    $("body").on("click",".clearCacheForm",function(){ 
            $(".refresh").empty(); 
            $(".refresh").attr('readonly',false); 
    });
    $.ajax({
        url: "{{ url('user') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $("#permissions_id option[value='" + data.permissions_id + "']").prop("selected", true).change();
                $("#unit_id option[value='" + data.unit_id + "']").prop("selected", true).change();
                $("#name").val(data.name);
                $("#email").val(data.email);
                $("#hp").val(data.hp);

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL DELETE
function deleteForm(id){
    swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            }, function () {
                    $.ajax({
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},
                    type: "POST",
                    url: "{{ url('user') }}"+'/'+id+"/delete",
                    timeout: 3000,
                    success : function(e) {
                        if (e = 1) {
                            ShowAlertDeletedWithReload();
                        }
                        else{
                            swal("Oops!", "Gagal Menghapus Data.", "warning");
                        }
                    },
                    error : function(e){
                        showErrorMessage();
                    }
                    });
            });
};


// End DELETE Function
    
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#btnSave').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){
            var id=$('#id').val();

            if (id == '') {
                var url  = "{{ route('user.store') }}";
            }
            else
            {
                var url  = "{{  url('user') }}"+'/'+id;
            }

            var form = $('#modal-form')[0];
            var inputData = new FormData(form);
            $.ajax({
                processData: false,
                contentType: false,
                cache: false,
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: inputData,
                timeout: 600000,
                success : function(syabdan) {
       
                        document.getElementById("modal-form").reset();
                        showAjaxLoaderSuccesMessage();
                                
                },
                error : function(syabdan){
                 showErrorMessage();
                }
            });
            return false;
        }
});

 $('.select2').select2();
</script>