<div class="modal fade" id="modal" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false" tabindex="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal" id="modal-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  />
                    <div class="row clearfix" style="display: none;">

                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="hidden" id="id" name="id" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="nama_user">Nama</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="name" name="name" class="form-control" placeholder="Nama User" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="email">Email / Username </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                                <input type="text" id="email" name="email" class="form-control" placeholder="Email" required>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="password">Password </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                <input type="text" id="password" name="password" class="form-control" placeholder="Password" required>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="pic">Image </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="file" class="form-control" accept="image/*" name="pic_file">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="NomorHp">Nomor Hp </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-smartphone"></i></span>
                                <input type="number" maxlength="12" id="hp" name="hp" class="form-control" placeholder="Nomor Hp" required>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="NomorHp">Level </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <select class="form-control show-tick" data-live-search="true" name="level" id="permissions_id">
                            @foreach($permissions as $level)
                            <option value="{{ $level->id }}"
                                    @if(old('permissions_id') === '{{ $level->id }}' ) selected="selected" @endif >{{ $level->permission_name}}
                            </option>
                           @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="NomorHp">Unit ID </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <select class="form-control select2" style="width:100%" name="unit_id" id="unit_id">
                            @foreach($units as $unit)
                            <option value="{{ $unit->id }}"
                                    @if(old('unit_id') === '{{ $unit->id }}' ) selected="selected" @endif >{{ $unit->nama_unit }} {{ $unit->jenis_pendidikan}}
                            </option>
                           @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSave" class="btn btn-default btn-round waves-effect"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit</button>
            </div>
        </div>
    </div>
</div>
