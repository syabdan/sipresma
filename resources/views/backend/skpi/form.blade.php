<div class="modal fade" id="modal" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false" tabindex="">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal" id="modal-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  />

                    <div class="row clearfix" style="display: none;">

                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="hidden" id="id" name="id" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>NPM</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="npm" name="npm" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Angkatan</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="angkatan" name="angkatan" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Nama Mahasiswa</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama" name="nama" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Program Studi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama_prodi" name="nama_prodi" class="form-control" readonly>
                                <input type="hidden" id="unit_id" name="unit_id" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Tempat Lahir</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Tanggal Lahir</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="tanggal_lahir" name="tanggal_lahir" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Gelar</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="gelar" name="gelar" class="form-control" required readonly>
                            </div>
                        </div>
                    </div>
                    {{--  <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Nomor Ijazah</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nomor_ijazah" name="nomor_ijazah" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Tanggal Lulus</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                        <div class="input-group " >
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="tanggal_lulus" name="tanggal_lulus" class="form-control" placeholder="Please choose date" pickTime="false">
                        </div>
                        </div>
                    </div>  --}}
                    <!-- <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Lama Studi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="lama_studi" name="lama_studi" class="form-control" required readonly>
                            </div>
                        </div>
                    </div> -->
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Status Akreditasi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="akreditasi" name="akreditasi" class="form-control" required readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>No. Sertifikat Akreditasi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="no_sertifikat_akre" name="no_sertifikat_akre" class="form-control" required readonly>
                            </div>
                        </div>
                    </div>
                    {{--  <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Judul Skripsi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="judul_skripsi" name="judul_skripsi" class="form-control" required >
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Judul Skripsi (English)</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="judul_skripsi_en" name="judul_skripsi_en" class="form-control" required >
                            </div>
                        </div>
                    </div>  --}}
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-6 form-control-label">
                            <label>Nama Kegiatan</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 form-control-label">
                        <label>Nama Kegiatan (English)</label>
                        </div>
                    </div>
                    <div class="input_fields_wrap"></div>
                    <!-- <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Capaian Pembelajaran</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <textarea type="text" id="isi_cp" name="isi_cp" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Capaian Pembelajaran (English)</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <textarea type="text" id="isi_cp_en" name="isi_cp_en" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div> -->

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSave" class="btn btn-default btn-round waves-effect btnSave"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit & Validasi</button>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal fade" id="modal-trans" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <div id="isi_file_skpi"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSaveTrans" class="btn btn-default btn-round waves-effect"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit</button>
            </div>
        </div>
    </div>
</div> -->

<div class="modal fade" id="modal-pimpinan" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT">
                <div class="row clearfix">
                <input type="hidden" id="npm_cetak_pimpinan" name="npm_cetak_pimpinan" class="form-control" readonly>

                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label for="">Nama Penandatangan SKPI </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <select class="form-control select2"  name="dosen_id" id="dosen_id" style="width:100%">
                            <option value="">- Pilih Dosen - </option>
                            @foreach($dosens as $dosen)
                            <option value="{{ $dosen->id }}"
                                    @if(old('id') === '{{ $dosen->id }}' ) selected="selected" @endif >{{ $dosen->gelar_depan }} {{ $dosen->nama_dosen}} {{ $dosen->gelar_belakang }}
                            </option>
                           @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSavePimpinan" class="btn btn-default btn-round waves-effect"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Print</button>
            </div>
        </div>
    </div>
</div>
