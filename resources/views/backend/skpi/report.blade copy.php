<!doctype html>
<html>
<head>
<meta charset="utf-8">
<style>
 .left    { text-align: left;}
 .right   { text-align: right;}
 .center-uir  { text-align: center;
           	 font-size: 120%;
             font-weight: bold;
            }
 .center-provinsi  { text-align: center;
           	 font-size: 160%;
             font-weight: bold;
            }
 .center-dinas  { text-align: center;
       font-size: 130%;
        font-weight: bold;
      }
 .center-alamat  { text-align: center;
            font-size: 70%;
               font-weight: bold;
             }
 .center-pekanbaru  {
	 		text-align: center;
           font-size: 110%;
            font-weight: bold;
			padding-bottom: 0;
            }
 .center-ttd  { text-align: center;
           font-size: 105%;
            font-weight: bold;
            }
 .center-ttd1  { text-align: center;
           font-size: 105%;
            }

 .font-biasa{
    font-size: 75%;
 }
 .font-ttd{
	font-size: 75%;
 }
 .eng { page-break-after: always; }


/* Reset chapter and figure counters on the body */
body {
    font-family: "Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif;
    line-height: 1.5;
    font-size: 11pt;
  }

  /* Get the title of the current chapter, which will be the content of the h1.
  Reset figure counter because figures start from 1 in each chapter. */
  h1 {
    line-height: 1.3;
  }

  table, tr, td {
      margin-left: 10px;
  }

  td{
    padding: 5px 5px 0px;
    text-align: left;
  }

  .unit {
      padding-top: -5px
  }
</style>
<table width="100%" border="0" style="border-bottom:3px solid #000;tr:margin-top:-25px">
  <tbody>
    <tr>
      <th rowspan="3" width="80"><center><img src="{{ asset('backend/assets/images/uir.png') }}" width="80%"></center></th>
      <th class="center-dinas">SURAT KETERANGAN PENDAMPING IJAZAH</th>
    </tr>
    <tr>
      <th class="center-dinas">UNIVERSITAS ISLAM RIAU</th>
    </tr>
    <tr style="margin-bottom: -10;">
      <th class="center-pekanbaru">Jl. Kaharudin Nasution No. 113 Pekanbaru</th>
    </tr>
  </tbody>
</table>
<br>

<table width="100%" border="1" style="border-collapse: collapse;" class="font-biasa">
	<tr>
		<td width="15%">Nama</td>
		<td>{{ $mhs->nama}}</td>
		<td width="22%">Tempat, Tanggal Lahir</td>
		<td>{{ $mhs->tempat_lahir }}, {{ tanggal_indonesia($mhs->tanggal_lahir, false) }}</td>
	</tr>
	<tr>
		<td width="15%">Program Studi</td>
		<td>{{ $mhs->nama_unit }}</td>
		<td width="22%">Nomor Pokok Mahasiswa</td>
		<td>{{ $mhs->npm}}</td>
	</tr>
	<tr>
		<td width="15%">Bahasa Pengantar</td>
		<td>Bahasa Indonesia</td>
		<td width="22%">Nomor Ijazah</td>
		<td>{{ $mhs->nomor_ijazah}}</td>
	</tr>

	<tr>
		<td width="15%">Gelar</td>
        <td>{{ $mhs->gelar}}</td>
        <td width="22%">Penomoran Ijazah Nasional</td>
		<td>{{ $mhs->nomor_ijazah_pin}}</td>

	</tr>
	<tr>
		<td width="15%">Status Akreditasi</td>
        <td>{{ $mhs->akreditasi}}</td>
        <td width="22%">Lama Studi</td>
		<td>{!! siswa_waktu($mhs->tanggal_lulus_sidang, $mhs->tahun_angkatan) !!}</td>

	</tr>
	<tr>
		<td width="20%">Nomor Sertifikat Akreditasi</td>
		<td colspan="3">{{ $mhs->no_sertifikat_akre}}</td>
	</tr>
	<tr>
		<td width="20%">Judul Skripsi</td>
		<td colspan="3">{{ $mhs->judul_skripsi}}</td>
	</tr>
</table>
<br>

<table width="100%" border="1" style="border-collapse: collapse;" class="font-biasa">
<tr>
<td>
<b>Capaian Pembelajaran</b>
	<table width="100%" border="0" style="border-collapse: collapse;">
		<tr border="0">
			<td colspan="4" >
			{!! $mhs->isi_cp !!}
			</td>
		</tr>
	</table>
</td>
</tr>
<br>
</table>

@if(count($data_prestasi->where('kategori_kegiatan', 'Aktivitas')) > 0)
<table width="100%" border="1" style="border-collapse: collapse; border-bottom:0px" class="font-biasa">
	<tr style="border-top: 0px solid #ddd;">
		<td style="border-bottom: 0px solid #ddd;">
			 <b>Aktivitas</b> <br><br>

			<table width="100%" border="0" style="border-collapse: collapse; ">
                @php
                    $no=1;
                @endphp
                @foreach($data_prestasi->where('kategori_kegiatan', 'Aktivitas') as $aktivitas)
                <tr border="0">
                    <td colspan="4" >
                        {{ $no }}. {{ $aktivitas->nama_kegiatan}} diselenggarakan oleh {{ $aktivitas->nama_penyelenggara}}, {{ $aktivitas->tempat_penyelenggara }}
                    </td>
                </tr>
                @php
                    $no++;
                @endphp
                @endforeach
                <br>
			</table>
        </td>
    </tr>
</table>
@endif
@if(count($data_prestasi->where('kategori_kegiatan', 'Prestasi')) > 0)
<table width="100%" border="1" style="border-collapse: collapse;" class="font-biasa" >
	<tr style="border-top: 0px solid #ddd;">
		<td style="border-top: 0px solid #ddd;">
		{{-- <br><br> --}}
			<b>Prestasi Akademik</b> <br> <br>
			<table width="100%" border="0" style="border-collapse: collapse;">
                @php
                    $no=1;
                @endphp
                @foreach($data_prestasi->where('kategori_kegiatan', 'Prestasi') as $prestasi)
                <tr >
                    <td colspan="4" >
                        {{ $no }}. {{ $prestasi->nama_kegiatan}} diselenggarakan oleh {{ $prestasi->nama_penyelenggara}}, {{ $prestasi->tempat_penyelenggara }}
                    </td>
                </tr>
                @php
                    $no++;
                @endphp
                @endforeach
                <br>
			</table>
		</td>
    </tr>
</table>
@endif
<table width="100%" class="font-ttd">
<br>
	<tr>
		<td width="50%"></td>
		<td align="center" width="50%"></td>
	</tr>
	<tr>
		<td width="50%"></td>
		<td align="center" width="50%"></td>
	</tr>
	<tr>
		<td width="50%"></td>
		<td align="center" width="50%"></td>
	</tr>
	<tr>
		<td  align="left"></td>
		<td align="left" width="50%">Pekanbaru, {{ tanggal_indonesia(date('Y-m-d'),false) }}</td>
	</tr>
	<tr>
		<td width="90%" >
            <div class="visible-print text-center">
                <img src="{{ asset('images/qrcode.svg') }}" alt="" style="width: 60px; height:60px;"><br>
            </div>
        </td>
		<td align="center" width="50%"> <br> <br> <br> <br></td>
	</tr>
	<tr>
		<td align="right"></td>
		<td align="left" width="50%"><b><u>
		@if($pimpinan->gelar_depan ?? '')
		{{ $pimpinan->gelar_depan }}.
		@endif
		{{ $pimpinan->nama_dosen ?? '' }}, {{ $pimpinan->gelar_belakang ?? ''}}</u></b></td>
	</tr>
	<tr>
		<td  align="right"></td>
		<td align="left" width="50%" class="unit">Dekan {{ $pimpinan->nama_unit ?? ''}}</td>
	</tr>

</table>


<div class="eng"></div>
<table width="100%" border="0" style="border-bottom:3px solid #000;tr:margin-top:-25px">
  <tbody>
    <tr>
      <th rowspan="3" width="80"><center><img src="{{ asset('backend/assets/images/uir.png') }}" width="80%"></center></th>
      <th class="center-dinas">DIPLOMA SUPPLEMENT</th>
    </tr>
    <tr>
      <th class="center-dinas">UNIVERSITAS ISLAM RIAU</th>
    </tr>
    <tr style="margin-bottom: -10;">
      <th class="center-pekanbaru">Jl. Kaharudin Nasution No. 113 Pekanbaru</th>
    </tr>
  </tbody>
</table>
<br>

<table width="100%" border="1" style="border-collapse: collapse;" class="font-biasa">
	<tr>
		<td width="15%">Name</td>
		<td>{{ $mhs->nama}}</td>
		<td width="22%">Place and Date of Birth</td>
		<td>{{ $mhs->tempat_lahir }}, {{ tanggal_indonesia($mhs->tanggal_lahir, false) }}</td>
	</tr>
	<tr>
		<td width="15%">Program Study</td>
		<td>{{ $mhs->nama_unit_en }}</td>
		<td width="22%">Student Number</td>
		<td>{{ $mhs->npm}}</td>
	</tr>
	<tr>
		<td width="15%">Language of Instruction</td>
		<td>Indonesian</td>
		<td width="22%">Certificate Number</td>
		<td>{{ $mhs->nomor_ijazah}}</td>
	</tr>
	<tr>
		<td width="15%">Degree</td>
		<td>{{ $mhs->gelar_en}}</td>
		<td width="22%">National Certificate Number</td>
		<td>{{ $mhs->nomor_ijazah_pin}}</td>
	</tr>
	<tr>
		<td width="15%">Accreditation</td>
		<td>{{ $mhs->akreditasi}}</td>
        <td width="22%">Lenght of Study</td>
		<td>{!! siswa_waktu($mhs->tanggal_lulus_sidang, $mhs->tahun_angkatan, 1) !!}</td>
	</tr>
	<tr>
		<td width="20%">Accreditation Number</td>
		<td colspan="3">{{ $mhs->no_sertifikat_akre}}</td>
	</tr>
	<tr>
		<td width="20%">Thesis Title</td>
		<td colspan="3">{{ $mhs->judul_skripsi}}</td>
	</tr>
</table>
<br>

<table width="100%" border="1" style="border-collapse: collapse;" class="font-biasa">
    <tr>
        <td>
        <b>Learning Outcomes</b>
            <table width="100%" border="0" style="border-collapse: collapse;">
                <tr border="0">
                    <td colspan="4" >
                    {!! $mhs->isi_cp_en !!}
                    </td>
                </tr>
            </table>
        </td>
    </tr>

</table>

<br>

@if(count($data_prestasi->where('kategori_kegiatan', 'Aktivitas'))> 0)
<table width="100%" border="1" style="border-collapse: collapse; border-bottom:0px" class="font-biasa">
	<tr style="border-bottom: 0px solid #ddd;">
		<td style="border-bottom: 0px solid #ddd;">
			 <b>Activities</b> <br><br>

			<table width="100%" border="0" style="border-collapse: collapse;">
                @php
                    $no=1;
                @endphp
                @foreach($data_prestasi->where('kategori_kegiatan', 'Aktivitas') as $aktivitas)
                <tr border="0">
                    <td colspan="4" >
                        {{ $no }}. {{ $aktivitas->nama_kegiatan_en}} hosted by {{ $aktivitas->nama_penyelenggara }}, {{ $aktivitas->tempat_penyelenggara }}
                    </td>
                </tr>
                @php
                    $no++;
                @endphp
                @endforeach
                <br>
			</table>
		</td>
	</tr>
</table>
@endif

@if(count($data_prestasi->where('kategori_kegiatan', 'Prestasi')) > 0)
<table width="100%" border="1" style="border-collapse: collapse;" class="font-biasa">
	<tr style="border-top: 0px solid #ddd;">
		<td style="border-top: 0px solid #ddd;">
			<b>Achievement </b> <br> <br>
			<table width="100%" border="0" style="border-collapse: collapse; ">
                @php
                    $no=1;
                @endphp
                @foreach($data_prestasi->where('kategori_kegiatan', 'Prestasi') as $prestasi)
                <tr >
                    <td colspan="4" >
                        {{ $no }}. {{ $prestasi->nama_kegiatan_en}} hosted by {{ $prestasi->nama_penyelenggara }}, {{ $prestasi->tempat_penyelenggara }}
                    </td>
                </tr>
                @php
                    $no++;
                @endphp
                @endforeach
                <br>
			</table>
		</td>
	</tr>
</table>
@endif

<table width="100%" class="font-ttd">
<br>
	<tr>
		<td width="50%"></td>
		<td align="center" width="40%"></td>
	</tr>
	<tr>
		<td width="50%"></td>
		<td align="center" width="40%"></td>
	</tr>
	<tr>
		<td width="50%"></td>
		<td align="center" width="40%"></td>
	</tr>
	<tr>
		<td  align="left"></td>
		<td align="left" width="40%">Pekanbaru, {{ tanggal_english(date('Y-m-d'),false) }}</td>
	</tr>
	<tr>
		<td width="90%" align="left">
            <div class="visible-print text-center">
                <img src="{{ asset('images/qrcode.svg') }}" alt="" style="width: 60px; height:60px"><br>
                {{--  <span>Scan me to return to the original page.</span>  --}}
            </div>
        </td>
		<td align="center" width="40%"> <br> <br> <br> <br></td>
	</tr>
	<tr>
		<td align="right">
        </td>
		<td align="left" width="50%"><b><u>
		@if($pimpinan->gelar_depan ?? '')
		{{ $pimpinan->gelar_depan }}.
		@endif
		{{ $pimpinan->nama_dosen ?? ''}}, {{ $pimpinan->gelar_belakang ?? ''}}</u></b></td>
	</tr>
	<tr>
		<td  align="right"></td>
		<td align="left" width="40%"  class="unit">Dean Faculty of  {{ $pimpinan->nama_unit_en ?? ''}}</td>
	</tr>

</table>

</html>
