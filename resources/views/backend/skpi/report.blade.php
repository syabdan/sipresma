<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="{{ asset('backend/assets/css/report.css') }}">
</head>
<body>
	<div id="footer">
		<table>
			<tr>
				<td class="nama_footer">
					{{ $mhs->nama }} | No: {{ $mhs->no_skpi }}
				</td>
				<td class="halaman_footer">
					<h5 class="page" style="font-weight: lighter;"></h5>
				</td>
			</tr>
		</table>
	</div>
	<table width="100%" border="0" style="margin-top:-60px">
		<tr>
			<td class="judul_skpi">SURAT KETERANGAN</td>
			<td class="spasi_no_skpi"></td>
		</tr>
		<tr>
			<td class="judul_skpi2">PENDAMPING IJAZAH</td>
			<td class="no_skpi">Nomor: {{ $mhs->no_skpi}}</td>
		</tr>
		<tr>
			<td class="font_eng">Diploma Supplement</td>
			<td></td>
		</tr>
	</table>
	<br>

	<table style="margin-top: 5px;">
		<tr>
			<td class="deskripsi">Surat Keterangan Pendamping Ijazah (SKPI) ini mengacu pada Kerangka Kualifikasi Nasional Indonesia (KKNI) dan Konvensi Unesco tentang pengakuan studi, ijazah dan gelar pendidikan tinggi. Tujuan dari SKPI ini adalah menjadi dokumen yang menyatakan kemampuan kerja, penguasaan pengetahuan, dan sikap/moral pemegangnya.</td>
		</tr>
		<tr><td></td></tr>
		<tr><td></td></tr>
		<tr>
			<td class="deskripsi_eng">This Diploma Supplement refers to the Indonesian Qualification Framework and UNESCO Convention on the Recognition of Studies, Diplomas and Degrees in Higher Education. The purpose of the supplement is to provide a description of the nature, level, context and status of the studies that were pursued and successfully completed by the individual named on the original qualification to which this supplement is appended.</td>
		</tr>
	</table>

	<table>
		<tr>
			<td class="sub_judul">01. INFORMASI TENTANG IDENTITAS DIRI PEMEGANG SKPI</td>
		</tr>
		<tr>
			<td class="sub_judul_eng" style="padding-bottom: -10px">01. Information Identifying The Holder of Diploma Supplement</td>
		</tr>
	</table>
	<table class="atribut">
		<tr>
			<td class="atribut_indo">NAMA LENGKAP</td>
			<td class="atribut_indo">TAHUN LULUS</td>
		</tr>
		<tr>
			<td class="atribut_eng">Full Name</td>
			<td class="atribut_eng">Year of Completion</td>
		</tr>
		<tr>
			<td class="isi">{{ $mhs->nama}}</td>
			<td class="isi">{{ substr($mhs->tanggal_lulus_sidang,0,4) }}</td>
		</tr>
		<tr>
			<td class="atribut_indo">TEMPAT TANGGAL LAHIR</td>
			<td class="atribut_indo">NOMOR IJAZAH NASIONAL</td>
		</tr>
		<tr>
			<td class="atribut_eng">Date and Place Of Birth</td>
			<td class="atribut_eng">Diploma Number</td>
		</tr>
		<tr>
			<td class="isi">{{ $mhs->tempat_lahir }}, {{ tanggal_indonesia($mhs->tanggal_lahir, false) }}</td>
			<td class="isi">{{ $mhs->nomor_ijazah_pin }}</td>
		</tr>
		<tr>
			<td class="atribut_indo">NOMOR POKOK MAHASISWA</td>
			<td class="atribut_indo">GELAR</td>
		</tr>
		<tr>
			<td class="atribut_eng">Student Identification Number</td>
			<td class="atribut_eng">Name of Qualification</td>
		</tr>
		<tr>
			<td class="isi">{{ $mhs->npm}}</td>
			<td class="isi">
				{{ $mhs->gelar}}
				<br>
				<i class="isi_en">{{ $mhs->gelar_en }}</i>
			</td>
		</tr>
	</table>

	<table>
		<tr>
			<td class="sub_judul">02.INFORMASI TENTANG IDENTITAS PENYELENGGARA PROGRAM</td>
		</tr>
		<tr>
			<td class="sub_judul_eng" style="padding-bottom: -10px">02.Information Identifying the Awarding Institution</td>
		</tr>
	</table>
	<table class="atribut">
		<tr>
			<td class="atribut_indo">SK PENDIRIAN PROGRAM STUDI</td>
			<td class="atribut_indo">PERSYARATAN PENERIMAAN</td>
		</tr>
		<tr>
			<td class="atribut_eng">Awarding Institution’s License</td>
			<td class="atribut_eng">Entry Requirements</td>
		</tr>
		<tr>
			<td class="isi">{{ $mhs->no_sertifikat_akre }}</td>
			<td class="isi">Lulus pendidikan menengah atas/sederajat</td>
		</tr>
		<tr>
			<td class="atribut_indo">PROGRAM STUDI</td>
			<td class="atribut_indo">BAHASA PENGANTAR KULIAH</td>
		</tr>
		<tr>
			<td class="atribut_eng">Major</td>
			<td class="atribut_eng">Language of Instruction</td>
		</tr>
		<tr>
			<td class="isi">
				{{ $mhs->nama_unit }}
				<br>
				<i class="isi_en">{{ $mhs->nama_unit_en }}</i>
			</td>
			<td class="isi">
				Indonesia
				<br>
				<i class="isi_en">Indonesian</i>
			</td>
		</tr>

		<tr>
			<td class="atribut_indo">FAKULTAS</td>
			<td class="atribut_indo">SISTEM PENILAIAN</td>
		</tr>
		<tr>
			<td>Faculty</td>
			<td>Grading System</td>
		</tr>
		<tr>
			<td class="isi">{{ $pimpinan->nama_unit }}
				<br>
				<i class="isi_en">{{ $pimpinan->nama_unit_en }}</i>
			</td>
			<td class="isi" style="padding-right: 5px">
				<span style="font-size:9px">Skala 1-4; A=4, A-=3.75, B+=3.5, B=3, B-=2.75, C+=2.5, C=2, C-=1.75, D=1</span>
				<br>
				<span style="font-size:9px"><i>Scale 1-4; A=4, A-=3.75, B+=3.5, B=3, B-=2.75, C+=2.5, C=2, C-=1.75, D=1</i></span>

			</td>
		</tr>

		<tr>
			<td class="atribut_indo">JENIS DAN JENJANG PENDIDIKAN</td>
			<td class="atribut_indo">LAMA STUDI REGULER</td>
		</tr>
		<tr>
			<td class="atribut_eng">Type and Level Education</td>
			<td class="atribut_eng">Regular Length of Study</td>
		</tr>
		<tr>
			<td class="isi">
				Akademik & Sarjana (Strata 1)
				<br>
				<i class="isi_en">Academic & Bachelor Degree</i>
			</td>
			<td class="isi">
				8 Semester
			</td>
		</tr>

		<tr>
			<td class="atribut_indo">JENJANG KUALIFIKASI SESUAI KKNI</td>
			<td class="atribut_indo">JENIS DAN JENJANG PENDIDIKAN LANJUTAN</td>
		</tr>
		<tr>
			<td class="atribut_eng">Level of Qualification in the National Qualification Framework</td>
			<td class="atribut_eng">Access to Further Study</td>
		</tr>
		<tr>
			<td class="isi">
				Level 6
			</td>
			<td class="isi">
				Program Magister & Doktoral
				<br>
				<i class="isi_en">Master & Doctoral Program</i>
			</td>
		</tr>
	</table>

	<table>
		<tr>
			<td class="sub_judul">03. INFORMASI TENTANG KUALIFIKASI DAN HASIL YANG DICAPAI</td>
		</tr>
		<tr>
			<td class="sub_judul_eng">03. Information Identifying the Qualification and Outcomes Obtained</td>
		</tr>
	</table>
	<table class="atribut">
		<tr>
			<td class="atribut_indo">A. CAPAIAN PEMBELAJARAN</td>
			<td class="sub_judul_eng_2">A. Learning Outcome</td>
		</tr>
		<tr>
			<td class="atribut_indo">{{ $mhs->gelar }} : {{ $mhs->nama_unit }} (KKNI LEVEL 6)</td>
			<td class="sub_judul_eng_2">{{ $mhs->gelar_en }} (KKNI LEVEL 6)</td>
		</tr>
		<tr>
			<td> </td>
			<td> </td>
		</tr>

		<tr>
			<td class="atribut_indo">KETERAMPILAN UMUM</td>
			<td class="sub_judul_eng_2">General Skills</td>
		</tr>
		<tr>
			<td class="isi_list">{!! $cp_umum->isi_cp ?? '' !!}</td>
			<td class="isi_list_en">{!! $cp_umum->isi_cp_en ?? '' !!}</td>
		</tr>
		<tr>
			<td> </td>
			<td> </td>
		</tr>
		<tr>
			<td class="atribut_indo">KETERAMPILAN KHUSUS</td>
			<td class="sub_judul_eng_2">Special Skills</td>
		</tr>
		<tr>
			<td class="isi_list">{!! $cp_khusus->isi_cp ?? '' !!}</td>
			<td class="isi_list_en">{!! $cp_khusus->isi_cp_en ?? '' !!}</td>
		</tr>
		<tr>
			<td> </td>
			<td> </td>
		</tr>
		<tr>
			<td class="atribut_indo">PENGUASAAN PENGETAHUAN</td>
			<td class="sub_judul_eng_2">Knowledge Competencies</td>
		</tr>
		<tr>
			<td class="isi_list">{!! $cp_pengetahuan->isi_cp ?? '' !!}</td>
			<td class="isi_list_en">{!! $cp_pengetahuan->isi_cp_en ?? '' !!}</td>
		</tr>
		<tr>
			<td> </td>
			<td> </td>
		</tr>
		<tr>
			<td class="atribut_indo">SIKAP KHUSUS</td>
			<td class="sub_judul_eng_2">Special Attitudes</td>
		</tr>
		<tr>
			<td class="isi_list">{!! $cp_sikapkhusus->isi_cp ?? '' !!}</td>
			<td class="isi_list_en">{!! $cp_sikapkhusus->isi_cp_en ?? '' !!}</td>
		</tr>
		<tr>
			<td> </td>
			<td> </td>
		</tr>
		<tr>
			<td class="atribut_indo">B. AKTIVITAS, PRESTASI DAN PENGHARGAAN</td>
			<td class="sub_judul_eng_2">B. Activities, Achievements, and Awards</td>
		</tr>
		<tr>
			<td class="sub_judul2">
				Aktivitas
			</td>
			<td class="sub_judul_eng_2">
				Activities
			</td>
		</tr>

		@php
		$no=1;
		@endphp
		@foreach($data_prestasi->where('kategori_kegiatan', 'Aktivitas') as $aktivitas)
		<tr>
			<td class="isi_list">
				{{ $no }}. {{ $aktivitas->capaian_prestasi}}, {{ $aktivitas->nama_kegiatan}}, {{ $aktivitas->nama_penyelenggara}}, {{ $aktivitas->tempat_penyelenggara }}
			</td>
			<td class="isi_list_en">
				{{ $no }}. {{ prestasiEng($aktivitas->capaian_prestasi)}}, {{ $aktivitas->nama_kegiatan_en ?? ''}}, {{ $aktivitas->nama_penyelenggara ?? ''}}, {{ $aktivitas->tempat_penyelenggara ?? ''}}
			</td>
		</tr>
		@php
			$no++;
		@endphp
		@endforeach

		<br>

		<tr>
			<td class="sub_judul2">
				Prestasi dan Penghargaan
			</td>
			<td class="sub_judul_eng_2">
				Achievement and Awards
			</td>
		</tr>
		@php
		$no=1;
		@endphp
		@foreach($data_prestasi->where('kategori_kegiatan', 'Prestasi') ?? null  as $prestasi)
		<tr>
			<td class="isi_list">
				{{ $no }}. {{ $aktivitas->capaian_prestasi}}, {{ $prestasi->nama_kegiatan ?? ''}}, {{ $prestasi->nama_penyelenggara ?? ''}}, {{ $prestasi->tempat_penyelenggara ?? ''}}
			</td>
			<td class="isi_list_en">
				{{ $no }}. {{ prestasiEng($aktivitas->capaian_prestasi)}}, {{ $prestasi->nama_kegiatan_en ?? ''}}, {{ $prestasi->nama_penyelenggara ?? ''}}, {{ $prestasi->tempat_penyelenggara ?? ''}}
			</td>
		</tr>
		@php
			$no++;
		@endphp
		@endforeach

	</table>

	{{-- <div class="break"></div> --}}
	<table>
		<tr>
			<td class="sub_judul">04. PENGESAHAN SKPI</td>
		</tr>
		<tr>
			<td class="sub_judul_eng">04. SKPI Legalization</td>
		</tr>
	</table>
	<table class="atribut ">
		<tr>
			<td style="padding-top: 12px; padding-bottom: -5px;" class="ttd">
				<b>
					PEKANBARU, {{ tanggal_indonesia(date('Y-m-d'),false) }}
				</b>
			</td>
		</tr>
		<tr>
			<td class="ttd">
				PEKANBARU, {{ tanggal_english(date('Y-m-d'),false) }}
			</td>
		</tr>
		<tr >
			<td style="padding-top:60px;" class="ttd">
				<b>
					<u>
						@if($pimpinan->gelar_depan ?? '')
						{{ $pimpinan->gelar_depan }}.
						@endif
						{{ $pimpinan->nama_dosen ?? ''}}, {{ $pimpinan->gelar_belakang ?? ''}}
					</u>
				</b>
			</td>
		</tr>
		<tr>
			<td class="ttd2">
				<b>
					Dekan {{ $pimpinan->nama_unit ?? ''}}
				</b>
			</td>
		</tr>
		<tr>
			<td style="padding-top: -5px" class="isi_en">
				Dean Faculty of  {{ $pimpinan->nama_unit_en ?? ''}}
			</td>
		</tr>

		<tr>
			<td style="padding-top: 10px" class="ttd2">
				<b>
					NOMOR INDUK PEGAWAI {{ $pimpinan->nip }}
				</b>
			</td>
		</tr>
			<td style="padding-top: -5px" class="isi_en">
				Employee ID Number {{ $pimpinan->nip }}
			</td>
		</tr>
	</table>



</body>
</html>
