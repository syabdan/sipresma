<script type="text/javascript">


//OPEN MODAL EDIT
function validasiForm(id){
    $('input[name=_method]').val('PUT');
    $('#modal .modal-title').text('Detail Identitas Mahasiswa');
    // $("#btnsaveValidasi").html('Update');

    $("body").on("click",".clearCacheForm",function(){
            $(".refresh").empty();
            $(".refresh").attr('readonly',false);
    });

    $.ajax({
        url: "{{ url('mahasiswa') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {

                $('#modal-validasi').modal('show');
                $('#idIdentitas').val(data.id);
                $('#npmIdentitas').val(data.npm);
                $('#namaIdentitas').val(data.nama);
                $('#nama_prodiIdentitas').val(data.nama_unit);
                $('#unit_idIdentitas').val(data.unit_id);
                $('#tempat_lahirIdentitas').val(data.tempat_lahir);
                $('#nomor_ijazahIdentitas').val(data.nomor_ijazah);
                $('#tanggal_lulusIdentitas').val(data.tgl_lulus_sidang);
                $('#angkatanIdentitas').val(data.tahun_angkatan);
                $('#tanggal_lahirIdentitas').val(data.tanggal_lahir);
                $('#gelarIdentitas').val(data.gelar);
                $('#judul_skripsiIdentitas').val(data.judul_skripsi);
                $('#judul_skripsi_enIdentitas').val(data.judul_skripsi_inggris);
                $('#nomor_ijazah_pinIdentitas').val(data.nin);

                //Isi Lamat Studi
                if(data.tanggal_lulus_sidang){
                    $.ajax({
                        url: "{{ url('lama_studi') }}"+'/'+data.tanggal_lulus_sidang+'/'+data.tahun_angkatan,
                        type: "GET",
                        success: function(syabdan) {
                            $('#lama_studiIdentitas').val(syabdan);
                        }
                    });
                }
                

        },
        error: function(syabdan) {
            $('#modal-validasi').modal('hide');
            swal({
                title: "Maaf, "+syabdan['responseJSON']['errors']['status_lulus'][0]+" !",
                text: "Anda tidak dapat melakukan 'Validasi Data' pada Mahasiswa yang belum lulus !",
                type: "error",
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true

            }, function () {
                location.reload();
            });
        }
    });
};


$('#btnsaveValidasi').on('click', function (syabdan){

    if (!syabdan.isDefaultPrevented()){
            swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, validate it!",
            closeOnConfirm: false
            }, function () {
            var id=$('#idIdentitas').val();

            var url  = "{{  url('mahasiswa/store_identitas/') }}"+'/'+id;

            var form = $('#modal-form-validasi')[0];
            var inputData = new FormData(form);
                $.ajax({
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: inputData,
                    // data: {'id':id, 'judul_skripsi':judul_skripsi, 'judul_skripsi_en':judul_skripsi_en, 'tanggal_lulus':tanggal_lulus, 'nomor_ijazah':nomor_ijazah},
                    headers: {'X-CSRF-TOKEN': $('#modal-validasi input[name="_token"]').val()},

                    timeout: 600000,
                    beforeSend: function(){
                    $(".btnsaveValidasi").prop("disabled", true);
                    $(".btnsaveValidasi").html('Submitting...');
                    },
                    success : function(syabdan) {
                            // console.log(syabdan.npm);
                            document.getElementById("modal-form-validasi").reset();
					        $(".btnsaveValidasi").prop("disabled", false);
                            showAjaxLoaderSuccesMessage();

                    },
                    error : function(syabdan){
				    $(".btnsaveValidasi").prop("disabled", false);
                    $(".btnsaveValidasi").html('Submit');
                    showErrorMessage();
                    }
                });
            });
            return false;
        }
});

</script>

<script>
$(function () {
        $('#tanggal_lulusIdentitas').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            time: false
        });
    });

</script>
