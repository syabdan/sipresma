<script type="text/javascript">

var table;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('skpi.data') !!}',
          type: 'POST'
         },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'actions', name: 'actions', orderable: false, searchable: false, printable: false , targets: [0] },
            { data: 'npm' },
            { data: 'nama'},
            { data: 'prodi'},
            { data: 'jenis_pendidikan'},
            { data: 'fakultas'},
            { data: 'nomor_ijazah_pin'},
            { data: 'no_skpi'},

        ],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Surat Keterangan Pendamping Ijazah Universitas Islam Riau',
                exportOptions: {
                    columns: [ 0, 2, 3, 4, 5, 6, 7, 8 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: 'Surat Keterangan Pendamping Ijazah Universitas Islam Riau',
                exportOptions: {
                    columns: [ 0, 2, 3, 4, 5, 6, 7, 8 ]
                }
            },
            'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, 100, 200, 500, 1000],
            [ '10 rows', '25 rows', '50 rows', '100 rows', '200 rows', '500 rows', '1000 rows' ]
        ],
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});

var table;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    table = $('#table-non').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('skpi.data_skpinon') !!}',
          type: 'POST'
         },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            { data: 'npm' },
            { data: 'nama'},
            { data: 'prodi'},
            { data: 'jenis_pendidikan'},
            { data: 'fakultas'},
            { data: 'nomor_ijazah_pin'},
            { data: 'no_skpi'},
        ]
    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});


//OPEN MODAL EDIT
function editForm(id){
    $('input[name=_method]').val('PUT');
    $('#modal').modal('show');
    $('#modal .modal-title').text('Detail SKPI Mahasiswa');
    $("#btnSave").html('Update');

    $("body").on("click",".clearCacheForm",function(){
            $(".refresh").empty();
            $(".refresh").attr('readonly',false);
    });
    $.ajax({
        url: "{{ url('skpi') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $('#npm').val(data.npm);
                $('#nama').val(data.nama);
                $('#nama_prodi').val(data.nama_unit);
                $('#unit_id').val(data.unit_id);
                $('#tempat_lahir').val(data.tempat_lahir);
                $('#nomor_ijazah').val(data.nomor_ijazah);
                $('#tanggal_lulus').val(data.tgl_lulus_sidang);
                $('#angkatan').val(data.tahun_angkatan);
                $('#tanggal_lahir').val(data.tanggal_lahir);
                $('#gelar').val(data.gelar);
                $('#akreditasi').val(data.akreditasi);
                $('#no_sertifikat_akre').val(data.no_sertifikat_akre);
                $('#judul_skripsi').val(data.judul_skripsi);
                $('#judul_skripsi_en').val(data.judul_skripsi_en);
                // CKEDITOR.instances['isi_cp'].setData(data.isi_cp);
                // CKEDITOR.instances['isi_cp_en'].setData(data.isi_cp_en);

        // Isi Nama Kegiatan
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        $.ajax({
        url: "{{ url('skpi/prestasi') }}"+'/'+data.npm,
        type: "GET",
        dataType: "JSON",
        success: function(adan) {
            $(".input_fields_wrap").empty();
            var n=0;
            $.each(adan, function(key, value)
            {

                $(wrapper).append('<div class="row clearfix form-kegiatan"><div class="col-lg-6 col-md-6 col-sm-6"><input type="hidden"  name="id_prestasi[]" class="form-control id_prestasi'+n+'" readonly><div class="form-group"><input type="text" name="nama_kegiatan_trans[]" class="form-control nama_kegiatan_trans'+n+'" placeholder="Nama Kegiatan" readonly></div></div><div class="col-lg-6 col-md-6 col-sm-6"><div class="form-group"><input type="text" name="nama_kegiatan_en_trans[]" class="form-control nama_kegiatan_en_trans'+n+'" placeholder="Nama Kegiatan English" ></div></div></div>'); //looping form kegiatan
                $('#npm').val(value.npm);
                $(".id_prestasi"+n).val(value.id);
                $(".nama_kegiatan_trans"+n).val(value.nama_kegiatan);
                $(".nama_kegiatan_en_trans"+n).val(value.nama_kegiatan_en);
                n=n+1;
            });

                },
                error: function() {
                    alert('Data is empty !')
                }
            });

        //Isi Lamat Studi
        $.ajax({
            url: "{{ url('lama_studi') }}"+'/'+data.tanggal_lulus_sidang+'/'+data.tahun_angkatan,
            type: "GET",
            success: function(syabdan) {
                $('#lama_studi').val(syabdan);
            }
        });

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL Print
function printForm(npm){
    // alert(npm);
    $('#modal-pimpinan').modal('show');
    $('#modal-pimpinan .modal-title').text('Edit Nama Penandatangan SKPI');
    $('#npm_cetak_pimpinan').val(npm);

};



$('#btnSave').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){

            swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, validate it!",
            closeOnConfirm: false
            }, function () {
            var id=$('#id').val();
            // var npm=$('#npm').val();
            // var judul_skripsi=$('#judul_skripsi').val();
            // var judul_skripsi_en=$('#judul_skripsi_en').val();
            // var nama_kegiatan_en=$('#nama_kegiatan_en').val();
            // var nama_kegiatan=$('#nama_kegiatan').val();

            var url  = "{{  url('skpi') }}"+"/"+id;

            var form = $('#modal-form')[0];
            var inputData = new FormData(form);
                $.ajax({
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: inputData,
                    // data: {npm:npm, judul_skripsi:judul_skripsi, judul_skripsi_en:judul_skripsi_en, nama_kegiatan:nama_kegiatan, nama_kegiatan_en:nama_kegiatan_en},
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},

                    timeout: 600000,
                    beforeSend: function(){
                    $(".btnSave").prop("disabled", true);
                    $(".btnSave").html('Submitting...');
                    },
                    success : function(syabdan) {
                            // console.log(syabdan.npm);
                            document.getElementById("modal-form").reset();
					        $(".btnSave").prop("disabled", false);
                            $(".btnSave").html('Submit');
                            showAjaxLoaderSuccesMessage();

                    },
                    error : function(syabdan){
				    $(".btnSave").prop("disabled", false);
                    $(".btnSave").html('Submit');
                    showErrorMessage();
                    }
                });
            });
            return false;
        }
});

$('#btnSavePimpinan').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){

            swal({
            title: "Are you sure?",
            // text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Print!",
            closeOnConfirm: false
            }, function () {
            var npm=$('#npm_cetak_pimpinan').val();
            var dosen_id=$('#dosen_id').val();
            var url  = "{{  url('skpi/cetak_ex') }}"+"/"+npm+"/"+dosen_id;

            var form = $('#modal-form')[0];
            var inputData = new FormData(form);
                $.ajax({
                    processData: false,
                    contentType: false,
                    cache: false,
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: url,
                    data: inputData,
                    // data: {npm:npm, dosen_id: dosen_id},
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},

                    timeout: 600000,
                    beforeSend: function(){
                    $(".btnSave").prop("disabled", true);
                    $(".btnSave").html('Submitting...');
                    },
                    success : function(syabdan) {
                        showAjaxLoaderSuccesMessage();
                        window.open(this.url);

                    },
                    error : function(syabdan){
				    $(".btnSave").prop("disabled", false);
                    $(".btnSave").html('Submit');
                    showErrorMessage();
                    }
                });
            });
            return false;
        }
});


</script>

<script>
$(function () {
        $('#tanggal_lulus').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            time: false
        });
    });

</script>
