<script type="text/javascript">

// Tabel prestasi per mahasiswa
var table;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    table = $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('prestasi.data') !!}',
          type: 'POST'
         },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            @if(Auth::user()->permissions_id != 5) // Tidak == Level Mahasiswa
            { data: 'npm' },
            { data: 'nama' },
            @if(Auth::user()->permissions_id == 1 || Auth::user()->permissions_id == 2)
            { data: 'prodi' },
            { data: 'fakultas' },
            @endif
            @if(Auth::user()->permissions_id == 3)
            { data: 'prodi' },
            @endif

            { data: 'total_prestasi'},

            @else
            // { data: 'npm' },
            { data: 'nama_kegiatan'},
            { data: 'kategori_kegiatan'},
            { data: 'jenis_kegiatan'},
            // { data: 'tgl_pelaksanaan'},
            // { data: 'tempat_penyelenggara'},
            // { data: 'nama_penyelenggara'},
            { data: 'tingkat'},
            { data: 'jenis_prestasi'},
            { data: 'capaian_prestasi'},
            { data: 'sertifikat'},
            @endif

        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10
                },
            },
        'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, 100, 200, 500, 1000, -1 ],
            [ '10 rows', '25 rows', '50 rows', '100 rows', '200 rows', '500 rows', '1000 rows', 'Show all' ]
        ],

    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});
// End Tabel prestasi per mahasiswa

// Tabel validasi prestasi per mahasiswa
var table;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    table = $('#table_validasi').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('prestasi.data_validasi') !!}',
          type: 'POST'
         },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            { data: 'npm' },
            { data: 'nama' },
            { data: 'tempat_lahir' },
            { data: 'tanggal_lahir' },
            { data: 'email' },

        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10
                }
            },
        'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, 100, 200, 500, 1000, -1 ],
            [ '10 rows', '25 rows', '50 rows', '100 rows', '200 rows', '500 rows', '1000 rows', 'Show all' ]
        ],

    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});
// End Tabel validasi prestasi per mahasiswa

// Tabel prestasi akademik
var table_akademik;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    table_akademik = $('#table_akademik').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('prestasi.data_jenisprestasi') !!}',
          type: 'POST',
          data: { jenis_prestasi : 'Akademik'}
         },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            @if(Auth::user()->permissions_id != 3 && Auth::user()->permissions_id != 4) // Level != Fakultas
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            @endif
            @if(Auth::user()->permissions_id != 5)
            { data: 'npm' },
            { data: 'nama'},
            @if(Auth::user()->permissions_id == 1 || Auth::user()->permissions_id == 2)
            { data: 'prodi' },
            { data: 'fakultas' },
            @endif
            @if(Auth::user()->permissions_id == 3)
            { data: 'prodi' },
            @endif
            @endif
            { data: 'nama_kegiatan'},
            { data: 'kategori_kegiatan'},
            { data: 'jenis_kegiatan'},
            { data: 'tingkat'},
            // { data: 'jenis_prestasi'},
            { data: 'capaian_prestasi'},
            { data: 'tgl_pelaksanaan'},
            { data: 'sertifikat'},
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10
                }
            },
        'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, 100, 200, 500, 1000, -1 ],
            [ '10 rows', '25 rows', '50 rows', '100 rows', '200 rows', '500 rows', '1000 rows', 'Show all' ]
        ],

    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});
// End Tabel prestasi akademik

// Tabel prestasi nonakademik
var table_nonakademik;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    table_nonakademik = $('#table_nonakademik').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('prestasi.data_jenisprestasi') !!}',
          type: 'POST',
          data: { jenis_prestasi : 'Non Akademik'}
         },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            @if(Auth::user()->permissions_id != 3 && Auth::user()->permissions_id != 4) // Level != Fakultas
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            @endif
            @if(Auth::user()->permissions_id != 5)
            { data: 'npm' },
            { data: 'nama'},
            @if(Auth::user()->permissions_id == 1 || Auth::user()->permissions_id == 2)
            { data: 'prodi' },
            { data: 'fakultas' },
            @endif
            @if(Auth::user()->permissions_id == 3)
            { data: 'prodi' },
            @endif
            @endif
            { data: 'nama_kegiatan'},
            { data: 'kategori_kegiatan'},
            { data: 'jenis_kegiatan'},
            { data: 'tingkat'},
            // { data: 'jenis_prestasi'},
            { data: 'capaian_prestasi'},
            { data: 'tgl_pelaksanaan'},
            { data: 'sertifikat'},
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10
                },
                title: 'Data Prestasi Mahasiswa Non Akademik'
            },
        'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, 100, 200, 500, 1000, -1 ],
            [ '10 rows', '25 rows', '50 rows', '100 rows', '200 rows', '500 rows', '1000 rows', 'Show all' ]
        ],

    });
    table.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );
});
// End Tabel prestasi nonakademik

// Tabel monitoring prestasi
var table_monitoring;
$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    table_monitoring = $('#table_monitoring').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('prestasi.data_monitoring') !!}',
          type: 'POST'
         },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'npm' },
            { data: 'nama'},
            { data: 'prestasi_akademik'},
            { data: 'aktifitas_akademik'},
            { data: 'prestasi_nonakademik'},
            { data: 'aktifitas_nonakademik'},

        ],
        dom: 'Bfrtip',
        buttons: [
            'csv', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10
                },
                title: 'Data Prestasi Mahasiswa'
            },

            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ],
        rowsGroup: [
        'second:nama',
        0,
        2
    ]

    });
});
// End Tabel monitoring prestasi

// Tabel Rekapitulasi Prodi


$(document).on("change","#tahun_kegiatan", function(){
    var tahun     = $("#tahun_kegiatan").val();

    location.href = "{!! url('prestasi/rekapitulasi_prodi/"+tahun+"') !!}";
});


$(document).ready( function () {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var tahun = $('#tahun').val();

    table_monitoring = $('#table_rekapitulasi').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '{!! route('prestasi.data_rekapitulasi') !!}'+'/'+tahun,
          type: 'POST'
         },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            { data: 'nama_unit' },
            { data: 'jenis_pendidikan'},
            { data: 'prestasi_akademik'},
            { data: 'aktifitas_akademik'},
            { data: 'prestasi_nonakademik'},
            { data: 'aktifitas_nonakademik'},

        ],
        dom: 'Bfrtip',
        buttons: [
            'csv', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10
                },
                title: 'Rekapitulasi Data Prestasi Mahasiswa'
            },

            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ],
        rowsGroup: [
        'second:nama_unit',
        0,
        2
    ]

    });
});
// End Tabel Rekapitulasi Prodi


// OPEN MODAL ADD
function modal_add_show(){
    save_method="add";
    $('input[name=_method]').val('POST');
    $('#modal').modal('show');
    $('#modal form')[0].reset();
    $('#modal .modal-title').text('Input Prestasi');
    $('.modal-backdrop').hide();
    $('#form-ket_kategori_peserta').hide();
    $('#btnSave').attr('disabled', false);

}

// OPEN MODAL ADD
function detailForm(npm){
    $('#modal-detail').modal('show');
    $('#modal-detail .modal-title').text('Detail Prestasi');


    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var table_detail;

    table_detail = $('#table_detail').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        bDestroy: true,
        ajax: {
          url: '{!! url('prestasi/data_detail') !!}',
          type: 'POST',
          data: { npm : npm}
         },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
            @if(Auth::user()->permissions_id == 4) // Level Prodi
            { data: 'actions', name: 'actions', orderable: false, searchable: false },
            { data: 'status_verifikasi_prodi'},
            @endif
            { data: 'nama_kegiatan'},
            { data: 'kategori_kegiatan'},
            { data: 'jenis_kegiatan'},
            { data: 'tgl_pelaksanaan'},
            { data: 'tempat_penyelenggara'},
            { data: 'nama_penyelenggara'},
            { data: 'tingkat'},
            { data: 'jenis_prestasi'},
            { data: 'capaian_prestasi'},
            { data: 'sertifikat'},
        ],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'print',
            {
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                download: 'open',
                customize: function(doc) {
                    doc.defaultStyle.fontSize = 6; //<-- set fontsize to 16 instead of 10
                    $(doc.document.body).find( 'thead' ).prepend('<div class="header-print">' + $('#dt-header').val() + '</div>');
                }
            }
        ]
    });
    table_detail.on( 'draw', function () {
        $('.livicon').each(function(){
            $(this).updateLivicon();
        });
    } );

}

$('#kategori_peserta').on("change", function (e)  {

    if($('#kategori_peserta ').val() == "Kelompok"){
        $('#form-ket_kategori_peserta').show();
    }

    if($('#kategori_peserta ').val() == "Individu"){
        $('#form-ket_kategori_peserta').hide();
    }
});

$('#kategori_kegiatan').on("change", function (e)  {

    var wrapper = $(".input_fields_wrap"); //Fields wrapper

    if($('#kategori_kegiatan ').val() == "Prestasi"){
    $(".input_fields_wrap").empty();
    $(wrapper).append('<div class="form-group"><select class="form-control show-tick" data-live-search="true" style="width:100%;cursor:pointer;" name="capaian_prestasi" id="capaian_prestasi" required><option value="" >- Pilih Prestasi -</option><option value="Juara I" >Juara I</option><option value="Juara II" >Juara II</option><option value="Juara III" >Juara III</option><option value="Harapan I" >Harapan I</option><option value="Harapan II" >Harapan II</option><option value="Harapan III" >Harapan III</option><option value="Peserta" >Peserta</option><option value="Penerima Hibah" >Penerima Hibah</option></select></div>');

    var jenis_kegiatan = $(".input_jenis_kegiatan").empty();
    $(jenis_kegiatan).append('<select class="form-control show-tick jenis_kegiatan" data-live-search="true" style="width:100%; cursor:pointer;" name="jenis_kegiatan" id="jenis_kegiatan" required><option value="" >- Pilih Jenis Kegiatan -</option><option value="PKM" >PKM</option><option value="Lomba" >Lomba</option></select>');

    }

    if($('#kategori_kegiatan ').val() == "Aktivitas"){
        $(".input_fields_wrap").empty();

        $(wrapper).append('<div class="form-group"><select class="form-control show-tick" data-live-search="true" style="width:100%;cursor:pointer;" name="capaian_prestasi" id="capaian_prestasi" required><option value="" >- Pilih Aktivitas -</option><option value="Peserta Terbaik" >Peserta Terbaik</option><option value="Pemakalah Terbaik" >Pemakalah Terbaik</option><option value="Makalah/Paper Terbaik" >Makalah/Paper Terbaik</option><option value="Peserta" >Peserta</option><option value="Pemakalah" >Pemakalah</option></select></div>');

        var jenis_kegiatan = $(".input_jenis_kegiatan").empty();
        $(jenis_kegiatan).append('<select class="form-control show-tick jenis_kegiatan" data-live-search="true" style="width:100%;cursor:pointer;" name="jenis_kegiatan" id="jenis_kegiatan" required><option value="" >- Pilih Jenis Kegiatan -</option><option value="Seminar" >Seminar</option><option value="Konferensi" >Konferensi</option><option value="Workshop" >Workshop</option><option value="Pertukaran pelajar" >Pertukaran pelajar</option><option value="Sertifikasi" >Sertifikasi</option><option value="Dan Lain Lain" >Dan Lain Lain</option></select>');
    }
});

//OPEN MODAL EDIT
function editForm(id){
    $(document).on("show.bs.modal", '.modal', function (event) {
    console.log("Global show.bs.modal fire");
    var zIndex = 100000 + (10 * $(".modal:visible").length);
    $(this).css("z-index", zIndex);
    setTimeout(function () {
        $(".modal-backdrop").not(".modal-stack").first().css("z-index", zIndex - 1).addClass("modal-stack");
    }, 0);
    }).on("hidden.bs.modal", '.modal', function (event) {
        console.log("Global hidden.bs.modal fire");
        $(".modal:visible").length && $("body").addClass("modal-open");
    });
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-detail').modal('hide');
    $('#modal').modal('show');
    $('#modal .modal-title').text('Edit Prestasi');

    $("body").on("click",".clearCacheForm",function(){
            $(".refresh").empty();
            $(".refresh").attr('readonly',false);
    });
    $.ajax({
        url: "{{ url('prestasi') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id').val(data.id);
                $('#nama_kegiatan').val(data.nama_kegiatan);
                $("#kategori_kegiatan option[value='" + data.kategori_kegiatan + "']").prop("selected", true).change();
                $("#jenis_kegiatan option[value='" + data.jenis_kegiatan + "']").prop("selected", true).change();
                $('#tgl_pelaksanaan').val(data.tgl_pelaksanaan);
                $('#tempat_penyelenggara').val(data.tempat_penyelenggara);
                $('#nama_penyelenggara').val(data.nama_penyelenggara);
                $("#tingkat option[value='" + data.tingkat + "']").prop("selected", true).change();
                $("#jenis_prestasi option[value='" + data.jenis_prestasi + "']").prop("selected", true).change();
                $("#capaian_prestasi option[value='" + data.capaian_prestasi + "']").prop("selected", true).change();
                $('#deskripsi_prestasi').val(data.deskripsi_prestasi);
                $("#kategori_peserta option[value='" + data.kategori_peserta + "']").prop("selected", true).change();
                $('#ket_kategori_peserta').val(parseInt(data.ket_kategori_peserta));

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};


//OPEN MODAL transForm
function transForm(id){
    $('input[name=_method]').val('PUT');
    $('#modal-trans').modal('show');
    $('#modal-trans .modal-title').text('Translate');

    $.ajax({
        url: "{{ url('prestasi') }}"+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
                $('#id_trans').val(data.id);
                $('#nama_kegiatan_trans').val(data.nama_kegiatan);
                $('#nama_kegiatan_en_trans').val(data.nama_kegiatan_en);
        },
        error: function() {
            alert('Data is empty !')
        }
    });
};

//OPEN MODAL transForm
function transFormAll(npm){
    $('input[name=_method]').val('POST');
    $('#modal-trans-all').modal('show');
    $('#modal-trans-all .modal-title').text('Translate');
    var wrapper = $(".input_fields_wrap"); //Fields wrapper

    $.ajax({
        url: "{{ url('prestasi/translate') }}"+'/'+npm,
        type: "GET",
        dataType: "JSON",
        success: function(data) {

            $(".input_fields_wrap").empty();
            var n=0;
            $.each(data, function(key, value)
            {
                $(wrapper).append('<div class="row clearfix form-kegiatan"><div class="col-lg-6 col-md-6 col-sm-6"><input type="hidden"  name="id_prestasi[]" class="form-control id_prestasi'+n+'"  ><div class="form-group" style="width:100%"><input type="text" name="nama_kegiatan_trans[]" class="form-control nama_kegiatan_trans'+n+'" placeholder="Nama Kegiatan" ></div></div><div class="col-lg-6 col-md-6 col-sm-6"><div class="form-group" style="width:100%"><input type="text" name="nama_kegiatan_en_trans[]" class="form-control nama_kegiatan_en_trans'+n+'" placeholder="Nama Kegiatan English" required></div></div></div>'); //looping form kegiatan
                $('#npm').val(value.npm);
                $('#judul_skripsi').val(value.judul_skripsi);
                $('#judul_skripsi_en').val(value.judul_skripsi_en);
                $(".id_prestasi"+n).val(value.id);
                $(".nama_kegiatan_trans"+n).val(value.nama_kegiatan);
                $(".nama_kegiatan_en_trans"+n).val(value.nama_kegiatan_en);
                n=n+1;
            });

        },
        error: function() {
            alert('Data is empty !')
        }
    });
};
//OPEN MODAL DELETE
function deleteForm(id){
    swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            }, function () {
                    $.ajax({
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},
                    type: "POST",
                    url: "{{ url('prestasi') }}"+'/'+id+"/delete",
                    timeout: 3000,
                    success : function(e) {
                        if (e = 1) {
                            ShowAlertDeletedWithReload();
                        }
                        else{
                            swal("Oops!", "Gagal Menghapus Data.", "warning");
                        }
                    },
                    error : function(e){
                        showErrorMessage();
                    }
                    });
            });
};

//OPEN MODAL Approve All
function approveForm(id){
    swal({
            title: "Apakah kamu yakin menyetujui prestasi Mahasiswa ini ?",
            text: "Kamu tidak lagi dapat merubah data ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, setuju !",
            closeOnConfirm: "Nanti saja"
            }, function () {
                    $.ajax({
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},
                    type: "POST",
                    url: "{{ url('prestasi') }}"+'/'+id+"/approve",
                    timeout: 3000,
                    success : function(e) {
                        if (e = 1) {
                            // ShowAlertDeletedWithReload();
                            swal({
                                title: 'Sukses',
                                text: 'Data berhasil di approve.',
                                type: 'success',
                                timer: 2000
                            });
                            location.reload();

                        }
                        else{
                            swal("Oops!", "Gagal Memproses Data.", "warning");
                        }
                    },
                    error : function(e){
                        showErrorMessage();
                    }
                    });
            });
};

//OPEN MODAL Approve All
function approve_oneForm(id){
    swal({
            title: "Apakah kamu yakin menyetujui prestasi ini ?",
            text: "Kamu tidak lagi dapat merubah data ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, setuju !",
            closeOnConfirm: "Nanti saja"
            }, function () {
                    $.ajax({
                    headers: {'X-CSRF-TOKEN': $('#modal input[name="_token"]').val()},
                    type: "POST",
                    url: "{{ url('prestasi') }}"+'/'+id+"/approve_one",
                    timeout: 6000,
                    success : function(e) {
                        if (e == 'sukses') {
                            // ShowAlertDeletedWithReload();
                            swal({
                                title: 'Sukses',
                                text: 'Data berhasil di approve.',
                                type: 'success',
                                timer: 2000
                            });
                            location.reload();

                        }else if(e == 'cp_kosong'){
                            swal("Oops!", "Harap Mengisi Capaian Pembelajaran terlebih dahulu", "warning");
                        }
                        else{
                            swal("Oops!", "Gagal Memproses Data.", "warning");
                        }
                    },
                    error : function(e){
                        showErrorMessage();
                    }
                    });
            });
};


// End DELETE Function

$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});

$('#btnSave').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){
            var id=$('#id').val();

            if (id == '') {
                var url  = "{{ route('prestasi.store') }}";
            }
            else
            {
                var url  = "{{  url('prestasi') }}"+'/'+id;
            }

            // for (instance in CKEDITOR.instances) {
            //     CKEDITOR.instances['isi_cp'].updateElement();
            //     CKEDITOR.instances['isi_cp_en'].updateElement();
            // }
            var form = $('#modal-form')[0];
            var inputData = new FormData(form);
            $.ajax({
                processData: false,
                contentType: false,
                cache: false,
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: inputData,
                timeout: 600000,
                beforeSend: function(){
				$(".btnSave").prop("disabled", true);
				$(".btnSave").html('Submitting...');
			    },
                success : function(syabdan) {

                        document.getElementById("modal-form").reset();
					    $(".btnSave").prop("disabled", false);
                        $(".btnSave").html('Submit');
                        showAjaxLoaderSuccesMessage();
                },
                error : function(syabdan){
				    $(".btnSave").prop("disabled", false);
                    $(".btnSave").html('Submit');
                    showErrorMessage();
                    if(syabdan['responseJSON']['errors']) {
                        if(syabdan['responseJSON']['errors']['nama_kegiatan']){
                            $( '#nama_kegiatan-Error' ).html(syabdan['responseJSON']['errors']['nama_kegiatan'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['kategori_kegiatan']){
                            $( '#kategori_kegiatan-Error' ).html(syabdan['responseJSON']['errors']['kategori_kegiatan'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['jenis_kegiatan']){
                            $( '#jenis_kegiatan-Error' ).html(syabdan['responseJSON']['errors']['jenis_kegiatan'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['tgl_pelaksanaan']){
                            $( '#tgl_pelaksanaan-Error' ).html(syabdan['responseJSON']['errors']['tgl_pelaksanaan'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['tempat_penyelenggara']){
                            $( '#tempat_penyelenggara-Error' ).html(syabdan['responseJSON']['errors']['tempat_penyelenggara'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['nama_penyelenggara']){
                            $( '#nama_penyelenggara-Error' ).html(syabdan['responseJSON']['errors']['nama_penyelenggara'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['tingkat']){
                            $( '#tingkat-Error' ).html(syabdan['responseJSON']['errors']['tingkat'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['jenis_prestasi']){
                            $( '#jenis_prestasi-Error' ).html(syabdan['responseJSON']['errors']['jenis_prestasi'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['capaian_prestasi']){
                            $( '#capaian_prestasi-Error' ).html(syabdan['responseJSON']['errors']['capaian_prestasi'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['kategori_peserta']){
                            $( '#kategori_peserta-Error' ).html(syabdan['responseJSON']['errors']['kategori_peserta'][0]);
                        }
                        if(syabdan['responseJSON']['errors']['sertifikat']){
                            $( '#sertifikat-Error' ).html(syabdan['responseJSON']['errors']['sertifikat'][0]);
                        }

                    }
                }
            });
            return false;
        }
});

$('#btnSaveTransAll').on('click', function (syabdan){
    if (!syabdan.isDefaultPrevented()){
            var npm  = $('#npm').val();

            var url  = "{{  url('prestasi/simpan_translate') }}"+'/'+npm;

            var form = $('#modal-form-trans-all')[0];
            var inputData = new FormData(form);
            $.ajax({
                processData: false,
                contentType: false,
                cache: false,
                type: "POST",
                enctype: 'multipart/form-data',
                url: url,
                data: inputData,
                dataType: "JSON",
                timeout: 600000,
                success : function(syabdan) {

                        document.getElementById("modal-form-trans-all").reset();
                        showAjaxLoaderSuccesMessage();

                },
                error : function(syabdan){
                 showErrorMessage();
                }
            });
            return false;
        }
});


</script>

<script>

$(function () {
        $('#tgl_pelaksanaan').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            time: false
        });
    });

</script>
