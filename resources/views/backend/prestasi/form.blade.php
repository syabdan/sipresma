<div class="modal fade" id="modal" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false" tabindex="">
    <div class="modal-dialog modal-lg" role="document" >
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal" id="modal-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  />
                    <div class="row clearfix" style="display: none;">

                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="hidden" id="id" name="id" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Nama Kegiatan</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama_kegiatan" name="nama_kegiatan" class="form-control" placeholder="Nama Kegiatan" required>
                                <span style="color: red;"><i><p id="nama_kegiatan-Error"></p></i></span>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Nama Kegiatan ( English )</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama_kegiatan_en" name="nama_kegiatan_en" class="form-control" placeholder="Nama Kegiatan" required>
                            </div>
                        </div>
                    </div> -->
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Kategori Kegiatan</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <select class="form-control show-tick" data-live-search="true" name="kategori_kegiatan" id="kategori_kegiatan" required>
                            <option value="" >-- Pilih Kategori Kegiatan --</option>
                            <option value="Aktivitas" >Aktivitas</option>
                            <option value="Prestasi" >Prestasi</option>
                            </select>
                            <span style="color: red;"><i><p id="kategori_kegiatan-Error"></p></i></span>

                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Jenis Kegiatan</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <div class="input_jenis_kegiatan"></div>
                            <span style="color: red;"><i><p id="jenis_kegiatan-Error"></p></i></span>

                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Tanggal pelaksanaan</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                        <div class="input-group " >
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="tgl_pelaksanaan" name="tgl_pelaksanaan" class="form-control" placeholder="Please choose date" pickTime="false">
                            <span style="color: red;"><i><p id="tgl_pelaksanaan-Error"></p></i></span>

                        </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Kota Tempat Penyelenggara</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="tempat_penyelenggara" name="tempat_penyelenggara" class="form-control " placeholder="Tempat Penyelenggara" required>
                                <span style="color: red;"><i><p id="tempat_penyelenggara-Error"></p></i></span>

                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Nama Penyelenggara</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama_penyelenggara" name="nama_penyelenggara" class="form-control" placeholder="Nama Penyelenggara" required>
                                <span style="color: red;"><i><p id="nama_penyelenggara-Error"></p></i></span>

                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Tingkat</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <select class="form-control show-tick" data-live-search="true" name="tingkat" id="tingkat" required>
                                <option value="" >- Pilih Tingkat -</option>
                                <option value="Internasional" >Internasional</option>
                                <option value="Nasional">Nasional</option>
                                <option value="Lokal">Lokal</option>
                                </select>
                                <span style="color: red;"><i><p id="tingkat-Error"></p></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Jenis Prestasi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <select class="form-control show-tick" data-live-search="true" name="jenis_prestasi" id="jenis_prestasi">
                            <option value="" >-Pilih Jenis Prestasi -</option>
                            <option value="Akademik" >Akademik</option>
                            <option value="Non Akademik" >Non Akademik</option>
                            </select>
                            <span style="color: red;"><i><p id="jenis_prestasi-Error"></p></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Prestasi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                        <div class="form-group">
                            <div class="input_fields_wrap"></div>
                            <span style="color: red;"><i><p id="capaian_prestasi-Error"></p></i></span>
                        </div>
                        </div>
                    </div>
                    <!-- <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Deskripsi Prestasi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group" >
                            <textarea class="form-control" name="deskripsi_prestasi" id="deskripsi_prestasi" >
                            </textarea>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Deskripsi Prestasi ( English )</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group" >
                            <textarea class="form-control" name="deskripsi_prestasi_en" id="deskripsi_prestasi_en" >
                            </textarea>
                            </div>
                        </div>
                    </div> -->
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Kategori Peserta</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <select class="form-control show-tick" data-live-search="true" name="kategori_peserta" id="kategori_peserta">
                                <option value="" >- Pilih Kategori Peserta -</option>
                                <option value="Individu" >Individu</option>
                                <option value="Kelompok" >Kelompok</option>
                                </select>
                                <span style="color: red;"><i><p id="kategori_peserta-Error"></p></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix" id="form-ket_kategori_peserta">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Keterangan Jumlah Dalam Kelompok</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                            <input type="number" maxlength="6" id="ket_kategori_peserta" name="ket_kategori_peserta" class="form-control" placeholder="eg. 5" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label >Bukti Dokumen (*.pdf) </label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="file" class="form-control" accept="Application/pdf" name="sertifikat">
                                <span style="color: red;"><i><p id="sertifikat-Error"></p></i></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSave" class="btn btn-default btn-round waves-effect btnSave"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-trans" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal" id="modal-form-trans">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  />
                    <div class="row clearfix" style="display: none;">

                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="id_trans" name="id" class="form-control">
                                <input type="text" id="form_trans" name="form_trans" class="form-control" value="1">
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Nama Kegiatan</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama_kegiatan_trans" name="nama_kegiatan" class="form-control" placeholder="Nama Kegiatan" >
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Nama Kegiatan (English)</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="nama_kegiatan_en_trans" name="nama_kegiatan_en" class="form-control" placeholder="Nama Kegiatan English" required>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSaveTrans" class="btn btn-default btn-round waves-effect"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-trans-all" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">
                <form data-toggle="validator" method="PUT" enctype="multipart/form-data" class="form-horizontal" id="modal-form-trans-all">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method"  />
                    <div class="row clearfix" style="display: none;">
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="npm" name="npm" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Judul Skripsi</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="judul_skripsi" name="judul_skripsi" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-2 col-sm-4 form-control-label">
                            <label>Judul Skripsi (English)</label>
                        </div>
                        <div class="col-lg-9 col-md-10 col-sm-8">
                            <div class="form-group">
                                <input type="text" id="judul_skripsi_en" name="judul_skripsi_en" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-6 col-sm-6 form-control-label">
                            <label>Nama Kegiatan</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 form-control-label">
                        <label>Nama Kegiatan (English)</label>
                        </div>
                    </div>
                    <div class="input_fields_wrap"></div>
                    <!-- <div class="row clearfix form-kegiatan">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                        <input type="text"  name="id_prestasi" class="form-control"  >
                            <div class="form-group">
                                <input type="text" name="nama_kegiatan_trans" class="form-control" placeholder="Nama Kegiatan" >
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" name="nama_kegiatan_en_trans" class="form-control" placeholder="Nama Kegiatan English" required>
                            </div>
                        </div>
                    </div> -->

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
                <button type="button" id="btnSaveTransAll" class="btn btn-default btn-round waves-effect"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h6 class="title modal-title" id="largeModalLabel"> </h6>
            </div>
            <div class="modal-body">

            <div class="body table-responsive">
                        <table class="table table-hover m-b-0 c_list" id="table_detail">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    @if(Auth::user()->permissions_id == 4) <!-- Level Prodi -->
                                    <th data-breakpoints="sm xs md">Action</th>
                                    <th data-breakpoints="sm xs md">Status Verifikasi</th>
                                    @endif
                                    <th data-breakpoints="xs">Nama Kegiatan</th>
                                    <th data-breakpoints="xs">Kategori Kegiatan</th>
                                    <th data-breakpoints="xs">Jenis Kegiatan</th>
                                    <th data-breakpoints="xs">Tgl Pelaksanaan</th>
                                    <th data-breakpoints="xs">Tmpt penyelenggara</th>
                                    <th data-breakpoints="xs">Nama Penyelenggara</th>
                                    <th data-breakpoints="xs">Tingkat</th>
                                    <th data-breakpoints="xs">Jenis Prestasi</th>
                                    <th data-breakpoints="xs">Capaian Prestasi</th>
                                    <th data-breakpoints="xs">Sertifikat</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn-danger waves-effect" data-dismiss="modal"><i class="zmdi zmdi-close zmdi-hc-lg"></i>Close</button>
            </div>
        </div>
    </div>
</div>
