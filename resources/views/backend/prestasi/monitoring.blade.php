@extends('layouts.backend.app')
@section('title', 'Monitoring Data Prestasi Mahasiswa')
@section('header','Monitoring Data Prestasi Mahasiswa')
@section('subheader','Monitoring Data Prestasi Mahasiswa')
@section('bread') <a href="">Monitoring Data Prestasi Mahasiswa</a> @endsection

@push('style')
<link href="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
<link href="{{ asset('backend/assets/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('backend/assets/plugins/footable-bootstrap/css/footable.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/assets/plugins/footable-bootstrap/css/footable.standalone.min.css')}}">
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="{{ asset('backend/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<!-- Dropzone Css -->
<link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropzone/dropzone.css')}}">

<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
<link href="{{ asset('backend/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" />

@endpush
@push('scripthead')

@endpush
@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                  
                    </div>
                    <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-hover m-b-0 c_list" id="table_monitoring">
                            <thead>
                                <tr>
                                    <th rowspan="2">No</th> 
                                    <th  rowspan="2">NPM</th>
                                    <th  rowspan="2">Nama</th>
                                    <th  colspan="2">Jumlah Kegiatan Akademik</th>
                                    <th  colspan="2">Jumlah Kegiatan Non Akademik</th>
                                </tr>
                                <tr>
                                    <th  >Prestasi</th>
                                    <th  >Aktivitas</th>
                                    <th  >Prestasi</th>
                                    <th  >Aktivitas</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scriptbottom')
<script src="{{ asset('backend/assets/plugins/momentjs/moment.js')}}"></script> 
<script src="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script> 
<script src="{{ asset('backend/assets/js/pages/forms/basic-form-elements.js')}}"></script> 
<script src="{{ asset('backend/assets/bundles/footable.bundle.js')}}"></script>

<!-- Jquery DataTable Plugin Js --> 
<script src="{{ asset('backend/assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.jszip.min.js')}}"></script>

    

<script src="{{ asset('backend/assets/plugins/ckeditor/ckeditor.js')}}"></script> <!-- Ckeditor --> 
<script src="{{ asset('backend/assets/plugins/ckeditor/config.js')}}"></script> <!-- Ckeditor --> 
<script src="{{ asset('backend/assets/plugins/momentjs/moment.js')}}"></script> <!-- Moment Plugin Js --> 
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<script src="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script> 
<script src="{{ asset('backend/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script> 
@include('backend.prestasi.ajax')

@endpush