@extends('layouts.backend.app')

@if(Auth::user()->permissions_id == 5)
    @section('title', 'Data Prestasi Mahasiswa')
    @section('header','Data Prestasi Mahasiswa')
    @section('subheader','Data Prestasi Mahasiswa')
    @section('bread') <a href="">Data Prestasi Mahasiswa</a> @endsection
@else
    @section('title', 'Data Prestasi Per Mahasiswa')
    @section('header','Data Prestasi Per Mahasiswa')
    @section('subheader','Data Prestasi Per Mahasiswa')
    @section('bread') <a href="">Data Prestasi Per Mahasiswa</a> @endsection
@endif

@push('style')
<link href="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
<link href="{{ asset('backend/assets/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('backend/assets/plugins/footable-bootstrap/css/footable.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/assets/plugins/footable-bootstrap/css/footable.standalone.min.css')}}">
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="{{ asset('backend/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}">
<!-- Dropzone Css -->
<link rel="stylesheet" href="{{ asset('backend/assets/plugins/dropzone/dropzone.css')}}">

<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
<link href="{{ asset('backend/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" />

<link href="{{ asset('backend/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('backend/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
@endpush
@push('scripthead')
<style>
@media print {
  .header-print {
    display: table-header-group;
  }
}
</style>
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                    @if(Auth::user()->permissions_id ==5)
                        <button type="button" class="btn btn-info btn-round waves-effect btn-sm align-right"  onclick="modal_add_show()"><i class="zmdi zmdi-plus "></i> Tambah</button>
                    @endif
                    </div>
                    <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-hover m-b-0 c_list" id="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th data-breakpoints="xs">Action</th>
                                    @if(Auth::user()->permissions_id != 5) <!-- Tidak == mahasiswa -->
                                    <th data-breakpoints="xs">NPM</th>
                                    <th data-breakpoints="xs">Nama</th>
                                        @if(Auth::user()->permissions_id == 1 || Auth::user()->permissions_id == 2)
                                        <th data-breakpoints="xs">Prodi</th>
                                        <th data-breakpoints="xs">Fakultas</th>
                                        @endif
                                        @if(Auth::user()->permissions_id == 3)
                                        <th data-breakpoints="xs">Prodi</th>
                                        @endif
                                    {{--  <th data-breakpoints="xs">Tempat Lahir</th>
                                    <th data-breakpoints="xs">Tanggal Lahir</th>
                                    <th data-breakpoints="xs">Email</th>  --}}
                                    <th data-breakpoints="xs">Total Prestasi</th>

                                    @else
                                    <!-- <th data-breakpoints="xs">NPM</th> -->
                                    <th data-breakpoints="xs">Nama Kegiatan</th>
                                    <th data-breakpoints="xs">Kategori Kegiatan</th>
                                    <th data-breakpoints="xs">Jenis Kegiatan</th>
                                    <th data-breakpoints="xs">Tingkat</th>
                                    <th data-breakpoints="xs">Jenis Prestasi</th>
                                    <th data-breakpoints="xs">Capaian Prestasi</th>
                                    <th data-breakpoints="xs">Sertifikat</th>
                                    @endif
                                </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
	@include('backend.prestasi.form')
@endsection
@push('scriptbottom')
<script src="{{ asset('backend/assets/plugins/momentjs/moment.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{ asset('backend/assets/js/pages/forms/basic-form-elements.js')}}"></script>
<script src="{{ asset('backend/assets/bundles/footable.bundle.js')}}"></script>
<script src="{{ asset('backend/vendors/select2/js/select2.js') }}" type="text/javascript"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('backend/assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/jquery-datatable/buttons/buttons.jszip.min.js')}}"></script>

<script src="{{ asset('backend/assets/plugins/ckeditor/ckeditor.js')}}"></script> <!-- Ckeditor -->
<script src="{{ asset('backend/assets/plugins/ckeditor/config.js')}}"></script> <!-- Ckeditor -->
<script src="{{ asset('backend/assets/plugins/momentjs/moment.js')}}"></script> <!-- Moment Plugin Js -->
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ asset('backend/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{ asset('backend/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
@include('backend.prestasi.ajax')

@endpush
