<aside id="leftsidebar" class="sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#dashboard"><i class="zmdi zmdi-home m-r-5"></i>APP</a></li>
        <li class="nav-item"><a class="nav-link " data-toggle="tab" href="#user"><i class="zmdi zmdi-account m-r-5"></i>User</a></li>
    </ul>


    <div class="tab-content">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info">
                            <div class="image">
                            @if(Auth::user()->pic)
                            <img src="{{ asset('backend/assets/images/users/'.Auth::user()->pic)}}" alt="User">
                            @else
                            <img src="{{ asset('backend/assets/images/user.jpg')}}" alt="User">
                            @endif
                            </div>
                            <div class="detail">
                                <h4>{{Auth::User()->name}}</h4>
                                <small>{{Auth::User()->email}}</small>                        
                            </div>
                            <!-- <a title="facebook" href="#"><i class="zmdi zmdi-facebook"></i></a>
                            <a title="twitter" href="#"><i class="zmdi zmdi-twitter"></i></a>
                            <a title="instagram" href="#"><i class="zmdi zmdi-instagram"></i></a>                             -->
                        </div>
                    </li>
                    
                    <!-- <li><a href="{{url('dashboard')}}" ><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li> -->
                    <?php
                    $data_sections_arr = explode(",", Auth::user()->permissionsGroup->data_menus);
                    ?>
                    @foreach($menu as $row)
                    @if(in_array($row->id,$data_sections_arr))

                    <li> <a href="{{ url($row->link) }}" @if($row->link == '#') class="menu-toggle" @endif><i class="zmdi zmdi-{{ $row->icon}}"></i><span>{{ $row->nama }}</span></a>

                        <ul class="ml-menu">
                        @foreach($row->subMenus as $rowSub)
                        @if(in_array($rowSub->id,$data_sections_arr))
                            <li><a href="{{url($rowSub->link)}}">{{ $rowSub->nama}}</a></li>
                        @endif
                        @endforeach
                        </ul>
        
                    </li>
                    @endif
                    @endforeach

                </ul>
            </div>
        </div>
        <div class="tab-pane stretchLeft" id="user">
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info m-b-20 p-b-15">
                            <div class="image">
                            @if(Auth::user()->pic)
                            <img src="{{ asset('backend/assets/images/users/'.Auth::user()->pic)}}" alt="User">
                            @else
                            <img src="{{ asset('backend/assets/images/user.jpg')}}" alt="User">
                            @endif
                            </div>
                            <div class="detail">
                                <h4>{{Auth::User()->name}}</h4>
                                <small>@if(Auth::User()->aktif == 1 ) Online @else Offline @endif</small>
                            </div>
                            <!-- <a title="facebook" href="{{Auth::User()->facebook}}"><i class="zmdi zmdi-facebook"></i></a>
                            <a title="twitter" href="{{Auth::User()->twitter}}"><i class="zmdi zmdi-twitter"></i></a>
                            <a title="instagram" href="{{Auth::User()->instagram}}"><i class="zmdi zmdi-instagram"></i></a> -->
                            <p class="text-muted"></p>
                        </div>
                    </li>
                    <li>
                    <form data-toggle="validator" enctype="multipart/form-data" class="form-horizontal" id="profile-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="_method" value="PUT" />
                    <input type="hidden" id="id_profile" name="id" value="{{ Auth::user()->id }}">

                        <small class="text-muted">Username: </small>
                        <p>{{Auth::User()->email}}</p>
                        <hr>
                        <small class="text-muted">Phone: </small>
                        <p>{{Auth::User()->hp}}</p>
                        <hr> 
                        <small class="text-muted">Pic Profile: </small>
                        <p><input type="file" class="form-control" accept="image/*" name="pic_file"></p>
                        <hr> 
                        <small class="text-muted">Password: </small>
                        <p><div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                <input type="text" id="password" name="password" class="form-control" placeholder="Password">
                            </div>
                        </p>
                        <hr> 
                          
                        <button type="button" id="profile_save" class="btn btn-default btn-round waves-effect"><i class="zmdi zmdi-upload zmdi-hc-lg"></i> Edit</button>
                    </form>               
                    </li>
                </ul>
            </div>
        </div>
    </div>    
</aside>

