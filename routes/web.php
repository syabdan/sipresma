<?php
use App\User;

	Route::get('insert_mahasiswa/{id}', function ($id) {
		if($id == '1'){
			\Artisan::call('insert:mahasiswa_1');
		}
		else if($id == '2'){
			\Artisan::call('insert:mahasiswa_2');
		}
		else if($id == '3'){
			\Artisan::call('insert:mahasiswa_3');
		}
		else if($id == '4'){
			\Artisan::call('insert:mahasiswa_4');
		}
		else if($id == '5'){
			\Artisan::call('insert:mahasiswa_5');
		}
		else if($id == '6'){
			\Artisan::call('insert:mahasiswa_6');
		}
		else if($id == 's2'){
			\Artisan::call('insert:mahasiswa_s2');
		}
	});

    Route::get('update_dosen', function () {
		\Artisan::call('update:dosen');
	});

    Route::get('update_akreditasi', function () {
        \Artisan::call('update:akreditasi');
    });

	Route::get('/', function () { return view('backend/login/index');	});

	Auth::routes();
	Route::any('pengguna/masuk', 'backend\masukController@masuk')->name('masuk');

	Route::group(['middleware' => [ 'auth']], function () {

	Route::get('/dashboard/{tahun?}', 'backend\Dashboard@index')->name('dashboard');

	# Periode
	Route::group([ 'prefix' => 'periode'], function () {
		Route::any('data', 'backend\periodeController@data')->name('periode.data');
		Route::post('{periode}/delete', 'backend\periodeController@destroy')->name('periode.delete');
	});
	Route::resource('periode', 'backend\periodeController');
	
	# User
	Route::group([ 'prefix' => 'user'], function () {
		Route::any('data', 'backend\UsersController@data')->name('user.data');
		Route::post('{user}/delete', 'backend\UsersController@destroy')->name('user.delete');
	});
	Route::resource('user', 'backend\UsersController');

	# Unit
	Route::group([ 'prefix' => 'unit'], function () {
		Route::any('data', 'backend\UnitController@data')->name('unit.data');
		Route::post('{unit}/delete', 'backend\UnitController@destroy')->name('unit.delete');
	});
	Route::resource('unit', 'backend\UnitController');

	# Dosen
	Route::group([ 'prefix' => 'dosen'], function () {
		Route::any('data', 'backend\dosenController@data')->name('dosen.data');
		Route::post('{dosen}/delete', 'backend\dosenController@destroy')->name('dosen.delete');
	});
	Route::resource('dosen', 'backend\dosenController');

	# Mahasiswa
	Route::group([ 'prefix' => 'mahasiswa'], function () {
		Route::any('data', 'backend\mahasiswaController@data')->name('mahasiswa.data');
        Route::post('{mahasiswa}/delete', 'backend\mahasiswaController@destroy')->name('mahasiswa.delete');

		Route::any('data_identitas', 'backend\mahasiswaController@data_identitas')->name('mahasiswa.data_identitas');
		Route::get('identitas', 'backend\mahasiswaController@index_identitas')->name('mahasiswa.index_identitas');
		Route::any('store_identitas/{id}', 'backend\mahasiswaController@store_identitas')->name('mahasiswa.store_identitas');
	});
	Route::resource('mahasiswa', 'backend\mahasiswaController');

	# SKPI
	Route::group([ 'prefix' => 'skpi'], function () {
		Route::any('data', 'backend\skpiController@data')->name('skpi.data');
		Route::any('data_skpinon', 'backend\skpiController@data_skpinon')->name('skpi.data_skpinon');
		Route::get('non', 'backend\skpiController@non')->name('skpi.non');
		Route::post('{skpi}/delete', 'backend\skpiController@destroy')->name('skpi.delete');
		Route::post('validasi', 'backend\skpiController@validasi')->name('skpi.validasi');
		Route::get('cetak/{skpi}', 'backend\skpiController@cetak_skpi')->name('skpi.cetak_skpi');
		Route::any('cetak_ex/{npm}/{dosen}', 'backend\skpiController@cetak_skpi_ex')->name('skpi.cetak_skpi_ex');
        Route::get('prestasi/{npm}', 'backend\skpiController@showPrestasi')->name('skpi.showPrestasi');


	});
	Route::resource('skpi', 'backend\skpiController');

	# capaian pembelajaran
	Route::group([ 'prefix' => 'capaian_pembelajaran'], function () {
		Route::any('data', 'backend\capaianPembelajaranController@data')->name('capaian_pembelajaran.data');
		Route::post('{capaian_pembelajaran}/delete', 'backend\capaianPembelajaranController@destroy')->name('capaian_pembelajaran.delete');
	});
	Route::resource('capaian_pembelajaran', 'backend\capaianPembelajaranController');

	# Prestasi
    Route::group([ 'prefix' => 'prestasi'], function () {
        Route::any('data', 'backend\prestasiController@data')->name('prestasi.data');
        Route::any('data_jenisprestasi', 'backend\prestasiController@data_jenisprestasi')->name('prestasi.data_jenisprestasi');
        Route::any('data_monitoring', 'backend\prestasiController@data_monitoring')->name('prestasi.data_monitoring');
        Route::any('data_rekapitulasi/{tahun?}', 'backend\prestasiController@data_rekapitulasi')->name('prestasi.data_rekapitulasi');
        Route::any('data_detail', 'backend\prestasiController@data_detail')->name('prestasi.data_detail');
        Route::any('data_validasi', 'backend\prestasiController@data_validasi')->name('prestasi.data_validasi');
        // Route::post('translate', 'backend\prestasiController@trans')->name('prestasi.trans');
        Route::get('translate/{npm}', 'backend\prestasiController@showTranslate')->name('prestasi.showTranslate');
        Route::post('simpan_translate/{npm}', 'backend\prestasiController@simpanTranslate')->name('prestasi.simpanTranslate');
        Route::post('{prestasi}/delete', 'backend\prestasiController@destroy')->name('prestasi.delete');
        Route::post('{prestasi}/approve', 'backend\prestasiController@approve')->name('prestasi.approve');
        Route::post('{prestasi}/approve_one', 'backend\prestasiController@approve_one')->name('prestasi.approve_one');
        Route::get('ambilFile/{prestasi}', 'backend\prestasiController@ambilFile')->name('prestasi.ambilFile');
        Route::get('akademik', 'backend\prestasiController@akademik')->name('prestasi.akademik');
        Route::get('nonakademik', 'backend\prestasiController@nonakademik')->name('prestasi.nonakademik');
        Route::get('validasi', 'backend\prestasiController@validasi_index')->name('prestasi.validasi_index');
        Route::get('monitoring', 'backend\prestasiController@monitoring_index')->name('prestasi.monitoring');
        Route::get('rekapitulasi_prodi/{tahun?}', 'backend\prestasiController@rekapitulasi_prodi')->name('prestasi.rekapitulasi_prodi');
    });
	Route::resource('prestasi', 'backend\prestasiController');
	Route::get('prestasi-input', 'backend\prestasiController@index')->name('prestasi-input');
	});

	Route::get('logout', 'Auth\LoginController@logout', function () {
    	return abort(404);
	});

	Route::get('skpi/unduh/{npm}/{id}/{nama?}/{file?}', 'backend\skpiController@unduh')->name('skpi.unduh');


Route::get('api_akreditasi', function() {
	return api_akreditasi();
});

Route::get('test_api/{npm}', function($npm) {
	return api_mahasiswa_by_npm($npm);
});

Route::get('api_mhs_limit_test/', function() {
	$alldata = array();
	// $data1 = api_mahasiswa_limit(0,5000);
	// $data2 = api_mahasiswa_limit(5001,10000);
	// $data3 = api_mahasiswa_limit(10001,15000);
	$data4 = api_mahasiswa_limit(15001,20000);
	$data5 = api_mahasiswa_limit(20001,25000);
	$data6 = api_mahasiswa_limit(25001,30000);
	// $alldata = array_merge($data, $data2);

	// decode json to array
	// $array[] = json_decode($data1, true);
	// $array[] = json_decode($data2, true);
	// $array[] = json_decode($data3, true);
	$array[] = json_decode($data4, true);
	$array[] = json_decode($data5, true);
	$array[] = json_decode($data6, true);

	// encode array to json
	$result = json_encode($array);
	$sumber = $result;
	$list = json_decode($sumber, true);
	return $list;
});
Route::get('test_api_skripsi/{npm}', function($npm) {
	return api_skripsi_by_npm($npm);
});
Route::get('api_skripsi_by_tahun/{tahun}', function($tahun) {
	return api_skripsi_by_tahun($tahun,1000,1100);
});
Route::get('test_api_mahasiswa/{npm}', function($npm) {
	return api_mahasiswa_by_npm($npm);
});

Route::get('api_mhs_limit/{awal}/{akhir}', function($awal,$akhir) {
	return api_mahasiswa_limit($awal,$akhir);
});

Route::get('test_api_mhs/{awal}/{akhir}', function($awal,$akhir) {
	return api_mahasiswa_all($awal,$akhir);
});

Route::get('test_api_s2/{npm}', function($npm) {
	return api_mahasiswa_by_npm_s2($npm);
});

Route::get('api_mhs_full_s2', function() {
	return api_mahasiswa_all_s2();
});

Route::get('test_api_mhs_s2/{awal}/{akhir}', function($awal,$akhir) {
	return api_mahasiswa_all_s2($awal,$akhir);
});

Route::get('test_mantra', function() {
	$d = new User();
	return $d->mantra('123510583');
});

Route::get('test_mantras2', function() {
	$d = new User();
	return $d->mantras2('187121043');
});

Route::get('lama_studi/{tgl_lulus}/{angkatan}', function($tgl_lulus,$angkatan) {
	return siswa_waktu($tgl_lulus, $angkatan);
});
